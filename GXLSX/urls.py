from django.urls import path

from . import views 

app_name = 'GXLSX_app'

urlpatterns = [
    path('get-excel-file-static/', views.generateExcel, name = 'GXLSX_get_excel')
]