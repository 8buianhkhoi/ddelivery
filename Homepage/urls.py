from django.urls import path
from . import views

app_name = 'homepage_app'

urlpatterns = [
    path('homepage/', views.homepage, name='Homepage_homepage'),
    path('login_account/', views.login_account, name='Homepage_login_account'),
    path('login_password/<role_user>/<user_name_login>', views.login_password, name='Homepage_login_password')
]