# Generated by Django 4.1.7 on 2023-09-18 08:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Homepage', '0012_alter_detail_order_tb_status_order'),
    ]

    operations = [
        migrations.AlterField(
            model_name='detail_order_tb',
            name='date_create',
            field=models.DateTimeField(),
        ),
    ]
