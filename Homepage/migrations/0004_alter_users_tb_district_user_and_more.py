# Generated by Django 4.1.7 on 2023-09-11 08:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Homepage', '0003_alter_users_tb_country_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='users_tb',
            name='district_user',
            field=models.CharField(blank=True, max_length=45, null=True),
        ),
        migrations.AlterField(
            model_name='users_tb',
            name='province_user',
            field=models.CharField(blank=True, max_length=45, null=True),
        ),
        migrations.AlterField(
            model_name='users_tb',
            name='ward_user',
            field=models.CharField(blank=True, max_length=45, null=True),
        ),
    ]
