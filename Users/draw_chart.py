import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
import base64
import io 

def drawChartStaticUser(lstLabels, lstPercent):
    buffer = io.BytesIO()
    plt.figure()

    plt.pie(lstPercent, shadow = True, pctdistance = 0.5, labeldistance = 1.1, explode = [0.1, 0.1, 0.1, 0.1], 
        autopct = f'%0.0f%%', labels = lstLabels)
    plt.legend(bbox_to_anchor=(1.2, 1))
    plt.savefig(buffer, format = 'png', bbox_inches='tight')

    buffer.seek(0)
    plt.close()
    pie_chart = base64.b64encode(buffer.getvalue()).decode()

    return pie_chart

def drawChartUserStaticSummary(lstXBarChart, lstYBarChart):
    buffer = io.BytesIO()
    plt.figure(figsize = (11,6))

    plt.bar(lstXBarChart, lstYBarChart)
    for i, value in enumerate(lstYBarChart):
        plt.text(i, value, str(value), ha = 'center', va = 'bottom')
    
    plt.xlabel('Status Order')
    plt.ylabel('Number Of Order')
    plt.savefig(buffer, format = 'png', bbox_inches='tight')
    
    buffer.seek(0)
    plt.close()
    bar_chart = base64.b64encode(buffer.getvalue()).decode()

    return bar_chart