from django import template

register = template.Library()

@register.filter(name = 'get_list_item')
def get_list_item(lst, index):
    return lst[index]

