from django import template

register = template.Library()

@register.simple_tag
def loop_range(start_para, end_para, step_para):
    return range(start_para, end_para, step_para)