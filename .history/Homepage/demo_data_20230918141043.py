# File này không được sử dụng trong thực tế. File này chỉ sử dụng trong quá trình phát triển
# Dùng để tạo các dữ liệu mẫu trong database, chỉ mục đích test web.

import random
import datetime 

# Khi tạo dữ liệu cần tạo các dữ liệu giả, dùng thư viện Faker có sẵn để tạo dữ liệu giả cho nhanh
from faker import Faker
from .models import *


# Hàm này tạo 1000 người gửi hàng ( sender ), 1000 hàng trong bảng shippingAddr
def ins_1000_sender():
    fake_information = Faker()

    for index in range(0, 1000, 1):
        full_name = fake_information.name()
        # giả sử trong bảng users tương ứng có 4 record có id như list sau
        lst_id_users = [3, 4, 5 , 6]
        tel_num = fake_information.phone_number()
        country = 'VN'
        random_number_division = random.randint(1, 10599)
        get_division = Division_tb.objects.filter(id = random_number_division).values()[0]
        province_division = get_division['province']
        district_division = get_division['district']
        ward_division = get_division['ward']

        instance_id_user = Users_tb.objects.get(pk = random.choice(lst_id_users))

        ins_sender = Shipping_Addr_tb(id_users = instance_id_user, name = full_name, tel_num = tel_num, country = 'VN', province = province_division, district = district_division, ward = ward_division, type_addr = 'home', note = '', status_addr = 'on', latitude = '', longitude = '')
        ins_sender.save()

def ins_1000_receiver():
    fake_information = Faker()

    for index in range(0, 1000, 1):
        full_name = fake_information.name()
        # giả sử trong bảng users tương ứng có 4 record có id như list sau
        lst_id_users = [3, 4, 5 , 6]
        tel_num = fake_information.phone_number()
        country = 'VN'
        random_number_division = random.randint(1, 10599)
        get_division = Division_tb.objects.filter(id = random_number_division).values()[0]
        province_division = get_division['province']
        district_division = get_division['district']
        ward_division = get_division['ward']

        instance_id_user = Users_tb.objects.get(pk = random.choice(lst_id_users))
        
        ins_receiver = Delivery_Addr_tb(id_users = instance_id_user, name = full_name, tel_num = tel_num, country = 'VN', province = province_division, district = district_division, ward = ward_division, type_addr = 'home', note = '', status_addr = 'on', latitude = '', longitude = '')
        ins_receiver.save()

def ins_1000_order():
    fake_information = Faker()

    for index in range(0, 1000):
        lst_id_user = [3, 4, 5 , 6]
        id_shipping_addr = random.randint(1, 1006)
        id_delivery_addr = random.randint(1, 1006)

        get_tel_sender = Shipping_Addr_tb.objects.filter(id = id_shipping_addr).values('tel_num')[0]['tel_num']
        get_tel_receiver = Delivery_Addr_tb.objects.filter(id = id_delivery_addr).values('tel_num')[0]['tel_num']

        order_code_random = datetime.datetime.now().strftime(f"%Y%m%d%H%M%S") + 'User' + str(random.choice(lst_id_user)) + dict_para_exec['telNumSender'] + dict_para_exec['telNumReceiver']