from django.shortcuts import render

def homepage(request):
    return render(request, 'Homepage/homepage.html')

def login_account(request):
    return render(request, 'Homepage/login_account.html')

def login_password(request):
    return render(request, 'Homepage/login_password.html')
