from django.shortcuts import render, redirect
from . import exec_data

def homepage(request):
    return render(request, 'Homepage/homepage.html')

def login_account(request):
    if 'user_name_login_ddelivery' in request.session:
        print('a')
    else:
        if request.method == 'POST':
            get_user_name = request.POST.get('input_username', None)
            result = ()
            role_user = ''

            if get_user_name[0] == 'U':
                role_user = 'users'
            elif get_user_name[0] == 'D' and get_user_name[1] != 'S':
                role_user = 'driver'
            elif get_user_name[0:2] == 'DS':
                role_user = 'ds'
            
            result = exec_data.check_login_account(role_user, get_user_name)

            if result != None:
                request.session['status_login_account'] = 'ok'
                return redirect('login_password', role_user = role_user, user_name_login = get_user_name)
            else:
                return render(request, 'Homepage/login_account.html', {'login_error' : 'Tên đăng nhập sai'})

    return render(request, 'Homepage/login_account.html')

def login_password(request, role_user, user_name_login):
    return render(request, 'Homepage/login_password.html')
