from django.contrib.sessions.models import Session
import jwt 
import os
import datetime

class check_login_before_views:
    def __init__(self, get_response):
        self.get_response = get_response
    
    def __call__(self, request):
        session_middleware = SessionMiddleware()
        session_middleware.process_request(request)

        # if 'token_ddelivery_web_demo' not in request.session.items():
        #     print('session session')
        # else:
        #     load_dotenv()
        #     secret_key = os.getenv('secret_key')
        #     decode_token = jwt.decode(request.session['token_ddelivery_web_demo'], secret_key, algorithms = ["HS256"])
        #     current_time = datetime.datetime.now()
        #     end_time_token = datetime.datetime.strptime(decode_token['endTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')
        #     if current_time < end_time_token:
        #         print('session session session session')
