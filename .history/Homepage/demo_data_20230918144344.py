# File này không được sử dụng trong thực tế. File này chỉ sử dụng trong quá trình phát triển
# Dùng để tạo các dữ liệu mẫu trong database, chỉ mục đích test web.

import random
import datetime 

# Khi tạo dữ liệu cần tạo các dữ liệu giả, dùng thư viện Faker có sẵn để tạo dữ liệu giả cho nhanh
from faker import Faker
from .models import *


# Hàm này tạo 1000 người gửi hàng ( sender ), 1000 hàng trong bảng shippingAddr
def ins_1000_sender():
    fake_information = Faker()

    for index in range(0, 1000, 1):
        full_name = fake_information.name()
        # giả sử trong bảng users tương ứng có 4 record có id như list sau
        lst_id_users = [3, 4, 5 , 6]
        tel_num = fake_information.phone_number()
        country = 'VN'
        random_number_division = random.randint(1, 10599)
        get_division = Division_tb.objects.filter(id = random_number_division).values()[0]
        province_division = get_division['province']
        district_division = get_division['district']
        ward_division = get_division['ward']

        instance_id_user = Users_tb.objects.get(pk = random.choice(lst_id_users))

        ins_sender = Shipping_Addr_tb(id_users = instance_id_user, name = full_name, tel_num = tel_num, country = 'VN', province = province_division, district = district_division, ward = ward_division, type_addr = 'home', note = '', status_addr = 'on', latitude = '', longitude = '')
        ins_sender.save()

def ins_1000_receiver():
    fake_information = Faker()

    for index in range(0, 1000, 1):
        full_name = fake_information.name()
        # giả sử trong bảng users tương ứng có 4 record có id như list sau
        lst_id_users = [3, 4, 5 , 6]
        tel_num = fake_information.phone_number()
        country = 'VN'
        random_number_division = random.randint(1, 10599)
        get_division = Division_tb.objects.filter(id = random_number_division).values()[0]
        province_division = get_division['province']
        district_division = get_division['district']
        ward_division = get_division['ward']

        instance_id_user = Users_tb.objects.get(pk = random.choice(lst_id_users))
        
        ins_receiver = Delivery_Addr_tb(id_users = instance_id_user, name = full_name, tel_num = tel_num, country = 'VN', province = province_division, district = district_division, ward = ward_division, type_addr = 'home', note = '', status_addr = 'on', latitude = '', longitude = '')
        ins_receiver.save()

def ins_1000_order():
    fake_information = Faker()

    for index in range(0, 1000):
        lst_id_user = [3, 4, 5 , 6]
        status_order_str = 'Pending'

        random_id_users = random.choice(lst_id_user)
        id_shipping_addr = random.randint(1, 1006)
        id_delivery_addr = random.randint(1, 1006)

        get_tel_sender = Shipping_Addr_tb.objects.filter(id = id_shipping_addr).values('tel_num')[0]['tel_num']
        get_tel_receiver = Delivery_Addr_tb.objects.filter(id = id_delivery_addr).values('tel_num')[0]['tel_num']

        order_code_random = datetime.datetime.now().strftime(f"%Y%m%d%H%M%S") + 'User' + str(random_id_users) + get_tel_sender + get_tel_receiver

        start_day = datetime.datetime(2020, 1, 1)
        end_day = datetime.datetime(2025, 12, 30)
        days_between = (end_day - start_day).days 
        random_days = random.randint(10, days_between)

        date_create_order = start_day + datetime.timedelta(days = random_days)
        departure_time_order = date_create_order + datetime.timedelta(days = 1)
        estimate_time_arrive = departure_time_order + datetime.timedelta(days = 10)
        date_now = datetime.datetime.now()

        if (date_now >= estimate_time_arrive):
           status_order_str = 'Success'
        elif (date_now < estimate_time_arrive):
            if ((estimate_time_arrive - date_now).days < 10):
                status_order_str = 'Delivery'

        shipping_cost_order = random.randint(1000, 1000000)
        cod_cost_order =  random.randint(1000, 1000000)

        # giả sử ta có id ds là 1, giả sử ta có id của driver là 2
        instance_id_ds = DS_tb.objects.get(pk = 1)
        instance_id_user = Users_tb.objects.get(pk = random_id_users)
        instance_id_delivery_addr = Delivery_Addr_tb.objects.get(pk = id_delivery_addr)
        instance_id_shipping_addr = Shipping_Addr_tb.objects.get(pk = id_shipping_addr)
        instance_id_driver = Driver_tb.objects.get(pk = 2)

        lst_payer = ['sender', 'receiver']

        ins_order_delivery = Order_Delivery_tb(id_users = instance_id_user, code_order_delivery = order_code_random)
        ins_order_delivery.save()
        ins_detail_order = Detail_Order_tb(code_order_delivery = order_code_random, id_delivery_addr = instance_id_delivery_addr, id_shipping_addr = instance_id_shipping_addr, id_driver = instance_id_driver, date_create = date_create_order, departure_time = departure_time_order, estimate_time_arrive = estimate_time_arrive, total_cost = (shipping_cost_order + cod_cost_order), note = '', )

