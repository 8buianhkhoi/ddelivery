# File này không được sử dụng trong thực tế. File này chỉ sử dụng trong quá trình phát triển
# Dùng để tạo các dữ liệu mẫu trong database, chỉ mục đích test web.

from .models import *

# Hàm này tạo 100 người gửi hàng ( sender ), 100 hàng trong bảng shippingAddr