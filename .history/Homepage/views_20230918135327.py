from django.shortcuts import render, redirect
import datetime
import hashlib
import os
import base64
from dotenv import load_dotenv
import jwt

from . import exec_data
from . import demo_data

# Normal function, not route in django. This function help hash password for secure
def hashPassword(password, salt=None, iterations= 100000, key_length =32):
    key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, iterations, dklen=key_length)
    
    return salt + key


def homepage(request):
    demo_data.ins_1000_sender()
    return render(request, 'Homepage/homepage.html')


def login_password(request, role_user, user_name_login):
    if 'status_login_account' not in request.session:
        return redirect('homepage_app:Homepage_login_account')
    else:
        if request.method == 'POST':
            load_dotenv()
            secret_key = os.getenv('secret_key')

            get_password_input = request.POST.get('input_pass', None)
            hash_bytes = hashPassword(get_password_input, secret_key.encode())
            get_password_input = base64.b64encode(hash_bytes).decode('utf-8')
            
            check_password_db = exec_data.check_login_password(role_user_para = role_user, password_input_para = get_password_input, user_name_para = user_name_login)

            if check_password_db is not None:
                request.session.clear()

                startTimeTokenStr = datetime.datetime.now().strftime(f'%d-%m-%Y %H:%M:%S')
                endTimeTokenStr = (datetime.datetime.now() + datetime.timedelta(days = 2)).strftime(f'%d-%m-%Y %H:%M:%S')
                dictTokenStr = {'roleUser' : role_user, 'idRoleUser' : check_password_db[0]['id'], 'startTimeTokenStr' : startTimeTokenStr, 'endTimeTokenStr' : endTimeTokenStr}

                request.session['token_ddelivery_web_demo'] = jwt.encode(dictTokenStr, secret_key, algorithm='HS256')
            
                if role_user == 'users':
                    request.session['role_user_ddelivery'] = role_user
                    request.session['id_role_user_ddelivery'] = check_password_db[0]['id']
                    return redirect('Users_app:Users_base_user_page')
                elif role_user == 'driver':
                    pass 
                elif role_user == 'ds':
                    pass

            else:
                return render(request, 'Homepage/login_password.html', {'login_error' : 'Mật khẩu đăng nhập sai'})

        return render(request, 'Homepage/login_password.html')
    
def login_account(request):
    if 'user_name_login_ddelivery' in request.session:
        role_user_login = request.session['user_name_login_ddelivery']
        # TODO code thêm chức năng, khi đã đăng nhập rồi mà vô login thì chuyển sang trang tương ứng
    else:
        if request.method == 'POST':
            get_user_name = request.POST.get('input_username', None)
            result = ()
            role_user = ''

            if get_user_name[0] == 'U':
                role_user = 'users'
            elif get_user_name[0] == 'D' and get_user_name[1] != 'S':
                role_user = 'driver'
            elif get_user_name[0:2] == 'DS':
                role_user = 'ds'
            
            result = exec_data.check_login_account(role_user, get_user_name)

            if result != None:
                request.session['status_login_account'] = 'ok'
                return redirect('homepage_app:Homepage_login_password', role_user = role_user, user_name_login = get_user_name)
            else:
                return render(request, 'Homepage/login_account.html', {'login_error' : 'Tên đăng nhập sai'})

    return render(request, 'Homepage/login_account.html')
