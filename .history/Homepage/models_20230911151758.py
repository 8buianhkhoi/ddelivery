from django.db import models

class users(models.Model):
    user_name = models.CharField(max_length = 45)
    password_user = models.CharField(max_length = 255)
    full_name_user = models.CharField(max_length = 255)
    tel_user  = models.CharField(max_length = 45)
    gmail_user = models.CharField(max_length = 45)
    note_user = models.CharField(max_length = 1000)
    status_user = models.CharField(max_length = 45)
    country_user = models.CharField(max_length = 45)
    province_user = models.CharField(max_length = 45)
    district_user = models.CharField(max_length = 45)
    ward_user = models.CharField(max_length = 45)
    gender_user = models.CharField(max_length = 45, choices = [('men', 'men'), ('woman', 'woman'), ('other', 'other')])

