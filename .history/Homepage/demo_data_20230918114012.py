# File này không được sử dụng trong thực tế. File này chỉ sử dụng trong quá trình phát triển
# Dùng để tạo các dữ liệu mẫu trong database, chỉ mục đích test web.

# Khi tạo dữ liệu cần tạo các dữ liệu giả, dùng thư viện Faker có sẵn để tạo dữ liệu giả cho nhanh
from faker import Faker
# from .models import *

# Hàm này tạo 100 người gửi hàng ( sender ), 100 hàng trong bảng shippingAddr
def ins_100_sender():
    fake_information = Faker()
    print(fake_information.name())
ins_100_sender()
