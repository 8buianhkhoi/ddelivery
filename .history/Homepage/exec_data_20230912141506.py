from . import models

def check_login_account(role_user_para, user_name_para):
    result_check = ''

    if role_user_para == 'users':
        result_check = models.Users_tb.objects.filter(user_name = user_name_para)
    elif role_user_para == 'driver':
        result_check = models.Driver_tb.objects.filter(driver_name = user_name_para)
    elif role_user_para == 'ds':
        result_check = models.DS_tb.objects.filter(DS_name = user_name_para)
    else:
        result_check = None
    
    return result_check

def check_login_password(role_user_para, password_input_para, user_name_input_para):
    result_check = ''

    if role_user_para == 'users':
        result_check = models.Users_tb.objects.filter(user_name = user_name_para) 
    elif role_user_para == 'driver':
        result_check = models.Driver_tb.objects.filter(driver_name = user_name_para) 
    elif role_user_para == 'ds':
        result_check = models.DS_tb.objects.filter(DS_name = user_name_para) 
    else:
        result_check = None

