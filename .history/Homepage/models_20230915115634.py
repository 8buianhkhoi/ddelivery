from django.db import models

class Users_tb(models.Model):
    user_name = models.CharField(max_length = 45)
    password_user = models.CharField(max_length = 255)
    full_name_user = models.CharField(max_length = 255)
    tel_user  = models.CharField(max_length = 45)
    gmail_user = models.CharField(max_length = 45)
    note_user = models.CharField(max_length = 1000)
    status_user = models.CharField(max_length = 45, choices = [('on', 'on'), ('off', 'off')])
    country_user = models.CharField(max_length = 45, blank = True, null = True)
    province_user = models.CharField(max_length = 45, blank = True, null = True)
    district_user = models.CharField(max_length = 45, blank = True, null = True)
    ward_user = models.CharField(max_length = 45, blank = True, null = True)
    gender_user = models.CharField(max_length = 45, choices = [('men', 'men'), ('woman', 'woman'), ('other', 'other')], blank = True, null = True)

class DS_tb(models.Model):
    DS_name = models.CharField(max_length = 45)
    password_DS = models.CharField(max_length = 255)
    full_name_DS = models.CharField(max_length = 255)
    tel_DS = models.CharField(max_length = 45)
    gmail_DS = models.CharField(max_length = 45)
    status_DS = models.CharField(max_length = 45, choices = [('on', 'on'), ('off', 'off')])
    note_DS = models.CharField(max_length = 1000)

class Driver_tb(models.Model):
    driver_name = models.CharField(max_length = 45)
    password_driver = models.CharField(max_length = 255)
    id_DS_FK = models.ForeignKey(DS_tb, on_delete = models.CASCADE)
    full_name_driver = models.CharField(max_length = 255)
    tel_driver = models.CharField(max_length = 45)
    gmail_driver = models.CharField(max_length = 45)
    status_driver = models.CharField(max_length = 45, choices = [('on', 'on'), ('off', 'off')])
    note_driver = models.CharField(max_length = 1000, blank = True, null = True)
    country_driver = models.CharField(max_length = 45, blank = True, null = True)
    province_driver = models.CharField(max_length = 45, blank = True, null = True)
    district_driver = models.CharField(max_length = 45, blank = True, null = True)
    ward_driver = models.CharField(max_length = 45, blank = True, null = True)
    gender_driver = models.CharField(max_length = 45, choices = [('on', 'on'), ('off', 'off'), ('other', 'other')], blank = True, null = True)
    cin_driver = models.CharField(max_length = 45, blank = True, null = True)

class Division_tb(models.Model):
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    latitude = models.CharField(max_length = 255)
    longitude = models.CharField(max_length = 255)

class Delivery_Addr_tb(models.Model):
    id_users = models.ForeignKey(Users_tb, on_delete = models.CASCADE, db_column = 'id_users')
    name = models.CharField(max_length = 255)
    tel_num = models.CharField(max_length = 255)
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    type_addr = models.CharField(max_length = 255, choices = [('home', 'home'),])
    note = models.CharField(max_length = 1000)
    status_addr = models.CharField(max_length = 255, choices = [('on', 'on'), ('off', 'off')])
    latitude = models.CharField(max_length = 255)
    longitude = models.CharField(max_length = 255)

class Shipping_Addr_tb(models.Model):
    id_users = models.ForeignKey(Users_tb, on_delete = models.CASCADE, db_column = 'id_users')
    name = models.CharField(max_length = 255)
    tel_num = models.CharField(max_length = 255)
    country = models.CharField(max_length = 255)
    province = models.CharField(max_length = 255)
    district = models.CharField(max_length = 255)
    ward = models.CharField(max_length = 255)
    type_addr = models.CharField(max_length = 255, choices = [('home', 'home'),])
    note = models.CharField(max_length = 1000)
    status_addr = models.CharField(max_length = 255, choices = [('on', 'on'), ('off', 'off')])
    latitude = models.CharField(max_length = 255)
    longitude = models.CharField(max_length = 255)

class Order_Delivery_tb(models.Model):
    id_users = models.ForeignKey(Users_tb, on_delete = models.CASCADE, db_column = 'id_users')
    code_order_delivery = models.CharField(max_length = 255)



