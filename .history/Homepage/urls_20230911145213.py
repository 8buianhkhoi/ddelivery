from django.urls import path
from . import views

urlpatterns = [
    path('homepage/', views.homepage, name='Homepage_homepage'),
    path('login_account/', views.login_account, name='Homepage_login_account')
]