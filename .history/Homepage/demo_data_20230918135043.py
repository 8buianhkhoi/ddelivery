# File này không được sử dụng trong thực tế. File này chỉ sử dụng trong quá trình phát triển
# Dùng để tạo các dữ liệu mẫu trong database, chỉ mục đích test web.

import random

# Khi tạo dữ liệu cần tạo các dữ liệu giả, dùng thư viện Faker có sẵn để tạo dữ liệu giả cho nhanh
from faker import Faker
from .models import *


# Hàm này tạo 100 người gửi hàng ( sender ), 100 hàng trong bảng shippingAddr
def ins_100_sender():
    fake_information = Faker()

    for index in range(0, 100, 1):
        full_name = fake_information.name()
        # giả sử trong bảng users tương ứng có 4 record có id như list sau
        lst_id_users = [3, 4, 5 , 6]
        tel_num = fake_information.phone_number()
        country = 'VN'
        random_number_division = random.randint(1, 10599)
        get_division = Division_tb.objects.filter(id = random_number_division).values()[0]
        province_division = get_division['province']
        district_division = get_division['district']
        ward_division = get_division['ward']

        ins_sender = Shipping_Addr_tb(id_users = random.choice(lst_id_users), name = full_name, tel_num = tel_num, country = 'VN', province = province_division, district = district_division, ward = ward_division, type_addr = 'home', note = '', status_addr = 'on', latitude = '', longitude = '')
        ins_sender.save()


