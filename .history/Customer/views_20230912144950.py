from django.shortcuts import render

# Create your views here.
def homepage_customer(request):
    return render(request, 'Customer/homepage_customer.html')
