from django.urls import path

from . import views 

app_name = 'customer_app'

urlpatterns = [
    path('homepage/', views.homepage_customer, name='Customer_homepage')
]