from django.shortcuts import render
from django.contrib.auth.hashers import make_password

def base_user_page(request):
    print(request.session.items())
    return render(request, 'Users/base_users_page.html')
