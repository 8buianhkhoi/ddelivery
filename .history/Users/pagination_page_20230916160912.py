# Function cal_pagination giúp tính toán điểm và phân trang sản phẩm. Khi views có danh sách sản phẩm. Cần hiển thị phân trang
# Ta truyền vào số trang hiện tại đang hiển thị ( mặc định là 1 ), giới hạn mỗi trang và độ dài của tất cả order đang có.

def cal_pagination(current_page, limit_show, length_all_order):
    pagination_page = -(- length_all_order // limit_show)
    start_index = (int(current_page) - 1) * limit_show
    end_index = min(start_index + limit_show, length_all_order - 1)

    return [pagination_page, start_index, end_index]

