from django.utils import timezone
import datetime
import hashlib
import base64

from Homepage.models import *
from . import pagination_page

limit_show_order = 30

def hashPassword(password, salt=None, iterations= 100000, key_length =32):
    key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, iterations, dklen=key_length)
    
    return salt + key 

# TODO code here
def get_name_user_by_id(id_user_para):
    pass

def getAllDS():
    all_ds = DS_tb.objects.filter(status_DS = 'on').values()
    return all_ds

def get_all_province():
    all_province = Division_tb.objects.values('province').distinct().order_by('province')
    return all_province

def get_all_district_by_name_province(name_province_para):
    all_district = Division_tb.objects.filter(province = name_province_para).values('district').distinct().order_by('district')
    return all_district

def get_all_ward_by_name_province_name_district(name_province_para, name_district_para):
    all_ward = Division_tb.objects.filter(province = name_province_para, district = name_district_para).values('ward').distinct().order_by('ward')
    return all_ward

def exec_users_send_product(id_user_para, order_code_random_para ,**kwargs): 
    instance_id_user = Users_tb.objects.get(pk = id_user_para)
    instance_id_ds = DS_tb.objects.get(pk = kwargs['chooseDS']) 

    ins_shipping_addr = Shipping_Addr_tb(
        id_users = instance_id_user,
        name = kwargs['nameSender'],
        tel_num = kwargs['telNumSender'],
        country = 'VN',
        province = kwargs['provinceAddrSender'],
        district = kwargs['districtAddrSender'],
        ward = kwargs['wardAddrSender'],
        type_addr = kwargs['typeAddrSender'],
        note = kwargs['noteAddrSender'],
        status_addr = 'on',
        latitude = '',
        longitude = ''
    )

    ins_shipping_addr.save()

    ins_delivery_addr = Delivery_Addr_tb(
        id_users = instance_id_user,
        name = kwargs['nameReceiver'],
        tel_num = kwargs['telNumReceiver'],
        country = 'VN',
        province = kwargs['provinceAddrReceiver'],
        district = kwargs['districtAddrReceiver'],
        ward = kwargs['wardAddrReceiver'],
        type_addr = kwargs['typeAddrReceiver'],
        note = kwargs['noteAddrReceiver'],
        status_addr = 'on',
        latitude = '',
        longitude = ''
    )

    ins_delivery_addr.save()

    ins_order = Order_Delivery_tb(
        id_users = instance_id_user,
        code_order_delivery = order_code_random_para
    )
    ins_order.save()

    instance_delivery_addr = Delivery_Addr_tb.objects.get(pk = ins_delivery_addr.id)
    instance_shipping_addr = Shipping_Addr_tb.objects.get(pk = ins_shipping_addr.id)

    ins_detail_order = Detail_Order_tb(
        code_order_delivery = order_code_random_para,
        id_delivery_addr = instance_delivery_addr,
        id_shipping_addr = instance_shipping_addr,
        id_driver = None,
        departure_time = (datetime.datetime.now() + datetime.timedelta(hours=1)).strftime(f"%Y-%m-%d %H:%M:%S"),
        estimate_time_arrive = (datetime.datetime.now() + datetime.timedelta(hours=100)).strftime(f"%Y-%m-%d %H:%M:%S"),
        total_cost = str(int(kwargs['shippingCost']) + int(kwargs['codCost'])),
        note = None,
        status_order = 'Pending',
        id_delivery_system = instance_id_ds,
        real_time_arrive = None,
        id_users = instance_id_user,
        shipping_cost = kwargs['shippingCost'],
        cod_cost = kwargs['codCost'],
        payer = kwargs['whoPayCost']
    )
    ins_detail_order.save()
    
    instance_detail_order = Detail_Order_tb.objects.get(pk = ins_detail_order.id)

    # Each list contain : productName, typeProduct, quantityProduct, weightProduct, sizeProduct, descriptionProduct
    for index in kwargs['lstAllProduct']:
        ins_product = Product_tb(
            id_detail_order = instance_detail_order,
            name_product = index[0],
            description_product = index[5],
            quantity = index[2],
            weight = index[3],
            type_product = index[1],
            size_product = index[4],
            cost_product = '30000'
        )
        ins_product.save()

def get_return_order(id_user_para, current_page_para):
    length_all_return_order = Detail_Order_tb.objects.filter(id_users = id_user_para, status_order = 'Return-Order').count()
    calc_pagination = pagination_page.cal_pagination(current_page_para, limit_show_order, length_all_return_order)

    return_order = Detail_Order_tb.objects.filter(id_users = id_user_para, status_order = 'Return-Order').all()[calc_pagination[1]:calc_pagination[1] + limit_show_order].values()
    
    return {'return_order' : return_order, 'length_all_return_order' : length_all_return_order, 'pagination' : calc_pagination[0]}

def exec_search_order(id_user_para, code_order_para):
    get_order = Detail_Order_tb.objects.filter(id_users = id_user_para, code_order_delivery = code_order_para).values()
    return get_order

def get_inform_users(id_user_para):
    user_inform = Users_tb.objects.filter(id = id_user_para).values()
    return user_inform

def update_basic_inform_user(id_user_para, name_user_para, tel_user_para, gmail_user_para, gender_user_para):
    try:
        if name_user_para != '':
            Users_tb.objects.filter(id = id_user_para).update(full_name_user = name_user_para)
        if tel_user_para != '':
            Users_tb.objects.filter(id = id_user_para).update(tel_user = tel_user_para)
        if gmail_user_para != '':
            Users_tb.objects.filter(id = id_user_para).update(gmail_user = gmail_user_para)
        if gender_user_para != '':
            Users_tb.objects.filter(id = id_user_para).update(gender_user = gender_user_para)
        return 'OK'
    except:
        return 'Fall'

def update_advanced_inform_user(id_user_para, province_user_para, district_user_para, ward_user_para):
    try:
        Users_tb.objects.filter(id = id_user_para).update(province_user = province_user_para, district_user = district_user_para, ward_user = ward_user_para)
        return 'OK'
    except:
        return 'Fall'

def update_password_user(id_user_para, password_user, secret_Key):
    try:
        hash_bytes = hashPassword(password_user, secret_Key.encode())
        hash_password = base64.b64encode(hash_bytes).decode('utf-8')

        Users_tb.objects.filter(id = id_user_para).update(password_user = hash_password)

        return 'OK'
    except:
        return 'Fall'


def exec_user_static_show_all_order(id_user_para, current_page_para):
    try:
        dict_driver = {}
        query_order = Detail_Order_tb.objects.filter(id_users = id_user_para)
        len_all_order = query_order.count()

        calc_pagination = pagination_page.cal_pagination(current_page_para, limit_show_order, len_all_order)

        all_order_user = query_order.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        
        for index in all_order_user:
            if index['status_order'] == 'Delivery' or index['status_order'] == 'Success':
                id_driver_temp = index['id_driver_id']
                get_name_driver = Driver_tb.objects.filter(id = id_driver_temp).values('full_name_driver')
                dict_driver[index['code_order_delivery']] = get_name_driver[0]['full_name_driver']
            elif index['status_order'] == 'Need-Delivery':
                id_driver_temp = index['id_driver_id']
                get_name_driver = Driver_tb.objects.filter(id = id_driver_temp).values('full_name_driver')
                dict_driver[index['code_order_delivery']] = 'Waiting accept from driver:' + get_name_driver[0]['full_name_driver']
            else:
                dict_driver[index['code_order_delivery']] = 'None'

        return [dict_driver, all_order_user, calc_pagination[0]]
    except:
        return False
    
def get_user_static_order_by_date(dateNow, id_user, CurrentPage):
    try:
        date_time_now = timezone.datetime.strptime(dateNow, f'%Y-%m-%d').date()
        query_get_order_date = Detail_Order_tb.objects.filter(id_users_id = id_user, date_create__date = date_time_now)

        len_all_order_date = query_get_order_date.count()
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order_date)

        all_order_date = query_get_order_date.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], all_order_date]
    except:
        return False

def get_user_static_order_success(id_user, CurrentPage):
    try:
        query_order = Detail_Order_tb.objects.filter(id_users_id = id_user, status_order = 'Success')
        len_all_order_success = query_order.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order_success)
        all_order_success = query_order.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        
        return [calc_pagination[0], all_order_success]
    except:
        return False

def get_user_static_order_pending(id_user, CurrentPage):
    try:
        query_order = Detail_Order_tb.objects.filter(id_users = id_user, status_order = 'Pending')
        len_all_order_pending = conn.execute(query_order).rowcount

        calcPagination = paginationPageUser.calPagination(CurrentPage, limitShowOrder, len_all_order_pending) 

        all_order_pending = conn.execute(query_order.offset(calcPagination[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()
        
        return [calcPagination[0], all_order_pending]
    except:
        return False