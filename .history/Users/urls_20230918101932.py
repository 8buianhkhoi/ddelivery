from django.urls import path

from . import views 

app_name = 'Users_app'

urlpatterns = [
    path('homepage/', views.base_user_page, name='Users_base_user_page'),
    path('send-product/', views.users_send_product, name='Users_send_product_page'),
    path('user-page/get-address/get-province/', views.userGetAllProvince, name='Users_get_all_province'),
    path('user-page/get-address/get-district/', views.userGetDistrictFromProvince, name='Users_get_all_district'),
    path('user-page/get-address/get-wards/', views.userGetWardFromDistrict, name='Users_get_all_ward'),
    path('return-order/<int:current_page>', views.return_order, name='Users_return_order'),
    path('logout/', views.users_logout, name = 'Users_log_out'),
    path('search-order/', views.users_search_order, name = 'Users_search_order'),
    path('personal-profile-homepage/', views.base_personal_profile, name = 'Users_personal_profile_homepage'),
    path('show-all-order/<int:current_page>/', views.user_static_show_all_order, name = 'Users_show_all_order'),
    path('static-by-date/<int:current_page>/<date_now>/', views.user_static_order_by_date, name='Users_static_order_by_date'),
    path('static-order-success/<int:current_page>/', views.user_static_order_success, name = 'Users_static_order_success'),
    path('static-order-pending/<int:current_page>/', views.user_static_order_pending, name = 'Users_static_order_pending'),
    path('static-order-delivery/<int:current_page>/', views.user_static_order_delivery, name = 'Users_static_order_delivery'),
    path('static-order-need-deliver/<int:current_page>/', views.user_static_order_need_delivery, name = 'Users_static_order_need_delivery')
]