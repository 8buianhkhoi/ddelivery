import datetime

from Homepage.models import *

# TODO code here
def get_name_user_by_id(id_user_para):
    pass

def getAllDS():
    all_ds = DS_tb.objects.filter(status_DS = 'on').values()
    return all_ds

def get_all_province():
    all_province = Division_tb.objects.values('province').distinct().order_by('province')
    return all_province

def get_all_district_by_name_province(name_province_para):
    all_district = Division_tb.objects.filter(province = name_province_para).values('district').distinct().order_by('district')
    return all_district

def get_all_ward_by_name_province_name_district(name_province_para, name_district_para):
    all_ward = Division_tb.objects.filter(province = name_province_para, district = name_district_para).values('ward').distinct().order_by('ward')
    return all_ward

def exec_users_send_product(id_user_para, order_code_random_para ,**kwargs): 
    instance_id_user = Users_tb.objects.get(pk = id_user_para)
    instance_id_ds = DS_tb.objects.get(pk = kwargs['chooseDS']) 

    ins_shipping_addr = Shipping_Addr_tb(
        id_users = instance_id_user,
        name = kwargs['nameSender'],
        tel_num = kwargs['telNumSender'],
        country = 'VN',
        province = kwargs['provinceAddrSender'],
        district = kwargs['districtAddrSender'],
        ward = kwargs['wardAddrSender'],
        type_addr = kwargs['typeAddrSender'],
        note = kwargs['noteAddrSender'],
        status_addr = 'on',
        latitude = '',
        longitude = ''
    )

    ins_shipping_addr.save()

    ins_delivery_addr = Delivery_Addr_tb(
        id_users = instance_id_user,
        name = kwargs['nameReceiver'],
        tel_num = kwargs['telNumReceiver'],
        country = 'VN',
        province = kwargs['provinceAddrReceiver'],
        district = kwargs['districtAddrReceiver'],
        ward = kwargs['wardAddrReceiver'],
        type_addr = kwargs['typeAddrReceiver'],
        note = kwargs['noteAddrReceiver'],
        status_addr = 'on',
        latitude = '',
        longitude = ''
    )

    ins_delivery_addr.save()

    ins_order = Order_Delivery_tb(
        id_users = instance_id_user,
        code_order_delivery = order_code_random_para
    )
    ins_order.save()
    
    print(ins_delivery_addr.id)
    ins_detail_order = Detail_Order_tb(
        code_order_delivery = order_code_random_para,
        id_delivery_addr = ins_delivery_addr.id,
        id_shipping_addr = ins_shipping_addr.id,
        id_driver = None,
        departure_time = (datetime.datetime.now() + datetime.timedelta(hours=1)).strftime(f"%Y-%m-%d %H:%M:%S"),
        estimate_time_arrive = (datetime.datetime.now() + datetime.timedelta(hours=100)).strftime(f"%Y-%m-%d %H:%M:%S"),
        total_cost = str(int(kwargs['shippingCost']) + int(kwargs['codCost'])),
        note = None,
        status_order = 'Pending',
        id_delivery_system = instance_id_ds,
        real_time_arrive = None,
        id_users = instance_id_user,
        shipping_cost = kwargs['shippingCost'],
        cod_cost = kwargs['codCost'],
        payer = kwargs['whoPayCost']
    )

    ins_detail_order.save()
    
    # Each list contain : productName, typeProduct, quantityProduct, weightProduct, sizeProduct, descriptionProduct
    for index in kwargs['lstAllProduct']:
        ins_product = Product_tb(
            id_detail_order = ins_detail_order.id,
            name_product = index[0],
            description_product = index[5],
            quantity = index[2],
            weight = index[3],
            type_product = index[1],
            size_product = index[4],
            cost_product = '30000'
        )
        ins_product.save()

