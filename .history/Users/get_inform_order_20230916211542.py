from Homepage.models import *

# parameter all_order_para is a QuerySet. Each element is a dict. Like QuerySet[{...}, {...}, {...}, ...]. Each dictionary element is each row Detail_Order_tb table
# This function get information about sender, receiver and product.
# TODO : viết try catch
def get_information(all_order_para):
    lst_sender, lst_receiver, lst_product = [], [], []
    for order_item in all_order_para:
        # Each item is dictionary
        sender_item = Shipping_Addr_tb.objects.filter(id = order_item['id_shipping_addr_id']).values()
        receiver_item = Delivery_Addr_tb.objects.filter(id = order_item['id_delivery_addr_id']).values()
        product_item = Product_tb.objects.filter(id = order_item['id']).all().values()

        lst_sender.append(sender_item)
        lst_receiver.append(receiver_item)
        lst_product.append(product_item)
    
    print(lst_product)
    return (lst_sender, lst_receiver, lst_product)




