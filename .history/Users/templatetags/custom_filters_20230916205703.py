from django import template

register = template.Library()

@register.filter(name = 'get_list_item')
def get_list_item(lst, index):
    return lst[index]['name']

@register.filter(name = 'get_name_sender')
def get_name_sender(lst):
    return lst['name']