from django.urls import path

from . import views 

app_name = 'Users_app'

urlpatterns = [
    path('homepage/', views.base_user_page, name='Users_base_user_page'),
    path('send-product/', views.users_send_product, name='Users_send_product_page'),
    path('user-page/get-address/get-province/', views.userGetAllProvince, name='Users_get_all_province'),
    path('user-page/get-address/get-district/', views.userGetDistrictFromProvince, name='Users_get_all_district'),
    path('user-page/get-address/get-wards/', views.userGetWardFromDistrict, name='Users_get_all_ward'),
    path('return-order/<int:current_page>', views.return_order, name='Users_return_order'),
    path('logout/', views.users_logout, name = 'Users_log_out')
]