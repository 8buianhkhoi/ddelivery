from django.shortcuts import render
from django.http import JsonResponse
from dotenv import load_dotenv
import datetime
import os
import jwt

from . import exec_data 

# This function try to check token. If token already is use. It continue browsing or redirect to homepage
def check_login_required(token_str):
    if token_str == '':
        return 'empty'
    else:
        load_dotenv()
        secret_key = os.getenv('secret_key')
        decode_token = jwt.decode(token_str, secret_key, algorithms = ["HS256"])
        current_time = datetime.datetime.now()
        end_time_token = datetime.datetime.strptime(decode_token['endTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')
        if current_time < end_time_token:
            role_user_temp = decode_token['roleUser']
            if role_user_temp == 'users':
                return {'role_user' : 'users', 'id_users' : decode_token['idRoleUser']}
            else:
                return 'NP'
        else:
            return 'ET'

# 3 function help to load and get division
def userGetAllProvince(request):
    get_all_province = exec_data.get_all_province()
    lst_all_province_temp = [item['province'] for item in get_all_province]
    dict_all_province = {'result' : lst_all_province_temp}
    return JsonResponse(dict_all_province)
    
def userGetDistrictFromProvince(request):
    name_province = request.POST.get('province', None)
    get_all_district = exec_data.get_all_district_by_name_province(name_province)
    lst_all_district = [item['district'] for item in get_all_district]
    dict_all_district = {'result' : lst_all_district}
    return JsonResponse(dict_all_district)

def userGetWardFromDistrict(request):
    name_province = request.POST.get('province', None)
    name_district = request.POST.get('district', None)
    get_all_ward = exec_data.get_all_ward_by_name_province_name_district(name_province, name_district)
    lst_all_ward = [item['ward'] for item in get_all_ward]
    dict_all_ward = {'result' : lst_all_ward}
    return JsonResponse(dict_all_ward)

# Base User Page
def base_user_page(request):
    return render(request, 'Users/base_users_page.html')

# This function show send product form.
def users_send_product(request):
    all_ds = exec_data.getAllDS()
    context_return = {'all_DS' : all_ds }

    if request.method == 'POST':
        lst_error = []
        dict_para_exec = {}
        
        dict_para_exec['idUser'] = request.session.get('id_role_user_ddelivery', None)

        # Information about sender
        dict_para_exec['nameSender'] = request.POST.get('inputSenderName', None)
        dict_para_exec['telNumSender'] = request.POST.get('inputSenderTelNum', None)
        dict_para_exec['provinceAddrSender'] = request.POST.get('chooseAddProvinceSender', None)
        dict_para_exec['districtAddrSender'] = request.POST.get('chooseAddDistrictSender', None)
        dict_para_exec['wardAddrSender'] = request.POST.get['chooseAddWardSender']
        dict_para_exec['typeAddrSender'] = request.POST.get['typeAddressSender']
        dict_para_exec['noteAddrSender'] = request.POST.get['noteAddressSender']

    return render(request, 'Users/users_send_product.html', context_return)
