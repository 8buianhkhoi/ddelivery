from django.contrib.sessions.backends.db import SessionStore
from Homepage.models import *

# TODO code here
def get_name_user_by_id(id_user_para):
    pass

def getAllDS():
    all_ds = DS_tb.objects.filter(status_DS = 'on').values()
    return all_ds

def get_all_province():
    all_province = Division_tb.objects.values('province').distinct().order_by('province')
    return all_province

def get_all_district_by_name_province(name_province_para):
    all_district = Division_tb.objects.filter(province = name_province_para).values('district').distinct().order_by('district')
    return all_district

def get_all_ward_by_name_province_name_district(name_province_para, name_district_para):
    all_ward = Division_tb.objects.filter(province = name_province_para, district = name_district_para).values('ward').distinct().order_by('ward')
    return all_ward

def exec_users_send_product(id_user_para, order_code_random_para ,**kwargs): 
    insShippingAddr = shippingAddr.insert().values(
        idShippingAddr = idShippingAddr,
        idUser = kwargs['idUser'],
        name = kwargs['nameSender'],
        telNum = kwargs['telNumSender'],
        country = kwargs['provinceAddrSender'],
        district = kwargs['districtAddrSender'],
        ward = kwargs['wardAddrSender'],
        type = kwargs['typeAddrSender'],
        note = kwargs['noteAddrSender'],
        status = 'OK',
        latitude = '',
        longitude = ''
    )

    insDeliveryAddr = deliveryAddr.insert().values(
        idDeliveryAddr = idDeliveryAddr,
        idUser = kwargs['idUser'],
        country = kwargs['provinceAddrReceiver'],
        name = kwargs['nameReceiver'],
        telNum = kwargs['telNumReceiver'],
        district = kwargs['districtAddrReceiver'],
        ward = kwargs['wardAddrReceiver'],
        type = kwargs['typeAddrReceiver'],
        note = kwargs['noteAddrReceiver'],
        status = 'OK',
        latitude = '',
        longitude = ''
    )

    insOrder = order.insert().values(
        idUser = kwargs['idUser'],
        codeOrder = order_code_random
    )

    insDetailOrder = detailOrder.insert().values(
        codeOrder = order_code_random,
        idDeliveryAddr = idDeliveryAddr,
        idShippingAddr = idShippingAddr,
        idDriver = None,
        dateCreate = datetime.datetime.now().strftime(f"%Y-%m-%d %H:%M:%S"),
        departureTime = (datetime.datetime.now() + datetime.timedelta(hours=1)).strftime(f"%Y-%m-%d %H:%M:%S"),
        estimateArrive = (datetime.datetime.now() + datetime.timedelta(hours=100)).strftime(f"%Y-%m-%d %H:%M:%S"),
        totalCost = str(int(kwargs['shippingCost']) + int(kwargs['codCost'])),
        note = None,
        status = 'Pending',
        idDeliverySystem = kwargs['chooseDS'],
        realTimeArrive = None,
        idUser = kwargs['idUser'],
        shippingCost = kwargs['shippingCost'],
        codCost = kwargs['codCost'],
        payer = kwargs['whoPayCost']
    )

    conn.execute(insShippingAddr)
    conn.execute(insDeliveryAddr)
    conn.execute(insOrder)
    conn.execute(insDetailOrder)
    
    # Each list contain : productName, typeProduct, quantityProduct, weightProduct, sizeProduct, descriptionProduct
    for index in kwargs['lstAllProduct']:
        insProduct = product.insert().values(
            idDetailOrder = idDetailOrder,
            name = index[0],
            description = index[5],
            quantity = index[2],
            weight = index[3],
            type = index[1],
            size = index[4],
            cost = '30000'
        )

        conn.execute(insProduct)
