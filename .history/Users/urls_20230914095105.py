from django.urls import path

from . import views 

app_name = 'Users_app'

urlpatterns = [
    path('homepage/', views.base_user_page, name='Users_base_user_page'),
    path('send-product/', views.users_send_product, name='Users_send_product_page')
]