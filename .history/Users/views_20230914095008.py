from django.shortcuts import render
from . import exec_data 

def base_user_page(request):
    return render(request, 'Users/base_users_page.html')

def users_send_product(request):
    all_ds = exec_data.getAllDS()

    context_return = {'all_ds' : all_ds }
    return render(request, 'Users/users_send_product.html', context_return)
