from Homepage.models import *

# parameter all_order_para is a list. Each element is a tuple. Like [(...), (...), (...), ...]. Each tuple element is each row Detail_Order_tb table
# This function get information about sender, receiver and product.
def giveInformationAboutOrder(all_order_para):
    with engine.connect() as conn:
        lstSender, lstReceiver, lstType = [], [], []
        for index in all_order_para:
            lstSender.append(conn.execute(shippingAddr.select().where(shippingAddr.c.idShippingAddr == index[3])).fetchone())
            lstReceiver.append(conn.execute(deliveryAddr.select().where(deliveryAddr.c.idDeliveryAddr == index[2])).fetchone())
            lstType.append(conn.execute(product.select().where(product.c.idDetailOrder == index[0])).fetchall())
    
        # idShippingAddr_list = [index[3] for index in all_order_para]
        # idDeliveryAddr_list = [index[2] for index in all_order_para]
        # idDetailOrder_list = [index[0] for index in all_order_para]

        # querySender = shippingAddr.select().where(shippingAddr.c.idShippingAddr.in_(idShippingAddr_list))
        # queryReceiver = deliveryAddr.select().where(deliveryAddr.c.idDeliveryAddr.in_(idDeliveryAddr_list))
        # queryType = product.select().where(product.c.idDetailOrder.in_(idDetailOrder_list))

        # lstSender = conn.execute(querySender).fetchall()
        # lstReceiver = conn.execute(queryReceiver).fetchall()
        # lstType = conn.execute(queryType).fetchall()
    
    return (lstSender, lstReceiver, lstType)

