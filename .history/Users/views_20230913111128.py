from django.shortcuts import render
from django.contrib.auth.hashers import make_password

def base_user_page(request):
    raw_pass = '123456'
    hash_pass = make_password(raw_pass)
    return render(request, 'Users/base_users_page.html')
