from django.shortcuts import render, redirect
from django.http import JsonResponse
from dotenv import load_dotenv
import datetime
import os
import jwt

from . import exec_data 
from . import get_inform_order

# This function try to check token. If token already is use. It continue browsing or redirect to homepage
def check_login_required(token_str):
    if token_str == '':
        return 'empty'
    else:
        load_dotenv()
        secret_key = os.getenv('secret_key')
        decode_token = jwt.decode(token_str, secret_key, algorithms = ["HS256"])
        current_time = datetime.datetime.now()
        end_time_token = datetime.datetime.strptime(decode_token['endTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')
        if current_time < end_time_token:
            role_user_temp = decode_token['roleUser']
            if role_user_temp == 'users':
                return {'role_user' : 'users', 'id_users' : decode_token['idRoleUser']}
            else:
                return 'NP'
        else:
            return 'ET'

# 3 function help to load and get division
def userGetAllProvince(request):
    get_all_province = exec_data.get_all_province()
    lst_all_province_temp = [item['province'] for item in get_all_province]
    dict_all_province = {'result' : lst_all_province_temp}
    return JsonResponse(dict_all_province)
    
def userGetDistrictFromProvince(request):
    name_province = request.POST.get('province', None)
    get_all_district = exec_data.get_all_district_by_name_province(name_province)
    lst_all_district = [item['district'] for item in get_all_district]
    dict_all_district = {'result' : lst_all_district}
    return JsonResponse(dict_all_district)

def userGetWardFromDistrict(request):
    name_province = request.POST.get('province', None)
    name_district = request.POST.get('district', None)
    get_all_ward = exec_data.get_all_ward_by_name_province_name_district(name_province, name_district)
    lst_all_ward = [item['ward'] for item in get_all_ward]
    dict_all_ward = {'result' : lst_all_ward}
    return JsonResponse(dict_all_ward)

# Base User Page
def base_user_page(request):
    return render(request, 'Users/base_users_page.html')

# This function show send product form.
def users_send_product(request):
    all_ds = exec_data.getAllDS()
    context_return = {'all_DS' : all_ds }

    if request.method == 'POST':
        lst_error = []
        dict_para_exec = {}
        
        dict_para_exec['idUser'] = request.session.get('id_role_user_ddelivery', None)

        # Information about sender
        dict_para_exec['nameSender'] = request.POST.get('inputSenderName', None)
        dict_para_exec['telNumSender'] = request.POST.get('inputSenderTelNum', None)
        dict_para_exec['provinceAddrSender'] = request.POST.get('chooseAddProvinceSender', None)
        dict_para_exec['districtAddrSender'] = request.POST.get('chooseAddDistrictSender', None)
        dict_para_exec['wardAddrSender'] = request.POST.get('chooseAddWardSender', None)
        dict_para_exec['typeAddrSender'] = request.POST.get('typeAddressSender', None)
        dict_para_exec['noteAddrSender'] = request.POST.get('noteAddressSender', None)

        # Information about receiver
        dict_para_exec['nameReceiver'] = request.POST.get('inputReceiverName', None)
        dict_para_exec['telNumReceiver']  = request.POST.get('inputReceiverTelNum', None)
        dict_para_exec['provinceAddrReceiver']  = request.POST.get('chooseAddProvinceReceiver', None)
        dict_para_exec['districtAddrReceiver'] = request.POST.get('chooseAddDistrictReceiver', None)
        dict_para_exec['wardAddrReceiver'] = request.POST.get('chooseAddWardReceiver', None)
        dict_para_exec['typeAddrReceiver'] = request.POST.get('typeAddressReceiver', None)
        dict_para_exec['noteAddrReceiver'] = request.POST.get('noteAddrReceiver', None)

        # Input tag html support required so we don't need to check blank fill
        # In the simple case we just need to check user input address
        if (dict_para_exec['provinceAddrSender'] == 'NotProvince'):
            lst_error.append('You not choose country sender')
        if (dict_para_exec['districtAddrSender'] == 'NotDistrict'):
            lst_error.append('You not choose district sender')
        if (dict_para_exec['wardAddrSender'] == 'NotWard'):
            lst_error.append('You not choose ward sender')
        if (dict_para_exec['provinceAddrReceiver'] == 'NotProvince'):
            lst_error.append('You not choose country receiver')
        if (dict_para_exec['districtAddrReceiver'] == 'NotDistrict'):
            lst_error.append('You not choose district receiver')
        if (dict_para_exec['wardAddrReceiver'] == 'NotWard'):
            lst_error.append('You not choose ward receiver')

        if lst_error:
            return redirect('Users_app:Users_send_product_page', error_send_product = lst_error)
        
        # We get information about shipping cost and cod cost
        dict_para_exec['shippingCost'] = request.POST.get('inputShippingCost', None)
        dict_para_exec['codCost'] = request.POST.get('inputCODCost', None)
        dict_para_exec['whoPayCost'] = request.POST.get('whoPayCost', None)
        dict_para_exec['chooseDS'] = request.POST.get('chooseDS', None)

        # Each list contain : productName, typeProduct, quantityProduct, weightProduct, sizeProduct, descriptionProduct 
        # Each order can contain many product so we create 1 list to store all product, 1 product is 1 list
        lst_all_product = []

        order_code_random = datetime.datetime.now().strftime(f"%Y%m%d%H%M%S") + 'User' + str(dict_para_exec['idUser']) + dict_para_exec['telNumSender'] + dict_para_exec['telNumReceiver']

        for index in range(0, int(request.POST.get('allProductAdd'))):
            lst_all_product.append([request.POST.get(f'productName{index}'), 
            request.POST.get(f'chooseTypeProduct{index}'),request.POST.get(f'productQuantity{index}'), request.POST.get(f'productWeight{index}'),
            request.POST.get(f'productSize{index}'), request.POST.get(f'descriptionProduct{index}')])
            img_product = request.FILES[f'imageProduct{index}']
            # uploadImageFirebase(img_product, f'User/UserProvideImage/{order_code_random}.jpg')
        
        dict_para_exec['lstAllProduct'] = lst_all_product

        exec_data.exec_users_send_product(dict_para_exec['idUser'], order_code_random, **dict_para_exec)

        context_return['notificationNewOrder'] = f'Create success New Order. Code Order is {order_code_random}. You can check it on order in pending tab'

        return render(request, 'Users/users_send_product.html', context_return)

    return render(request, 'Users/users_send_product.html', context_return)

def return_order(request, current_page = 1):
    id_user = request.session.get('id_role_user_ddelivery', None)
    
    get_return_order = exec_data.get_return_order(id_user, current_page)
    paginationPage = get_return_order['pagination']
    return_order = get_return_order['return_order']
    len_return_order = get_return_order['length_all_return_order']
    
    all_inform = get_inform_order.get_information(return_order)
    
    context_return = {'return_order' : return_order}
    context_return['lstSender'] = all_inform[0]
    context_return['lstReceiver'] = all_inform[1]
    context_return['lstProduct'] = all_inform[2]
    context_return['paginationPage'] = paginationPage
    context_return['CurrentPage'] = current_page
    context_return['len_all_order'] = len_return_order
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
    
    return render(request, 'Users/order_return.html', context_return)

def users_logout(request):
    request.session.clear()
    return redirect('homepage_app:Homepage_homepage')

def users_search_order(request):
    if request.method == 'POST':
        id_user = request.session.get('id_role_user_ddelivery', None)
        code_order = request.POST.get('inputCodeOrder', None)

        get_order = exec_data.exec_search_order(id_user, code_order)
        all_inform = get_inform_order.get_information(get_order)

        context_return = {'getOrder' : get_order, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstProduct' : all_inform[2]}

        return render(request, 'Users/users_search_order.html', context_return)

    return render(request, 'Users/users_search_order.html')

def base_personal_profile(request):
    id_user = request.session.get('id_role_user_ddelivery', None)
    user_inform = exec_data.get_inform_users(id_user) 

    context_return = {}

    context_return['userInform'] = user_inform

    if request.method == 'POST':
        form_data = request.POST

        if 'submitBasicInform' in form_data:
            name_user = form_data['inputNameUser']
            tel_user = form_data['inputTelUser']
            gmail_user = form_data['inputGmailUser']
            gender_user = form_data['inputGenderUser']

            status_update = exec_data.update_basic_inform_user(id_user, name_user, tel_user, gmail_user, gender_user)
            if status_update == 'OK':
                context_return['notificationMsg'] = 'Update basic information success'
                return render(request, 'Users/users_personal_profile.html', context_return)
            else:
                context_return['notificationMsg'] = 'Update basic information fail'
                return render(request, 'Users/users_personal_profile.html', context_return)
        elif 'submitAdvanceInform' in form_data:
            province_user = form_data['chooseProvinceUserProfile']
            district_user = form_data['chooseDistrictUserProfile']
            ward_user = form_data['chooseWardUserProfile']
            
            if province_user == 'NotProvince' or district_user == 'notDistrict' or ward_user == 'NotWard':
                context_return['notificationMsg'] = "Update advanced information fail"
                return render(request, 'Users/users_personal_profile.html', context_return)

            status_update = exec_data.update_advanced_inform_user(id_user, province_user, district_user, ward_user)
            if status_update == 'OK':
                context_return['notificationMsg'] = "Update advanced information success"
                return render(request, 'Users/users_personal_profile.html', context_return)
            else:
                context_return['notificationMsg'] = "Update advanced information fail"
                return render(request, 'Users/users_personal_profile.html', context_return)
        elif 'submitChangePass' in form_data:
            first_pass = form_data['inputFirstPass']
            second_pass = form_data['inputSecondPass']
            load_dotenv()
            secret_key = os.getenv('secret_key')

            status_update = exec_data.update_password_user(id_user, first_pass, secret_key)
            if status_update == 'OK':
                context_return['notificationMsg'] = "Change password success"
                return render(request, 'Users/users_personal_profile.html', context_return)
            else:
                context_return['notificationMsg'] = "Change password fail"
                return render(request, 'Users/users_personal_profile.html', context_return)

    return render(request, 'Users/users_personal_profile.html', context_return)

def user_static_show_all_order(request,current_page = 1):
    id_user = request.session.get('id_role_user_ddelivery', None)

    lst_name_order = exec_data.exec_user_static_show_all_order(id_user, current_page)
    all_order_user = lst_name_order[1]

    all_inform = get_inform_order.get_information(all_order_user)

    context_return = {'allOrderByUser' : all_order_user, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstProduct' : all_inform[2], 'CurrentPage' : current_page, 'paginationPage' : lst_name_order[2], 'nameDriver' : lst_name_order[0]}

    context_return['range_pagination_page'] = [iteration for iteration in range(1, lst_name_order[2] + 1)]

    return render(request, 'Users/static_show_all_order.html', context_return)

def user_static_order_by_date(request, current_page = 1, date_now = datetime.datetime.now().strftime(f'%Y-%m-%d')):
    id_user = request.session.get('id_role_user_ddelivery', None)

    if request.method == 'POST':
        date_now = request.POST.get('chooseDateStatic', None)
        execute_database = exec_data.get_user_static_order_by_date(date_now, id_user, current_page)
        all_order_date = execute_database[1]
        
        paginationPage = execute_database[0]
        all_inform = get_inform_order.get_information(all_order_date)
        
        context_return = {'allOrderByDate' : all_order_date, 'lstSender' : all_inform[0], 'dateNow' : date_now, 'lstReceiver' : all_inform[1], 'lstProduct' : all_inform[2], 'CurrentPage' : current_page, 'paginationPage' : paginationPage}

        context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
        return render(request, 'Users/static_order_by_date.html', context_return)

    if date_now == 'None':
        date_now = datetime.datetime.now().strftime(f'%Y-%m-%d')

    execute_database = exec_data.get_user_static_order_by_date(date_now, id_user, current_page)
    all_order_date = execute_database[1]
    paginationPage = execute_database[0]
    all_inform = get_inform_order.get_information(all_order_date)

    context_return = {'allOrderByDate' : all_order_date, 'lstSender' : all_inform[0], 'dateNow' : date_now, 'lstReceiver' : all_inform[1], 'lstProduct' : all_inform[2], 'CurrentPage' : current_page, 'paginationPage' : paginationPage}

    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
    return render(request, 'Users/static_order_by_date.html', context_return)

def user_static_order_success(request, current_page = 1):
    id_user = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.get_user_static_order_success(id_user, current_page)
    all_order_success = execute_database[1]
    paginationPage = execute_database[0]
    
    all_inform = get_inform_order.get_information(all_order_success)

    context_return = {'all_order_success' : all_order_success, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstProduct' : all_inform[2], 'CurrentPage' : current_page, 'paginationPage' : paginationPage}
    
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
    
    return render(request, 'Users/static_order_success.html', context_return)

def user_static_order_pending(request, current_page = 1):
    id_user = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.get_user_static_order_pending(id_user, current_page)
    all_order_pending = execute_database[1]
    paginationPage = execute_database[0]
    print(execute_database)
    all_inform = get_inform_order.get_information(all_order_pending)

    context_return = {'allOrderPending' : all_order_pending, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstProduct' : all_inform[2], 'CurrentPage' : current_page, 'paginationPage' : paginationPage}
    return render(request, 'Users/static_order_pending.html', context_return)

# @userStaticOrder_blueprint.route('/static-order-delivery/<CurrentPage>')
# def userStaticOrderDelivery(CurrentPage = 1):
#     id_user = session.get('idUser')

#     execute_database = executeDatabase.getUserStaticOrderDelivery(id_user, CurrentPage)
#     all_order_delivery = execute_database[1]
#     paginationPage = execute_database[0]
#     all_inform = userGiveInformationAboutOrder.giveInformationAboutOrder(all_order_delivery)

#     return render_template('staticOrderDelivery.html', allOrderDelivery = all_order_delivery, lstSender = all_inform[0], lstReceiver =all_inform[1],
#         lstProduct = all_inform[2], CurrentPage = int(CurrentPage), paginationPage = paginationPage)

# @userStaticOrder_blueprint.route('/static-order-need-delivery/<CurrentPage>')
# def userStaticOrderNeedDelivery(CurrentPage = 1):
#     id_user = session.get('idUser')

#     execute_database = executeDatabase.getUserStaticOrderNeedDelivery(id_user, CurrentPage)
#     all_order_need_delivery = execute_database[1]
#     paginationPage = execute_database[0]

#     all_inform = userGiveInformationAboutOrder.giveInformationAboutOrder(all_order_need_delivery)

#     return render_template('staticOrderNeedDelivery.html', allOrderNeedDelivery = all_order_need_delivery, lstSender = all_inform[0], lstReceiver =all_inform[1],
#         lstProduct = all_inform[2], CurrentPage = int(CurrentPage), paginationPage = paginationPage)