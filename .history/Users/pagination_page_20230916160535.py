def calPagination(CurrentPage, limit_show, length_all_order):
    pagination_page = -(- length_all_order // limit_show)
    start_index = (int(CurrentPage) - 1) * limit_show
    end_index = min(start_index + limit_show, length_all_order - 1)

    return [pagination_page, start_index, end_index]
