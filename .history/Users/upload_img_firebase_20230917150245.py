from firebase_admin import storage

def uploadImageFirebase(files, remote_file_name):
    bucket = storage.bucket()
    blob = bucket.blob(remote_file_name)
    blob.upload_from_file(files.stream)