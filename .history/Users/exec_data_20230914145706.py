from django.contrib.sessions.backends.db import SessionStore
from Homepage.models import *

def get_name_user_by_id(id_user_para):
    pass

def getAllDS():
    all_ds = DS_tb.objects.filter(status_DS = 'on').values()
    return all_ds

def get_all_province():
    all_province = Division_tb.objects.values('province').distinct().order_by('province')
    return all_province

def get_all_district_by_name_province(name_province_para):
    all_district = Division_tb.objects.filter(province = name_province_para).values('district').distinct().order_by('district')
    return all_district

def get_all_ward_by_name_province_name_district(name_province_para, name_district_para):
    all_ward = Division_tb.objects.filter(province = name_province_para, district = name_district_para).values('ward').distinct().order_by('ward')
    return all_ward
