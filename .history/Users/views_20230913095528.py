from django.shortcuts import render

def base_user_page(request):
    return render(request, 'Users/base_users_page.html')
