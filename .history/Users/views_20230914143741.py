from django.shortcuts import render
from django.http import JsonResponse

from . import exec_data 

def userGetAllProvince(request):
    get_all_province = exec_data.get_all_province()
    lst_all_province_temp = [item['province'] for item in get_all_province]
    dict_all_province = {'result' : lst_all_province_temp}
    return JsonResponse(dict_all_province)
    
def userGetDistrictFromProvince(request):
    if request.is_ajax() and request.method == 'POST':
        name_province = request.POST.get('province', None)

        get_all_district = exec_data.get_all_district_by_name_province(name_province)
        lst_all_district = [item['district'] for item in get_all_district]
        dict_all_district = {'result' : lst_all_district}


    return jsonify(result = get_districts)

# @getAddress_blueprint.route('/get-wards', methods = ['POST'])
# def userGetWardFromDistrict():
#     with engine.connect() as conn:
#         province = request.form['province']
#         district = request.form['district']
#         query_all_ward = select(division.c.ward.distinct()).where(and_(division.c.province == province, division.c.district == district))
#         all_wards = conn.execute(query_all_ward).fetchall()
#         all_wards = [tuple(row) for row in all_wards]

#     return jsonify(result = all_wards)


def base_user_page(request):
    return render(request, 'Users/base_users_page.html')

def users_send_product(request):
    all_ds = exec_data.getAllDS()
    context_return = {'all_DS' : all_ds }
    return render(request, 'Users/users_send_product.html', context_return)
