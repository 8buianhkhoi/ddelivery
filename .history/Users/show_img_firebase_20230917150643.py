# @showImageFirebaseUser_blueprint.route('/see-image/image-get-product/<codeOrder>')
# def userShowImageGetProductFb(codeOrder = 'None'):
#     if codeOrder != 'None':
#         fileBytes = downloadImageFirebase(f'Driver/ImageGetProduct/{codeOrder}.jpg')
#         contextReturn = {}

#         try:
#             imageGetProduct = base64.b64encode(fileBytes).decode('utf-8')
#             contextReturn['imageShow'] = imageGetProduct
#         except NotFound:
#             contextReturn['errorShowImage'] = 'Not Found Image In Database. Please contact to admin for more information'
#         except Exception as e:
#             contextReturn['errorShowImage'] = 'Have some problem in downloading process. Please contact to admin for more information'

#         return render_template('showImageFbUser.html', **contextReturn)
#     else:
#         return render_template('showImageFbUser.html')

# @showImageFirebaseUser_blueprint.route('/see-image/image-in-delivery/<codeOrder>')
# def userShowImageInDeliveryFb(codeOrder = 'None'):
#     if codeOrder != 'None':
#         fileBytes = downloadImageFirebase(f'Driver/ImageInTransport/{codeOrder}.jpg')
#         contextReturn = {}

#         try:
#             imageInDelivery = base64.b64encode(fileBytes).decode('utf-8')
#             contextReturn['imageShow'] = imageInDelivery
#         except NotFound:
#             contextReturn['errorShowImage'] = 'Not Found Image In Database. Please contact to admin for more information'
#         except Exception as e:
#             contextReturn['errorShowImage'] = 'Have some problem in downloading process. Please contact to admin for more information'

#         return render_template('showImageFbUser.html', **contextReturn)
#     else:
#         return render_template('showImageFbUser.html')

# @showImageFirebaseUser_blueprint.route('/see-image/image-order-success/<codeOrder>')
# def userShowImageOrderSuccessFb(codeOrder = 'None'):
#     if codeOrder != 'None':
#         fileBytes = downloadImageFirebase(f'Driver/DeliverySuccess/{codeOrder}.jpg')
#         contextReturn = {}

#         try:
#             imageOrderSuccess = base64.b64encode(fileBytes).decode('utf-8')
#             contextReturn['imageShow'] = imageOrderSuccess
#         except NotFound:
#             contextReturn['errorShowImage'] = 'Not Found Image In Database. Please contact to admin for more information'
#         except Exception as e:
#             contextReturn['errorShowImage'] = 'Have some problem in downloading process. Please contact to admin for more information'

#         return render_template('showImageFbUser.html', **contextReturn)
#     else:
#         return render_template('showImageFbUser.html')

# @showImageFirebaseUser_blueprint.route('/see-image/image-product-user-provide/<codeOrder>')
# def userShowImageProductProvideFb(codeOrder = 'None'):
#     if codeOrder != 'None':
#         fileBytes = downloadImageFirebase(f'User/UserProvideImage/{codeOrder}.jpg')
#         contextReturn = {}

#         try:
#             imageUserProvide = base64.b64encode(fileBytes).decode('utf-8')
#             contextReturn['imageShow'] = imageUserProvide
#         except NotFound:
#             contextReturn['errorShowImage'] = 'Not Found Image In Database. Please contact to admin for more information'
#         except Exception as e:
#             contextReturn['errorShowImage'] = 'Have some problem in downloading process. Please contact to admin for more information'

#         return render_template('showImageFbUser.html', **contextReturns)
#     else:
#         return render_template('showImageFbUser.html')