function addNewDivProduct() {
    let originalDiv = document.getElementsByClassName('each__input__infor__product')[0]
    let cloneDiv = originalDiv.cloneNode(true)
    document.getElementsByClassName('group__all__product')[0].appendChild(cloneDiv)
    let currentNumberDivProduct = parseInt(document.getElementById('all__product__add__number').value)
    document.getElementById('all__product__add__number').value = currentNumberDivProduct + 1
    document.getElementsByClassName('input__product__name')[currentNumberDivProduct].setAttribute('name', 'productName' + currentNumberDivProduct)
    document.getElementsByClassName('input__product__type__select')[currentNumberDivProduct].setAttribute('name', 'chooseTypeProduct' + currentNumberDivProduct)
    document.getElementsByClassName('input__product__quantity')[currentNumberDivProduct].setAttribute('name', 'productQuantity' + currentNumberDivProduct)
    document.getElementsByClassName('input__product__weight')[currentNumberDivProduct].setAttribute('name', 'productWeight' + currentNumberDivProduct)
    document.getElementsByClassName('input__product__size')[currentNumberDivProduct].setAttribute('name', 'productSize' + currentNumberDivProduct)
    document.getElementsByClassName('input__description__product')[currentNumberDivProduct].setAttribute('name', 'descriptionProduct' + currentNumberDivProduct)
    document.getElementsByClassName('count__product__sender')[currentNumberDivProduct].innerText = 'Product ' + (currentNumberDivProduct + 1)
    document.getElementsByClassName('input__choose__image__product')[currentNumberDivProduct].setAttribute('name', 'imageProduct' + currentNumberDivProduct)
    document.getElementsByClassName('input__choose__image__product')[currentNumberDivProduct].id = 'input__file__image__' + currentNumberDivProduct
    document.getElementsByClassName('preview__image__product')[currentNumberDivProduct].id = 'preview__image__product__' + currentNumberDivProduct
    document.getElementsByClassName('preview__image__product')[currentNumberDivProduct].src = ''
    document.getElementsByClassName('preview__image__product')[currentNumberDivProduct].style.display = 'none'
}

function loadDistrict(event){
    let nameSelect = event.target.name
    
    if (nameSelect === 'chooseAddProvinceSender'){
        let province = document.getElementById('choose__province__sender').value
        $.ajax({
            url:'/users/user-page/get-address/get-district/',
            method: 'POST',
            data: {province:province},
            success: function(response){
                let allDistrict = response['result']
                let chooseSelect = document.getElementById('choose__district__division_sender')
                let lengthOptionInSelect = chooseSelect.options.length
                for (let option = lengthOptionInSelect - 1; option >= 1 ; option--){
                    chooseSelect.options[option].remove();
                }
                for (let index in allDistrict){
                    let eachDistrict = allDistrict[index]
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachDistrict;
                    newOptionElement.value = eachDistrict;
                    chooseSelect.appendChild(newOptionElement)
                }
            },
            error: function(error){
                console.log(error)
            }
        })
    }
    else if (nameSelect === 'chooseAddProvinceReceiver'){
        let province = document.getElementById('choose__province__receiver').value
        $.ajax({
            url:'/users/user-page/get-address/get-district/',
            method: 'POST',
            data: {province:province},
            success: function(response){
                let allDistrict = response['result']
                let chooseSelect = document.getElementById('choose__district__division__receiver')
                let lengthOptionInSelect = chooseSelect.options.length
                for (let option = lengthOptionInSelect - 1; option >= 1 ; option--){
                    chooseSelect.options[option].remove();
                }
                for (let index in allDistrict){
                    let eachDistrict = allDistrict[index]
                    
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachDistrict;
                    newOptionElement.value = eachDistrict;
                    chooseSelect.appendChild(newOptionElement)
                }
            },
            error: function(error){
                console.log(error)
            }
        })
    }

}

function loadWard(event){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 

    let nameSelect = event.target.name
    
    if (nameSelect === 'chooseAddDistrictSender'){
        let district = document.getElementById('choose__district__division_sender').value
        let province = document.getElementById('choose__province__sender').value
        $.ajax({
            url:'/users/user-page/get-address/get-wards/',
            method: 'POST',
            data: {province:province, district :district, csrfmiddlewaretoken : csrf_token},
            success: function(response){
                let allWard = response['result']
                let chooseSelect = document.getElementById('choose__ward__division__sender')
                let lengthOptionInSelect = chooseSelect.options.length 
                for (let option = lengthOptionInSelect - 1; option >=1 ; option--){
                    chooseSelect.options[option].remove()
                }
                for (let index in allWard){
                    let eachWard = allWard[index]
                    
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachWard;
                    newOptionElement.value = eachWard;
                    chooseSelect.appendChild(newOptionElement)
                }
            },
            error: function(error){
                console.log(error)
            }
        })
    }
    else if (nameSelect === 'chooseAddDistrictReceiver'){
        let province = document.getElementById('choose__province__receiver').value
        let district = document.getElementById('choose__district__division__receiver').value
        $.ajax({
            url:'/users/user-page/get-address/get-wards/',
            method: 'POST',
            data: {province:province, district:district},
            success: function(response){
                let allWard = response['result']
                let chooseSelect = document.getElementById('choose__ward__division__receiver')
                let lengthOptionInSelect = chooseSelect.options.length 
                for (let option = lengthOptionInSelect - 1; option >=1 ; option--){
                    chooseSelect.options[option].remove()
                }
                for (let index in allWard){
                    let eachWard = allWard[index]
                    
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachWard;
                    newOptionElement.value = eachWard;
                    chooseSelect.appendChild(newOptionElement)
                }
            },
            error: function(error){
                console.log(error)
            }
        })
    }

}

function onLoadProvince(){
    const csrf_token = document.querySelector('[name=csrfmiddlewaretoken]').value 
    
    $.ajax({
        url:'/users/user-page/get-address/get-province/',
        method: 'POST',
        data: {
            csrfmiddlewaretoken : csrf_token,
        },
        success: function(response){
            let allProvince = response['result']
            for (let index in allProvince){
                let eachProvince = allProvince[index]
                let chooseSelect = document.getElementsByClassName('choose__province__division');
                let lengthSelectProvince = chooseSelect.length

                for (let select = 0; select < lengthSelectProvince; select++){
                    let newOptionElement = document.createElement('option');
                    newOptionElement.text = eachProvince;
                    newOptionElement.value = eachProvince;
                    chooseSelect[select].appendChild(newOptionElement)
                }
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function chooseBtnSenderReceiver(strBtn){
    if ( strBtn === 'sender' ){
        document.getElementsByClassName('infor__sender')[0].style.display = 'block';
        document.getElementsByClassName('infor__receiver')[0].style.display = 'none';
        document.getElementsByClassName('btn__choose__sender')[0].className = 'btn__choose__role btn__choose__sender btn__choose__active'
        document.getElementsByClassName('btn__choose__receiver')[0].className = 'btn__choose__role btn__choose__receiver btn__choose__disable'
    }
    else if ( strBtn === 'receiver'){
        document.getElementsByClassName('infor__sender')[0].style.display = 'none';
        document.getElementsByClassName('infor__receiver')[0].style.display = 'block';
        document.getElementsByClassName('btn__choose__sender')[0].className = 'btn__choose__role btn__choose__sender btn__choose__disable'
        document.getElementsByClassName('btn__choose__receiver')[0].className = 'btn__choose__role btn__choose__receiver btn__choose__active'
    }
}


function inputImagePreview(event){
    // let fileImage = document.getElementById(`input__file__image__${intImageProduct}`)
    // let previewImage = document.getElementById(`preview__image__product__${intImageProduct}`)
    let idIndexInputFile = event.currentTarget.id[event.currentTarget.id.length - 1]
    let fileImage = event.target.files[0]
    let showImage = document.getElementById(`preview__image__product__${idIndexInputFile}`)
    let reader = new FileReader();

    reader.onload = function(event){
        showImage.src = event.target.result;
        showImage.style.display = 'block'
    }

    reader.readAsDataURL(fileImage);
}
