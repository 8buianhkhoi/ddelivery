from django.urls import path

from . import views 
urlpatterns = [
    path('homepage/', views.base_user_page, name='Users_base_user_page')
]