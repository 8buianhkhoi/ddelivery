from Homepage.models import *

# parameter all_order_para is a QuerySet. Each element is a dict. Like QuerySet[{...}, {...}, {...}, ...]. Each dictionary element is each row Detail_Order_tb table
# This function get information about sender, receiver and product.
def get_information(all_order_para):
    lstSender, lstReceiver, lstType = [], [], []
    for order_item in all_order_para:
        sender_item = Shipping_Addr_tb.objects.filter(id = order_item['id_shipping_addr_id']).values()
        receiver_item = Delivery_Addr_tb.objects.filter(id = order_item['id_delivery_addr_id']).values()
        product_item = Product_tb.objects.filter(id = order_item['id']).values()

        lstSender.append(sender_item)
        lstReceiver.append(receiver_item)
        lstType.append(product_item)

    return (lstSender, lstReceiver, lstType)




