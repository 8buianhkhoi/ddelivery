from django.shortcuts import render
from django.http import JsonResponse

from . import exec_data 

# @getAddress_blueprint.route('/get-province', methods = ['POST'])
def userGetAllProvince():
    get_all_province = exec_data.get_all_province()
    dict_all_province = {item['province'] for item in get_all_province}
    return JsonResponse(result = get_all_province)
    
# @getAddress_blueprint.route('/get-district', methods = ['POST'])
# def userGetDistrictFromProvince():
#     province = request.form['province']

#     with engine.connect() as conn:
#         query_district = select(division.c.district.distinct()).where(division.c.province == province).order_by(division.c.district)
#         get_districts = conn.execute(query_district).fetchall()
#         get_districts = [tuple(row) for row in get_districts]

#     return jsonify(result = get_districts)

# @getAddress_blueprint.route('/get-wards', methods = ['POST'])
# def userGetWardFromDistrict():
#     with engine.connect() as conn:
#         province = request.form['province']
#         district = request.form['district']
#         query_all_ward = select(division.c.ward.distinct()).where(and_(division.c.province == province, division.c.district == district))
#         all_wards = conn.execute(query_all_ward).fetchall()
#         all_wards = [tuple(row) for row in all_wards]

#     return jsonify(result = all_wards)


def base_user_page(request):
    return render(request, 'Users/base_users_page.html')

def users_send_product(request):
    all_ds = exec_data.getAllDS()
    context_return = {'all_Ds' : all_ds }
    print(userGetAllProvince())
    return render(request, 'Users/users_send_product.html', context_return)
