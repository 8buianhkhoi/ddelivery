from django.shortcuts import render
import xlsxwriter
import datetime

from Homepage.models import *
from DS import exec_data

# Create your views here.
@generateExcel__blueprint.route('/get-excel-file-static')
def generateExcel():
    workbook = xlsxwriter.Workbook('demo.xlsx')
    worksheet = workbook.add_worksheet()
    conn = engine.connect()
    get_args = request.args.get('status')
    
    id_ds = session.get('idDS')
    if get_args == 'current_page':
        get_all_order = request.args.getlist('allOrder')
        allOrder = []

        # Because each elements on list get_args is string like "(1,2,3, ...)" so we need to change each element to tuple
        # we use eval function
        for each_tuple in get_all_order:
            allOrder.append(eval(each_tuple))
    elif get_args == 'all_page':
        allOrder = dsExecuteDatabase.getStaticDSExcelAll(id_ds)




    # Có các cột tương ứng trong file excel
    worksheet.write(0,0,'Code Order')
    worksheet.write(0,1,'Information Delivery')
    worksheet.write(0,2,'Information Shipping')
    worksheet.write(0,3,'Driver Name')
    worksheet.write(0,4,'Date Create Order')
    worksheet.write(0,5,'Departure Time')
    worksheet.write(0,6,'Estimate Time')
    worksheet.write(0,7,'Total cost')
    worksheet.write(0,8,'Note')
    worksheet.write(0,9,'Status')
    row, col = 1, 0

    # Gía trị từng hàng tương ứng với từng cột
    for index in allOrder:
        for subindex in index[1:-1]:
            if col == 2:
                querySubindex = shippingAddr.select().where(shippingAddr.c.idShippingAddr == subindex )
                subindex = conn.execute(querySubindex).fetchone()[2:7]
                subindex = ','.join(subindex)
            elif col == 1:
                querySubindex = deliveryAddr.select().where(deliveryAddr.c.idDeliveryAddr == subindex )
                subindex = conn.execute(querySubindex).fetchone()[2:7]
                subindex = ','.join(subindex)
            elif col == 3:
                if subindex is None:
                    subindex = ''
                else:
                    querySubindex = driver.select().where(driver.c.idDriver == subindex )
                    subindex = conn.execute(querySubindex).fetchone()[4]
            elif col == 4 or col == 5 or col == 6 :
                subindex = str(subindex)
            worksheet.write(row, col, subindex)
            col = col + 1
        col = 0
        row = row + 1
    workbook.close()
    return send_file('demo.xlsx', as_attachment=True)
