# @DSProfile_blueprint.route('/ds-edit-driver-profile', methods = ['GET','POST'])
# def editDriverProfile():
#     id_ds = session.get('idDS')

#     if request.method == 'POST':
#         form_data = request.form 
#         notificationMsg = ''
#         id_driver = form_data['inputIdDriver']

#         if 'submitBasicInform' in form_data:
#             driver_user = form_data['inputNameDriver']
#             tel_driver = form_data['inputTelDriver']
#             gmail_driver = form_data['inputGmailDriver']
#             gender_driver = form_data['inputGenderDriver']

#             statusUpdateBasic = dsExecuteDatabase.updateBasicInformDriver(id_driver, driver_user, tel_driver, gmail_driver, gender_driver)
#             if statusUpdateBasic == 'OK':
#                 notificationMsg = 'Update basic information success'
#             else:
#                 notificationMsg = 'Update basic information fail'
#         elif 'submitAdvanceInform' in form_data:
#             province_driver = form_data['chooseProvinceDriverProfile']
#             district_driver = form_data['chooseDistrictDriverProfile']
#             ward_driver = form_data['chooseWardDriverProfile']
            
#             if province_driver == 'NotProvince' or district_driver == 'notDistrict' or ward_driver == 'NotWard':
#                 return redirect(url_for('.editDriverProfile', notificationMsg = "Update advanced information fail"))

#             statusUpdateAdvanced = dsExecuteDatabase.updateAdvancedInformDriver(id_driver, province_driver, district_driver, ward_driver)
#             if statusUpdateAdvanced == 'OK':
#                 notificationMsg = "Update advanced information success"
#             else:
#                 notificationMsg = "Update advanced information fail"
#         elif 'submitChangePass' in form_data:
#             first_pass = form_data['inputFirstPass']
#             second_pass = form_data['inputSecondPass']
#             load_dotenv()
#             secret_key = os.getenv('secret_key')

#             statusUpdatePass = dsExecuteDatabase.updatePasswordDriver(id_driver, first_pass, secret_key)
#             if statusUpdatePass == 'OK':
#                 notificationMsg = "Change password success"
#             else:
#                 notificationMsg = "Change password fail"
#         elif 'submitDriverLicense' in form_data:
#             full_name = form_data['inputFullNameLicenseDriver']
#             beginning_date = form_data['inputBeginningDateLicenseDriver']
#             class_motor = form_data['inputClassLicenseDriver']

#             statusUpdateDriverLicense = dsExecuteDatabase.updateDriverLicense(id_driver, full_name, beginning_date, class_motor)
#             if statusUpdateDriverLicense =='OK':
#                 notificationMsg = "Change driver license success"
#             else:
#                 notificationMsg = "Change driver license fail"
#         elif 'submitMotorRegCert' in form_data:
#             number_reg_cert = form_data['inputNumberRegCert']
#             palate_reg_cert = form_data['inputPalateRegCert']
#             branch_reg_cert = form_data['inputBranchRegCert']
#             color_reg_cert = form_data['inputColorRegCert']
#             capacity_motor_reg_cert = form_data['inputCapacityMotorRegCert']
#             first_reg_cert = form_data['inputFirstRegCert']

#             statusUpdateMotorRegCert = dsExecuteDatabase.updateDriverRegCert(id_driver, number_reg_cert, palate_reg_cert, branch_reg_cert, color_reg_cert, capacity_motor_reg_cert, first_reg_cert)
#             if statusUpdateMotorRegCert == 'OK':
#                 notificationMsg = 'Update motorbike registration certificate success'
#             else:
#                 notificationMsg = 'Update motorbike registration certificate fail'

#         return redirect(url_for('.editDriverProfile', notificationMsg = notificationMsg))


#     all_name_driver = dsExecuteDatabase.getAllDriver(id_ds)

#     notificationMsg = request.args.get('notificationMsg')
#     if notificationMsg is None:
#         return render_template('DSeditDriverProfile.html', allNameDriver = all_name_driver)
#     else:
#         return render_template('DSeditDriverProfile.html', allNameDriver = all_name_driver, notificationMsg = notificationMsg)