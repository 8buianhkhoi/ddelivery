# from sqlalchemy import select, and_, Integer, union, cast, Date
# from sqlalchemy.sql import func, extract
# import platform
# import hashlib
# import base64
# import datetime
from Homepage.models import *

from . import exec_data 
from . import pagination_page
from . import get_inform_order
from . import update_id_inventory

limit_show_order = 30
# from models.models import *
# from . import paginationPageDS
# from . import updateIdInventory

# def checkCurrentOS(conn):
#     if platform.system() == 'Linux':
#         conn.commit()
#     elif platform.system() == 'Darwin':
#         pass 
#     elif platform.system() == 'Windows':
#         pass

def get_order_in_delivery(idDS, CurrentPage):
    try:
        query_order = Detail_Order_tb.objects.filter(id_delivery_system = idDS, status_order = 'Delivery')
        len_all_order_delivery = query_order.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order_delivery)
        all_order_delivery = query_order.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        lstDriver = [] 
        for index in all_order_delivery: 
            driverTemp = Driver_tb.objects.filter(id = index['id_driver_id']).values()
            lstDriver.append(driverTemp)

        return [calc_pagination[0], all_order_delivery, lstDriver, len_all_order_delivery]
    except:
        return False

def get_order_success(idDS, CurrentPage):
    try:
        query_order = Detail_Order_tb.objects.filter(id_delivery_system = idDS, status_order = 'Success')
        len_all_order_success = query_order.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order_success)
        all_order_success = query_order.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        lstDriver = []
        for index in all_order_success:
            driverTemp = Driver_tb.objects.filter(id = index['id_driver_id']).values()
            lstDriver.append(driverTemp)

        return [calc_pagination[0], all_order_success, lstDriver, len_all_order_success]
    except:
        return False

def get_order_pending(idDS, CurrentPage):
    try:
        query_order = Detail_Order_tb.objects.filter(status_order = 'Pending', id_delivery_system = idDS)
        len_all_order_pending = query_order.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order_pending)
        all_order_pending = query_order.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        allDriver = Driver_tb.objects.filter(id_DS_FK = idDS).values()

        return [calc_pagination[0], all_order_pending, allDriver, len_all_order_pending]
    except:
        return False

def post_order_pending(idDS, codeOrder, idDriver, driverName):
    try:
        updateOrder = Detail_Order_tb.objects.filter(code_order_delivery = codeOrder).update(status_order = 'Need-Delivery')
        updateDriver = Detail_Order_tb.objects.filter(code_order_delivery = codeOrder).update(id_driver = idDriver)

        allInventory = Inventory_tb.objects.filter(id_ds = idDS).values()
        
        notificationUpdate = update_id_inventory.update_id_inventory(codeOrderPara = codeOrder, allInventory = allInventory)
        if notificationUpdate == 'Error':
            return 'NES'

        lstMessageUpdate = f'Update order {codeOrder} for driver {driverName}'
    
        return lstMessageUpdate
    except:
        return False

def get_order_need_delivery(idDS, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_delivery_system = idDS, status_order = 'Need-Delivery')
        len_all_order_need_delivery = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order_need_delivery)
        all_order_need_delivery = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        allDriverName = []
        lstDriverFull = []
        
        for order in all_order_need_delivery:
            idDriverTemp = order['id_driver_id']
            queryDriverName = Driver_tb.objects.filter(id_DS_FK = idDS, id = idDriverTemp)

            driverName = queryDriverName.values()[0]
            allDriverName.append(driverName['full_name_driver'])
            lstDriverFull.append(driverName)

        return [calc_pagination[0], all_order_need_delivery, allDriverName, lstDriverFull, len_all_order_need_delivery]
    except:
        return False

def get_all_driver(idDS):
    try:
        allDriverDS = Driver_tb.objects.filter(id_DS_FK = idDS, status_driver = 'on').values()
        return allDriverDS
    except:
        return False

def get_all_driver_ds(idDS):
    try:
        allDriverDS = Driver_tb.objects.filter(id_DS_FK = idDS, status_driver = 'on').values()
        return allDriverDS
    except:
        return False

# def getAllDriver(idDS):
#     with engine.connect() as conn:
#         queryOrder = select(driver.c.idDriver, driver.c.name).where(driver.c.idDeliverySystem == idDS)
#         all_name_driver = conn.execute(queryOrder).fetchall()

#     return all_name_driver

# def updateBasicInformDriver(idDriver, nameDriver, telDriver, gmailDriver, genderDriver):
#     with engine.connect() as conn:
#         if nameDriver != '':
#             queryUpdateNameDriver = driver.update().where(driver.c.idDriver == idDriver).values(name = nameDriver)
#             conn.execute(queryUpdateNameDriver)
#         if telDriver != '':
#             queryUpdateTelDriver = driver.update().where(driver.c.idDriver == idDriver).values(telNum = telDriver)
#             conn.execute(queryUpdateTelDriver)
#         if gmailDriver != '':
#             queryUpdateGmailDriver = driver.update().where(driver.c.idDriver == idDriver).values(gmail = gmailDriver)
#             conn.execute(queryUpdateGmailDriver)
#         if genderDriver != '':
#             queryUpdateGenderDriver = driver.update().where(driver.c.idDriver == idDriver).values(gender = genderDriver)
#             conn.execute(queryUpdateGenderDriver)

#         checkCurrentOS(conn)

#     return 'OK'

# def updateAdvancedInformDriver(idUser, provinceUser, districtUser, wardUser):
#     with engine.connect() as conn:
#         update_address = users.update().where(users.c.idUser == idUser).values(province = provinceUser, district = districtUser, ward = wardUser)
#         conn.execute(update_address)

#         checkCurrentOS(conn)

#     return 'OK'

# def hashPassword(password, salt=None, iterations= 100000, key_length =32):
#     key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, iterations, dklen=key_length)
    
#     return salt + key 

# def updatePasswordDriver(idUser, passwordUser, secretKey):
#     with engine.connect() as conn:
#         hash_bytes = hashPassword(passwordUser, secretKey.encode())
#         hash_password = base64.b64encode(hash_bytes).decode('utf-8')

#         update_pass = users.update().where(users.c.idUser == idUser).values(passWord = hash_password)
#         conn.execute(update_pass)

#         checkCurrentOS(conn)

#     return 'OK'

# def updateDriverLicense(idDriver, fullName, beginDate, classMotor):
#     with engine.connect() as conn:
#         update_driver_license = driverLicense.update().where(driverLicense.c.idDriver == idDriver).values(fullName = fullName, beginDate = beginDate, classLicense = classMotor)
#         conn.execute(update_driver_license)

#         checkCurrentOS(conn)
    
#     return 'OK'

# def updateDriverRegCert(idDriver, numberCert, palateCert, branchCert, colorCert, capacityMotorCert, firstRegCert):
#     with engine.connect() as conn:
#         update_driver_reg_cert = motorbikeRegCert.update().where(motorbikeRegCert.c.idDriver == idDriver).values(numberReg = numberCert, plate = palateCert, branch = branchCert, color = colorCert, capacity = capacityMotorCert, firstReg = firstRegCert)
#         conn.execute(update_driver_reg_cert)

#         checkCurrentOS(conn)

#     return 'OK'

# def executeGetInformDriver(idDriver):
#     conn = engine.connect()
#     driverInform = conn.execute(select(driver).where(driver.c.idDriver == idDriver)).fetchall()

#     return driverInform


# def updateBasicInformUser(idUser, nameUser, telUser, gmailUser, genderUser):
#     with engine.connect() as conn:
#         if nameUser != '':
#             queryUpdateNameUser = users.update().where(users.c.idUser == idUser).values(name = nameUser)
#             conn.execute(queryUpdateNameUser)
#         if telUser != '':
#             queryUpdateTelUser = users.update().where(users.c.idUser == idUser).values(telNum = telUser)
#             conn.execute(queryUpdateTelUser)
#         if gmailUser != '':
#             queryUpdateGmailUser = users.update().where(users.c.idUser == idUser).values(gmail = gmailUser)
#             conn.execute(queryUpdateGmailUser)
#         if genderUser != '':
#             queryUpdateGenderUser = users.update().where(users.c.idUser == idUser).values(gender = genderUser)
#             conn.execute(queryUpdateGenderUser)

#         checkCurrentOS(conn)

#     return 'OK'

# def updateAdvancedInformUser(idUser, provinceUser, districtUser, wardUser):
#     with engine.connect() as conn:
#         update_address = users.update().where(users.c.idUser == idUser).values(province = provinceUser, district = districtUser, ward = wardUser)
#         conn.execute(update_address)

#         checkCurrentOS(conn)

#     return 'OK'

# def updatePasswordUser(idUser, passwordUser, secretKey):
#     with engine.connect() as conn:
#         hash_bytes = hashPassword(passwordUser, secretKey.encode())
#         hash_password = base64.b64encode(hash_bytes).decode('utf-8')

#         update_pass = users.update().where(users.c.idUser == idUser).values(passWord = hash_password)
#         conn.execute(update_pass)

#         checkCurrentOS(conn)

#     return 'OK'

# def getStaticDSExcelSuccess(idDS, CurrentPage, limitShowOrder):
#     with engine.connect() as conn:
#         query_order = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS))
#         len_all_order = conn.execute(query_order).rowcount

#         calcPaginationPage = paginationPageDS.calPagination(CurrentPage, limitShowOrder, len_all_order)

#         all_order = conn.execute(query_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()
    
#     return {'pagination' : calcPaginationPage[0], 'all_order' : all_order, 'len_all_order' : len_all_order}

# def getStaticDSExcelAll(idDS):
#     with engine.connect() as conn:
#         queryAllOrder = detailOrder.select().where(detailOrder.c.idDeliverySystem == idDS)
#         allOrder = conn.execute(queryAllOrder).fetchall()
    
#     return allOrder

def getTotalMoneyMonth(monthValue, idDS, CurrentPage):
    try:
        query_order_all_money = select(func.sum(detailOrder.c.totalCost.cast(Integer))).where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success', extract('year', detailOrder.c.realTimeArrive) == int(monthValue[:4]), extract('month', detailOrder.c.realTimeArrive) == int(monthValue[5:])))
        query_all_order = select(detailOrder).where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success', extract('year', detailOrder.c.realTimeArrive) == int(monthValue[:4]), extract('month', detailOrder.c.realTimeArrive) == int(monthValue[5:])))
        
        execute_order_all_money = conn.execute(query_order_all_money).scalar()

        if execute_order_all_money is None:
            all_money_month = 0

        else:
            all_money_month = int(execute_order_all_money)

        len_all_order_success = conn.execute(query_all_order).rowcount

        calcPaginationPage = paginationPageDS.calPagination(CurrentPage, limitShowOrder, len_all_order_success)

        all_order_success = conn.execute(query_all_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()

        lstDriver = []
        for index in all_order_success:
            driverTemp = conn.execute(driver.select().where(driver.c.idDriver == index[4])).fetchone()
            lstDriver.append(driverTemp)


    return [all_money_month, all_order_success, lstDriver, calcPaginationPage[0]]

def getTotalMoneyDay(dayValue, idDS, CurrentPage, limitShowOrder):
    with engine.connect() as conn:
        chooseDate = datetime.datetime.strptime(dayValue, f'%Y-%m-%d').date()

        query_order_all_money = select(func.sum(detailOrder.c.totalCost.cast(Integer))).where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success', func.DATE(detailOrder.c.realTimeArrive) == chooseDate))
        query_all_order = select(detailOrder).where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success', func.DATE(detailOrder.c.realTimeArrive) == chooseDate))
        
        execute_order_all_money = conn.execute(query_order_all_money).scalar()
        
        if execute_order_all_money is None:
            all_money_day = 0
        else:
            all_money_day = int(execute_order_all_money)
        len_all_order_success = conn.execute(query_all_order).rowcount

        calcPaginationPage = paginationPageDS.calPagination(CurrentPage, limitShowOrder, len_all_order_success)

        all_order_success = conn.execute(query_all_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()

        lstDriver = []
        for index in all_order_success:
            driverTemp = conn.execute(driver.select().where(driver.c.idDriver == index[4])).fetchone()
            lstDriver.append(driverTemp)


    return [all_money_day, all_order_success, lstDriver, calcPaginationPage[0]]

def getTotalMoneyYear(yearValue, idDS, CurrentPage, limitShowOrder):
    with engine.connect() as conn:
        query_order_all_money = select(func.sum(detailOrder.c.totalCost.cast(Integer))).where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success', extract('year', detailOrder.c.realTimeArrive) == int(yearValue)))
        query_all_order = select(detailOrder).where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success', extract('year', detailOrder.c.realTimeArrive) == int(yearValue)))
        
        execute_order_all_money = conn.execute(query_order_all_money).scalar()

        if execute_order_all_money is None:
            all_money_year = 0
        else:
            all_money_year = int(execute_order_all_money)
            
        len_all_order_success = conn.execute(query_all_order).rowcount

        calcPaginationPage = paginationPageDS.calPagination(CurrentPage, limitShowOrder, len_all_order_success)

        all_order_success = conn.execute(query_all_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()

        lstDriver = []
        for index in all_order_success:
            driverTemp = conn.execute(driver.select().where(driver.c.idDriver == index[4])).fetchone()
            lstDriver.append(driverTemp)


    return [all_money_year, all_order_success, lstDriver, calcPaginationPage[0]]

# def calcWarehousingFee(id_ds, year_create):
#     check_finish_warehouse_fee = False

#     with engine.connect() as conn:
#         check_warehousing_fee_finish_query = select(warehousingFee.c.finish).where(and_(warehousingFee.c.idDeliverySystem == id_ds, extract('year', warehousingFee.c.timeFee) == int(year_create)))

#         if conn.execute(check_warehousing_fee_finish_query).scalar() == 'Success':
#             check_finish_warehouse_fee = True
#         else:
#             query_order = select(detailOrder).where(detailOrder.c.idDeliverySystem == id_ds)
#             len_all_order = conn.execute(query_order).rowcount
            
#             storage_fee = len_all_order * 2000
#             th_fee = len_all_order * 3000
#             insurance_fee = len_all_order * 3000

#     if check_finish_warehouse_fee == False:
#         return [storage_fee, th_fee, insurance_fee, len_all_order]
#     else:
#         return 'Finish'

# def postWarehousingFee(id_ds, date_submit):
#     try:
#         with engine.connect() as conn:
#             update_order = warehousingFee.update().where(warehousingFee.c.idDeliverySystem == id_ds).values(dateFinish = date_submit, finish = 'Success')
#             conn.execute(update_order)

#             checkCurrentOS(conn)

#         return 'Success'
#     except:
#         return 'Fail'

# def getHumanResourceCost(id_ds, date_create):
#     # we need to define a variable flag to check human resource cost, if cost has been paid will return finish
#     # If cost hasn't been paid show all money
#     check_human_resource_cost = False 

#     with engine.connect() as conn:
#         query_check_human_resource_cost = humanResourceCost.select().where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#             humanResourceCost.c.finish == 'Not',
#             extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#             extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))

#         if conn.execute(query_check_human_resource_cost).rowcount == 0:
#             check_human_resource_cost = True
#         else:
#             # We have 3 levels in company is employee, head of department and ceo
#             # We assumption each person have 3 kind of money : salary fee, insurance fee and support fee
#             # support fee and insurance fee are the same with all person, and equal 1000 vnđ
#             # salary employee = 1000, salary head of department = 1500 and salary of ceo = 2000
#             employee_str = 'employee'
#             head_department_str = 'head_of_department'
#             ceo_str = 'CEO'

#             query_employee = select(humanResourceCost).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == employee_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             query_all_salary_employee = select(func.sum(humanResourceCost.c.salary.cast(Integer))).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == employee_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             query_all_insurance_employee = select(func.sum(humanResourceCost.c.insurance.cast(Integer))).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == employee_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             query_all_support_employee = select(func.sum(humanResourceCost.c.support.cast(Integer))).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == employee_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             len_employee = conn.execute(query_employee).rowcount
#             all_salary_employee = conn.execute(query_all_salary_employee).scalar() or 0
#             all_insurance_employee = conn.execute(query_all_insurance_employee).scalar() or 0
#             all_support_employee = conn.execute(query_all_support_employee).scalar() or 0

#             # HOD is abbreviation of head of department
#             query_HOD = select(humanResourceCost).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, humanResourceCost.c.levels == head_department_str, 
#                 humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             query_all_salary_HOD = select(func.sum(humanResourceCost.c.salary.cast(Integer))).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == head_department_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             query_all_insurance_HOD = select(func.sum(humanResourceCost.c.insurance.cast(Integer))).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == head_department_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             query_all_support_HOD = select(func.sum(humanResourceCost.c.support.cast(Integer))).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == head_department_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             len_HOD = conn.execute(query_HOD).rowcount
#             all_salary_HOD = conn.execute(query_all_salary_HOD).scalar() or 0
#             all_insurance_HOD = conn.execute(query_all_insurance_HOD).scalar() or 0
#             all_support_HOD = conn.execute(query_all_support_HOD).scalar() or 0 

#             query_ceo = select(humanResourceCost).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == ceo_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             query_all_salary_ceo = select(func.sum(humanResourceCost.c.salary.cast(Integer))).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == ceo_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             query_all_insurance_ceo = select(func.sum(humanResourceCost.c.insurance.cast(Integer))).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == ceo_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             query_all_support_ceo = select(func.sum(humanResourceCost.c.support.cast(Integer))).where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#                 humanResourceCost.c.levels == ceo_str, humanResourceCost.c.finish == 'Not',
#                 extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#                 extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:])))
#             len_ceo = conn.execute(query_ceo).rowcount
#             all_salary_ceo = conn.execute(query_all_salary_ceo).scalar() or 0
#             all_insurance_ceo = conn.execute(query_all_insurance_ceo).scalar() or 0
#             all_support_ceo = conn.execute(query_all_support_ceo).scalar() or 0

#     if check_human_resource_cost == False:
#         return {'employee' : [len_employee, all_salary_employee, all_insurance_employee, all_support_employee], 
#                 'head_of_department' : [len_HOD, all_salary_HOD, all_insurance_HOD, all_support_HOD], 
#                 'ceo' : [len_ceo, all_salary_ceo, all_insurance_ceo, all_support_ceo]}
#     else:
#         return 'Finish'

# def postHumanResourceCost(id_ds, date_create, date_finish):
#     try:
#         with engine.connect() as conn:
#             update_human_resource_cost = humanResourceCost.update().where(and_(humanResourceCost.c.idDeliverySystem == id_ds, 
#             extract('year', humanResourceCost.c.dateCreate) == int(date_create[:4]), 
#             extract('month', humanResourceCost.c.dateCreate) == int(date_create[5:]),
#             humanResourceCost.c.finish == 'Not')).values(finish = 'Success', dateFinish = date_finish)

#             conn.execute(update_human_resource_cost)

#             checkCurrentOS(conn)

#         return 'OK'
#     except:
#         return 'Fail'

# def getIDAddrMatchTel(tel_num_sender, tel_num_recv):
#     with engine.connect() as conn:
#         query_addr_sender = select(shippingAddr).where(shippingAddr.c.telNum  == tel_num_sender)
#         all_id_addr_sender = conn.execute(query_addr_sender).fetchall()
#         lst_id_addr_sender = [ids.idShippingAddr for ids in all_id_addr_sender]

#         query_addr_recv = select(deliveryAddr).where(deliveryAddr.c.telNum  == tel_num_recv)
#         all_id_addr_recv = conn.execute(query_addr_recv).fetchall()
#         lst_id_addr_recv = [ids.idDeliveryAddr for ids in all_id_addr_recv]

#         query_temp_a = select(detailOrder.c.idDetailOrder).where(detailOrder.c.idShippingAddr.in_(lst_id_addr_sender))
#         query_temp_b = select(detailOrder.c.idDetailOrder).where(detailOrder.c.idDeliveryAddr.in_(lst_id_addr_recv))
#         query_union_a_b = union(query_temp_a, query_temp_b)
        
#         all_id_detail_order = conn.execute(query_union_a_b).fetchall()
#         lst_id_detail_order = [row[0] for row in all_id_detail_order]
    
#     return lst_id_detail_order

# def getAllDetailOrder(lst_id):
#     with engine.connect() as conn:
#         query_order = select(detailOrder).where(detailOrder.c.idDetailOrder.in_(lst_id))
#         all_order = conn.execute(query_order).fetchall()
#     return all_order

# def checkOrderNumberExits(order_number):
#     with engine.connect() as conn:
#         query_order = select(detailOrder).where(detailOrder.c.codeOrder == order_number)
#         len_query_order = conn.execute(query_order).rowcount

#     if len_query_order == 0:
#         return 'Not'
#     else:
#         return 'OK'
    

# def updateEditOrder(order_number, name_sender, name_receiver, tel_sender, tel_receiver):
#     try:
#         with engine.connect() as conn:
#             query_get_id_addr = select(detailOrder.c.idShippingAddr, detailOrder.c.idDeliveryAddr).where(detailOrder.c.codeOrder == order_number)
#             get_id_addr = conn.execute(query_get_id_addr).fetchall()[0]

#             update_inform_shipping_addr = shippingAddr.update().where(shippingAddr.c.idShippingAddr == get_id_addr[0]).values(name = name_sender, telNum = tel_sender)
#             update_inform_delivery_addr = deliveryAddr.update().where(deliveryAddr.c.idDeliveryAddr == get_id_addr[1]).values(name = name_receiver, telNum = tel_receiver)

#             conn.execute(update_inform_shipping_addr)
#             conn.execute(update_inform_delivery_addr)

#             checkCurrentOS(conn)

#         return 'OK'
#     except:
#         return 'Fail'
    
# def drawChartDrawBarChartHomepage(idDS):
#     with engine.connect() as conn:
#         query_order_success = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success'))
#         query_order_pending = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Pending'))
#         query_order_need_delivery = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Need-Delivery'))
#         query_order_delivery = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Delivery'))

#         len_order_success = conn.execute(query_order_success).rowcount
#         len_order_pending = conn.execute(query_order_pending).rowcount
#         len_order_need_delivery = conn.execute(query_order_need_delivery).rowcount
#         len_order_delivery = conn.execute(query_order_delivery).rowcount
    
#     return [len_order_success, len_order_pending, len_order_need_delivery, len_order_delivery]

# def getDrawPieChartStaticDS(idDS):
#     with engine.connect() as conn:
#         query_pending = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Pending'))
#         query_need_delivery = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Need-Delivery'))
#         query_delivery = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Delivery'))
#         query_success = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success'))
#         query_all_order = detailOrder.select().where(detailOrder.c.idDeliverySystem == idDS)

#         orderPending = conn.execute(query_pending).rowcount
#         orderNeedDelivery = conn.execute(query_need_delivery).rowcount
#         orderInDelivery = conn.execute(query_delivery).rowcount
#         orderSuccess = conn.execute(query_success).rowcount
#         allOrder = conn.execute(query_all_order).rowcount

#         percentOrderPending = allOrder/orderPending
#         percentOrderNeedDelivery = allOrder/orderNeedDelivery
#         percentOrderDelivery = allOrder/orderInDelivery
#         percentOrderSuccess = allOrder/orderSuccess

#     return [percentOrderPending, percentOrderNeedDelivery, percentOrderDelivery, orderSuccess, percentOrderSuccess]

# def getSearchOrderByCodeOrder(idDS, CodeOrder):
#     with engine.connect() as conn:
#         querySearchOrder = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.codeOrder == CodeOrder))
#         searchOrderDS = conn.execute(querySearchOrder).fetchall()
    
#     return searchOrderDS

# def getSearchOrderDSByDay(idDS, chooseDate, CurrentPage, limitShowOrder):
#     with engine.connect() as conn:
#         dateChoose = datetime.datetime.strptime(chooseDate, f'%Y-%m-%d')

#         query_order = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, cast(detailOrder.c.dateCreate, Date) == dateChoose.date()))
#         len_lst_order = conn.execute(query_order).rowcount

#         calcPaginationPage = paginationPageDS.calPagination(CurrentPage,  limitShowOrder, len_lst_order)

#         all_order = conn.execute(query_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()
    
#     return [calcPaginationPage[0], all_order, len_lst_order]

# def getSearchOrderDSByMonth(idDS, chooseMonth, CurrentPage, limitShowOrder):
#     with engine.connect() as conn:
#         query_order = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, 
#             extract('year', detailOrder.c.dateCreate) == int(chooseMonth[:4]), 
#             extract('month', detailOrder.c.dateCreate) == int(chooseMonth[5:])))

#         len_lst_order = conn.execute(query_order).rowcount

#         calcPaginationPage = paginationPageDS.calPagination(CurrentPage,  limitShowOrder, len_lst_order)

#         all_order = conn.execute(query_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()
    
#     return [calcPaginationPage[0], all_order, len_lst_order]

# def getSearchOrderDSByYear(idDS, chooseYear, CurrentPage, limitShowOrder):
#     with engine.connect() as conn:
#         query_order = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, extract('year', detailOrder.c.dateCreate) == chooseYear))
#         len_lst_order = conn.execute(query_order).rowcount

#         calcPaginationPage = paginationPageDS.calPagination(CurrentPage,  limitShowOrder, len_lst_order)

#         all_order = conn.execute(query_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()

#     return [calcPaginationPage[0], all_order, len_lst_order]


# def getOrderAllMonth(idDS, yearValue):
#     with engine.connect() as conn:
#         lst_month_str = ['01', '02', '03', '04', '05', '06', '07', '06', '09', '10', '11', '12']
#         lst_order_create_each_month = []
#         lst_order_pending, lst_order_need_delivery, lst_order_delivery, lst_order_success = [], [], [], []
        
#         for index in lst_month_str:
#             query_order_date_create = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, 
#                 extract('year', detailOrder.c.dateCreate) == yearValue, extract('month', detailOrder.c.dateCreate) == index))
#             query_order_pending = query_order_date_create.where(detailOrder.c.status == 'Pending')
#             query_order_need_delivery = query_order_date_create.where(detailOrder.c.status == 'Need-Delivery')
#             query_order_delivery = query_order_date_create.where(detailOrder.c.status == 'Delivery')
#             query_order_success = query_order_date_create.where(detailOrder.c.status == 'Success')

#             lst_order_create_each_month.append(conn.execute(query_order_date_create).rowcount)
#             lst_order_pending.append(conn.execute(query_order_pending).rowcount)
#             lst_order_need_delivery.append(conn.execute(query_order_need_delivery).rowcount)
#             lst_order_delivery.append(conn.execute(query_order_delivery).rowcount)
#             lst_order_success.append(conn.execute(query_order_success).rowcount)

#     return [lst_order_create_each_month, lst_order_pending, lst_order_need_delivery, lst_order_delivery, lst_order_success]

# def getAllOrder(idDS, CurrentPage, limitShowOrder):
#     with engine.connect() as conn:
#         query_order = detailOrder.select().where(detailOrder.c.idDeliverySystem == idDS)
#         len_all_order = conn.execute(query_order).rowcount
#         len_order_pending = conn.execute(query_order.where(detailOrder.c.status == 'Pending')).rowcount
#         len_order_need_delivery = conn.execute(query_order.where(detailOrder.c.status == 'Need-Delivery')).rowcount
#         len_order_delivery = conn.execute(query_order.where(detailOrder.c.status == 'Delivery')).rowcount
#         len_order_success = conn.execute(query_order.where(detailOrder.c.status == 'Success')).rowcount

#         calcPaginationPage = paginationPageDS.calPagination(CurrentPage, limitShowOrder, len_all_order)

#         all_order = conn.execute(query_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()


#     return {'pagination' : calcPaginationPage[0], 'all_order' : all_order, 'len_all_order' : len_all_order, 
#         'len_all_order_pending' : len_order_pending, 'len_all_order_need_delivery' : len_order_need_delivery,
#         'len_all_order_delivery' : len_order_delivery, 'len_all_order_success' : len_order_success}

# def getStaticOrderByDescTime(idDS, CurrentPage, limitShowOrder):
#     with engine.connect() as conn:
#         query_order = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success'))
#         len_all_order = conn.execute(query_order).rowcount

#         calcPaginationPage = paginationPageDS.calPagination(CurrentPage, limitShowOrder, len_all_order)

#         all_order = conn.execute(query_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()
    
#     return {'pagination' : calcPaginationPage[0], 'all_order' : all_order, 'len_all_order' : len_all_order}

def getStaticDSDateTime(idDS, CurrentPage, date_value):
    try:
        query_order = Detail_Order_tb.objects.filter(id_delivery_system = idDS, status_order = 'Success', real_time_arrive__date = date_value)
        len_all_order = query_order.count()

        calcPaginationPage = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)

        all_order = query_order.order_by('-date_create')[calcPaginationPage[1] : calcPaginationPage[1] + limit_show_order].values()
        return {'pagination' : calcPaginationPage[0], 'all_order' : all_order, 'len_all_order' : len_all_order}
    except:
        return False

def getStaticDSMonthTime(idDS, CurrentPage, month_value):
    try:
        query_order = Detail_Order_tb.objects.filter(id_delivery_system = idDS, status_order = 'Success', real_time_arrive__year = int(month_value[:4]), real_time_arrive__month = int(month_value[5:]))

        len_all_order = query_order.count()

        calcPaginationPage = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)

        all_order = query_order.order_by('-date_create')[calcPaginationPage[1] : calcPaginationPage[1] + limit_show_order].values()

        return {'pagination' : calcPaginationPage[0], 'all_order' : all_order, 'len_all_order' : len_all_order}
    except:
        return False

def getStaticDSYearTime(idDS, CurrentPage, year_value):
    try:
        query_order = Detail_Order_tb.objects.filter(id_delivery_system = idDS, status_order = 'Success', real_time_arrive__year = int(year_value))
        
        len_all_order = query_order.count()

        calcPaginationPage = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)

        all_order = query_order.order_by('-date_create')[calcPaginationPage[1] : calcPaginationPage[1] + limit_show_order].values()

        return {'pagination' : calcPaginationPage[0], 'all_order' : all_order, 'len_all_order' : len_all_order}
    except:
        return False

# def getRevenueOrderSuccess(idDS, CurrentPage, limitShowOrder):
#     with engine.connect() as conn:
#         query_order = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Success'))
#         len_all_order = conn.execute(query_order).rowcount 

#         calcPaginationPage = paginationPageDS.calPagination(CurrentPage, limitShowOrder, len_all_order)

#         all_order = conn.execute(query_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()
    
#     return {'pagination' : calcPaginationPage[0], 'all_order' : all_order, 'len_all_order' : len_all_order}

# def getAllOrderSearchOrder(idDS, CurrentPage, limitShowOrder):
#     with engine.connect() as conn:
#         query_order = detailOrder.select().where(detailOrder.c.idDeliverySystem == idDS)
#         len_all_order = conn.execute(query_order).rowcount 

#         calcPaginationPage = paginationPageDS.calPagination(CurrentPage, limitShowOrder, len_all_order)

#         all_order = conn.execute(query_order.offset(calcPaginationPage[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()

#     return {'pagination' : calcPaginationPage[0], 'all_order' : all_order, 'len_all_order' : len_all_order}

# def postFailOrderByDriver(idDS, codeOrder, idDriver, failTime):
#     try:
#         with engine.connect() as conn:
#             insert_record = deliveryFailOrder.insert().values(idDetailOrder = idDS, idDriver = idDriver, failTime = failTime, idDeliverySystem = idDS, codeOrder = codeOrder)
#             conn.execute(insert_record)
#             update_record = detailOrder.update().where(and_(detailOrder.c.codeOrder == codeOrder, detailOrder.c.idDeliverySystem == idDS)).values(status = 'Return-Order')
#             conn.execute(update_record)
#             checkCurrentOS(conn)
#         return 'OK'
#     except:
#         return 'Fail'
    
def get_order_by_driver(idDS, idDriver, dateSearchTemp, CurrentPage):
    try:
        if dateSearchTemp == 'all':
            query_order = Detail_Order_tb.objects.filter(id_delivery_system = idDS, id_driver = idDriver, status_order = 'Success')
        else:
            query_order = Detail_Order_tb.objects.filter(id_delivery_system = idDS, id_driver = idDriver, status_order = 'Success', real_time_arrive__date = dateSearchTemp)
    
        len_all_order = query_order.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)
        all_order = query_order[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], all_order, len_all_order]
    except:
        return False

# def orderReturn(idDS, CurrentPage, limitShowOrder):
#     with engine.connect() as conn:
#         query_order = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.status == 'Return-Order'))

#         len_all_order = conn.execute(query_order).rowcount 
#         calcPaginationPage = paginationPageDS.calPagination(CurrentPage, limitShowOrder, len_all_order)
#         all_order = conn.execute(query_order.offset(calcPaginationPage[1]).limit(limitShowOrder)).fetchall()
    
#     return {'len_all_order' : len_all_order, 'pagination' : calcPaginationPage[0], 'all_order' : all_order}

# def getEachDriverOrder(idDS):
#     with engine.connect() as conn:
#         all_driver = conn.execute(select(driver.c.idDriver, driver.c.name).where(driver.c.idDeliverySystem == idDS)).fetchall()
#         dict_order_driver = {}

#         for index in all_driver:
#             id_driver_temp = str(index[0])
#             query_each_driver = select(detailOrder).where(and_(detailOrder.c.idDeliverySystem == idDS, detailOrder.c.idDriver == id_driver_temp))
#             len_order_each_driver = conn.execute(query_each_driver).rowcount

#             dict_order_driver[str(index[1])] = len_order_each_driver 
#     return dict_order_driver

