from django.shortcuts import render
from django.http import HttpResponse

# @DSProfile_blueprint.route('/ds-edit-driver-profile', methods = ['GET','POST'])
# def editDriverProfile():
#     id_ds = session.get('idDS')

#     if request.method == 'POST':
#         form_data = request.form 
#         notificationMsg = ''
#         id_driver = form_data['inputIdDriver']

#         if 'submitBasicInform' in form_data:
#             driver_user = form_data['inputNameDriver']
#             tel_driver = form_data['inputTelDriver']
#             gmail_driver = form_data['inputGmailDriver']
#             gender_driver = form_data['inputGenderDriver']

#             statusUpdateBasic = dsExecuteDatabase.updateBasicInformDriver(id_driver, driver_user, tel_driver, gmail_driver, gender_driver)
#             if statusUpdateBasic == 'OK':
#                 notificationMsg = 'Update basic information success'
#             else:
#                 notificationMsg = 'Update basic information fail'
#         elif 'submitAdvanceInform' in form_data:
#             province_driver = form_data['chooseProvinceDriverProfile']
#             district_driver = form_data['chooseDistrictDriverProfile']
#             ward_driver = form_data['chooseWardDriverProfile']
            
#             if province_driver == 'NotProvince' or district_driver == 'notDistrict' or ward_driver == 'NotWard':
#                 return redirect(url_for('.editDriverProfile', notificationMsg = "Update advanced information fail"))

#             statusUpdateAdvanced = dsExecuteDatabase.updateAdvancedInformDriver(id_driver, province_driver, district_driver, ward_driver)
#             if statusUpdateAdvanced == 'OK':
#                 notificationMsg = "Update advanced information success"
#             else:
#                 notificationMsg = "Update advanced information fail"
#         elif 'submitChangePass' in form_data:
#             first_pass = form_data['inputFirstPass']
#             second_pass = form_data['inputSecondPass']
#             load_dotenv()
#             secret_key = os.getenv('secret_key')

#             statusUpdatePass = dsExecuteDatabase.updatePasswordDriver(id_driver, first_pass, secret_key)
#             if statusUpdatePass == 'OK':
#                 notificationMsg = "Change password success"
#             else:
#                 notificationMsg = "Change password fail"
#         elif 'submitDriverLicense' in form_data:
#             full_name = form_data['inputFullNameLicenseDriver']
#             beginning_date = form_data['inputBeginningDateLicenseDriver']
#             class_motor = form_data['inputClassLicenseDriver']

#             statusUpdateDriverLicense = dsExecuteDatabase.updateDriverLicense(id_driver, full_name, beginning_date, class_motor)
#             if statusUpdateDriverLicense =='OK':
#                 notificationMsg = "Change driver license success"
#             else:
#                 notificationMsg = "Change driver license fail"
#         elif 'submitMotorRegCert' in form_data:
#             number_reg_cert = form_data['inputNumberRegCert']
#             palate_reg_cert = form_data['inputPalateRegCert']
#             branch_reg_cert = form_data['inputBranchRegCert']
#             color_reg_cert = form_data['inputColorRegCert']
#             capacity_motor_reg_cert = form_data['inputCapacityMotorRegCert']
#             first_reg_cert = form_data['inputFirstRegCert']

#             statusUpdateMotorRegCert = dsExecuteDatabase.updateDriverRegCert(id_driver, number_reg_cert, palate_reg_cert, branch_reg_cert, color_reg_cert, capacity_motor_reg_cert, first_reg_cert)
#             if statusUpdateMotorRegCert == 'OK':
#                 notificationMsg = 'Update motorbike registration certificate success'
#             else:
#                 notificationMsg = 'Update motorbike registration certificate fail'

#         return redirect(url_for('.editDriverProfile', notificationMsg = notificationMsg))


#     all_name_driver = dsExecuteDatabase.getAllDriver(id_ds)

#     notificationMsg = request.args.get('notificationMsg')
#     if notificationMsg is None:
#         return render_template('DSeditDriverProfile.html', allNameDriver = all_name_driver)
#     else:
#         return render_template('DSeditDriverProfile.html', allNameDriver = all_name_driver, notificationMsg = notificationMsg)

#@DSProfile_blueprint.route('/ds-fee', methods = ['GET','POST'])
# def dsProfileFee():
#     id_ds = session.get('idDS')
#     current_month_now = datetime.datetime.now().strftime("%Y-%m")
#     current_year_now = datetime.datetime.now().strftime("%Y")

#     if request.method == 'POST':
#         if 'submitWarehousingFee' in request.form:
#             execute_post = dsExecuteDatabase.postWarehousingFee(id_ds, datetime.datetime.now())
            
#             if execute_post == 'Success':
#                 return redirect(url_for('.dsProfileFee', notificationMsg = 'Update Warehousing Fee Success'))
#             else:
#                 return redirect(url_for('.dsProfileFee', notificationMsg = 'Update Warehousing Fee Fail'))
#         elif 'submitHumanResourceCost' in request.form:
#             execute_post = dsExecuteDatabase.postHumanResourceCost(id_ds, current_month_now, datetime.datetime.now())

#             if execute_post == 'OK':
#                 return redirect(url_for('.dsProfileFee', notificationMsg = 'Update Human Resource Cost Success'))
#             else:
#                 return redirect(url_for('.dsProfileFee', notificationMsg = 'Update Human Resource Cost Fail'))
    
#     warehousing_fee = dsExecuteDatabase.calcWarehousingFee(id_ds, current_year_now)
#     get_human_resource_cost = dsExecuteDatabase.getHumanResourceCost(id_ds, current_month_now)
    
#     locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')

#     if warehousing_fee == 'Finish':
#         storage_fee, th_fee, insurance_fee = 0, 0, 0
#         len_all_order = 0
#     else:
#         storage_fee = locale.format_string("%d", int(warehousing_fee[0]), grouping=True)
#         th_fee = locale.format_string("%d", int(warehousing_fee[1]), grouping=True)
#         insurance_fee = locale.format_string("%d", int(warehousing_fee[2]), grouping=True)

#         len_all_order = warehousing_fee[3]
    
#     if get_human_resource_cost == 'Finish':
#         get_human_resource_cost = 0
#     else:
#         get_human_resource_cost['employee'] = [locale.format_string("%d", fee, grouping=True) for fee in get_human_resource_cost['employee']]
#         get_human_resource_cost['head_of_department'] = [locale.format_string("%d", fee, grouping=True) for fee in get_human_resource_cost['head_of_department']]
#         get_human_resource_cost['ceo'] = [locale.format_string("%d", fee, grouping=True) for fee in get_human_resource_cost['ceo']]

#     return_context = {'storage_fee' : storage_fee, 'th_fee' : th_fee, 'insurance_fee' : insurance_fee, 'len_all_order' : len_all_order, 'human_resource_cost' : get_human_resource_cost}
    
#     notificationMsg = request.args.get('notificationMsg')

#     if notificationMsg is None:
#         return render_template('DSProfileFee.html', **return_context)
#     else:
#         return_context['notificationMsg'] = notificationMsg
#         return render_template('DSProfileFee.html', **return_context)

# @DSProfile_blueprint.route('/ds-edit-order', methods = ['GET','POST'])
# def dsEditOrder():
#     id_ds = session.get('idDS')

#     if request.method == 'POST':
#         if 'submitTelNum' in request.form:
#             input_tel_sender = request.form['inputTelSender']
#             input_tel_recv = request.form['inputTelRecv']

#             execute_database = dsExecuteDatabase.getIDAddrMatchTel(input_tel_sender, input_tel_recv)
            
#             return redirect(url_for('.dsEditOrder', lst_id_order = execute_database))
#         elif 'submitOrderNumber' in request.form:
#             input_order_number = request.form['inputOrderNumber']
#             execute_database = dsExecuteDatabase.checkOrderNumberExits(input_order_number)

#             if execute_database == 'Not':
#                 return redirect(url_for('.dsEditOrder', msg_notification = f'Not found any order match order number {input_order_number}'))
#             else:
#                 return redirect(url_for('.dsEditOrder', msg_notification = f'OK', input_order_number = input_order_number))

#         elif 'submitEditOrder' in request.form:
#             code_order_number = request.form['valueCodeOrderNumber']
#             input_name_sender = request.form['editNameSender']
#             input_name_receiver = request.form['editNameReceiver']
#             input_tel_sender = request.form['editTelSender']
#             input_tel_receiver = request.form['editTelReceiver']

#             execute_database = dsExecuteDatabase.updateEditOrder(code_order_number, input_name_sender, input_name_receiver,input_tel_sender, input_tel_receiver)
            
#             if execute_database == 'OK':
#                 return 'Update Successful'
#             else:
#                 return 'Have some problem when update, please update again or contact admin for more support'

#     msg_notification = request.args.get('msg_notification', 'None')
#     lst_id_order = request.args.getlist('lst_id_order')

#     if msg_notification == 'OK':
#         order_number_need_edit = request.args.get('input_order_number')
#         return render_template('DSEditOrder.html', edit_order = True, order_number_need_edit = order_number_need_edit)

#     if lst_id_order == []:
#         return render_template('DSEditOrder.html', msg_notification = msg_notification) 
#     else:
#         all_order = dsExecuteDatabase.getAllDetailOrder(lst_id_order)
#         return render_template('DSEditOrder.html', all_order = all_order, msg_notification = msg_notification) 

# @DSProfile_blueprint.route('/ds-show-all-order-by-tel/<int:CurrentPage>', methods = ['GET','POST'])
# def dsShowAllOrderByTel(CurrentPage = 1):
#     id_ds = session.get('idDS')

#     get_all_order = request.args.getlist('all_order')
#     all_order = [eval(row) for row in get_all_order]

#     calcPagination = paginationPageDS.calPagination(CurrentPage, limit_show_order, len(all_order))
#     all_order = all_order[calcPagination[1] : calcPagination[2]]

#     all_inform = giveInformation.giveInformationAboutOrder(all_order)

#     return render_template('dsShowAllOrderByTel.html', all_order = all_order, paginationPage = calcPagination[0],
#         lstSender = all_inform[0], lstReceiver = all_inform[1], lstProduct = all_inform[2], CurrentPage = CurrentPage)
