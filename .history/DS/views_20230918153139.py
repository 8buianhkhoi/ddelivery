from django.shortcuts import render

# Create your views here.

# from flask import Blueprint, render_template, session, request, redirect, url_for
# from models.models import *
# from sqlalchemy import and_
# from datetime import datetime
# import jwt
# import os 
# from dotenv import load_dotenv

# from . import countTimeExits
# from . import giveInformation
# from . import drawChart
# from . import dsExecuteDatabase

# deliverySystem_blueprint = Blueprint('deliverySystem', __name__, static_folder = 'static', template_folder = 'templates')
# limitShowOrder = 30

# @deliverySystem_blueprint.before_request
# def checkCurrentToken():
#     if 'tokenDeliveryWedDemo' not in session:
#         return redirect(url_for('loginAccountBp.loginAccount'))
#     load_dotenv()
#     secretKey = os.getenv('secret_key')
#     decodeToken = jwt.decode(session['tokenDeliveryWedDemo'], secretKey, algorithms = ["HS256"])

#     currentTime = datetime.now()
#     startTimeToken = datetime.strptime(decodeToken['startTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')
#     endTimeToken = datetime.strptime(decodeToken['endTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')

#     if currentTime > startTimeToken and currentTime < endTimeToken:
#         roleUserTemp = decodeToken['roleUser']
#         if roleUserTemp == 'DS':
#             session['roleUser'] = 'User'
#             session['idDS'] = decodeToken['idRoleUser']
#         else:
#             return "<p>You are not login at role user, so you can't access this page</p>"
#     else:
#         return redirect(url_for('loginAccountBp.loginAccount', notificationEndToken = True))

# # Route này route cơ bản để khởi tạo
# @deliverySystem_blueprint.route('/')
# def deliverySystem():
#     idDS = session['idDS']
#     imageChart = drawChart.drawBarChartHomepage(idDS = idDS)
#     totalMoneyChart = drawChart.drawTotalMoneyChart(idDS = idDS)
#     statusOrderEachMonth = drawChart.drawChartOrderCreateEachMonth(idDS = idDS)
#     len_order_fail = dsExecuteDatabase.orderReturn(idDS, 1, limitShowOrder)['len_all_order']
#     chart_each_driver_order = drawChart.drawChartOrderEachDriver(idDS)

#     return render_template('baseDeliverySystem.html', imageChartBase = imageChart, totalMoneyChart = totalMoneyChart, 
#         statusOrderEachMonth = statusOrderEachMonth, len_order_fail = len_order_fail, status ='Homepage', chart_each_driver_order = chart_each_driver_order)

# # Show all order in Delivery
# @deliverySystem_blueprint.route('/in-transit/<CurrentPage>')
# def deliveryInTransit(CurrentPage = 1):
#     id_ds = session['idDS']

#     execute_database = dsExecuteDatabase.getOrderInDelivery(id_ds, CurrentPage, limitShowOrder)
#     allOrderDelivery = execute_database[1]
#     lstDriver = execute_database[2]

#     allInform = giveInformation.giveInformationAboutOrder(allOrderDelivery)

#     dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(allInform[0])
#     dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(allInform[1])
#     sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrderDelivery)      
#     dictDriverTimes = countTimeExits.countTimeExitsDriver(lstDriver)

#     return render_template('deliveryTransit.html', allOrderDelivery = allOrderDelivery, lstSender = allInform[0], lstReceiver = allInform[1], dictDriverTimes = dictDriverTimes,
#             lstType = allInform[2], lstDriver = lstDriver, paginationPage = execute_database[0], CurrentPage = int(CurrentPage), sumMoneyOrder = sumMoneyOrder, 
#             dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, len_all_order = execute_database[3])
        
# # Show all order in delivery success
# @deliverySystem_blueprint.route("/order-successful/<CurrentPage>")
# def orderDeliverySuccessful(CurrentPage = 1):
#     id_ds = session['idDS']

#     execute_database = dsExecuteDatabase.getOrderDeliverySuccess(id_ds, CurrentPage, limitShowOrder)
#     lstDriver = execute_database[2]
#     allOrderSuccess = execute_database[1]

#     allInform = giveInformation.giveInformationAboutOrder(allOrderSuccess)

#     dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(allInform[0])
#     dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(allInform[1])
#     sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrderSuccess)
#     dictDriverTimes = countTimeExits.countTimeExitsDriver(lstDriver)

#     return render_template('orderDeliverySuccess.html', allOrderSuccess = allOrderSuccess, lstSender = allInform[0], 
#             lstReceiver = allInform[1], lstType = allInform[2], lstDriver = lstDriver, CurrentPage = int(CurrentPage), paginationPage = execute_database[0],
#             dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, sumMoneyOrder = sumMoneyOrder, dictDriverTimes = dictDriverTimes,
#             len_all_order = execute_database[3])

# # Show all order need delivery. DS will assign each other for driver
# @deliverySystem_blueprint.route('/in-pending/<int:CurrentPage>', methods= ['GET', 'POST'])
# def orderInPending(CurrentPage = 1, notificationMsg = []):
#     id_ds = session['idDS']

#     execute_database = dsExecuteDatabase.getOrderPending(id_ds, CurrentPage, limitShowOrder)
#     allDriver = execute_database[2]
#     allProductNeedDelivery = execute_database[1]
#     len_all_order = execute_database[3]

#     allInform = giveInformation.giveInformationAboutOrder(allProductNeedDelivery)

#     notificationMsg = request.args.getlist('notificationMsg')

#     if request.method == 'POST':
#         # if 'autoUpdateDriver' in request.form:
#         #     return redirect(url_for('autoUpdateShipperDS.autoUpdateShipper'))

#         lstMessageUpdate = []
#         for index in range(0, len(allProductNeedDelivery)):
#             eachOrderTemp = request.form[f'updateShipper{index}']
#             if eachOrderTemp == 'notValue':
#                 continue
#             else:
#                 driverName = eachOrderTemp.split(",")[2]
#                 codeOrder = eachOrderTemp.split(",")[1]
#                 idDriver = int(eachOrderTemp.split(",")[0])

#                 message_update = dsExecuteDatabase.postOrderPending(id_ds, codeOrder, idDriver, driverName)
#                 lstMessageUpdate.append(message_update)
#         return redirect(url_for('.orderInPending', CurrentPage = 1, notificationMsg = lstMessageUpdate))
    
#     dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(allInform[0])
#     dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(allInform[1])
#     sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allProductNeedDelivery)

#     contextReturnDict = {'allProductNeedDelivery' : allProductNeedDelivery, 'allDriver' : allDriver, 'dictCountryDeliveryAddr' : dictCountryDeliveryAddr,
#         'sumMoneyOrder' : sumMoneyOrder, 'lstSender' : allInform[0], 'CurrentPage' : CurrentPage, 'paginationPage' : execute_database[0], 'lstReceiver' : allInform[1],
#         'lstType' : allInform[2], 'dictCountryShippingAddr' : dictCountryShippingAddr, 'len_all_order' : len_all_order}

#     if notificationMsg:
#         contextReturnDict['notificationMsg'] = notificationMsg

#     return render_template('orderInPending.html', **contextReturnDict)
                

# @deliverySystem_blueprint.route('/need-delivery/<CurrentPage>')
# def orderNeedDelivery(CurrentPage = 1):
#     id_ds = session['idDS']
    
#     execute_database = dsExecuteDatabase.getOrderNeedDelivery(id_ds, CurrentPage, limitShowOrder)

#     allOrderNeedDelivery = execute_database[1]
#     allDriverName = execute_database[2]
#     lstDriverFull = execute_database[3]
#     len_all_order = execute_database[4]

#     allInform = giveInformation.giveInformationAboutOrder(allOrderNeedDelivery)

#     dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(allInform[0])
#     dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(allInform[1])
#     sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrderNeedDelivery)
#     dictDriverTimes = countTimeExits.countTimeExitsDriver(lstDriverFull)

#     return render_template('orderNeedDelivery.html', allOrderNeedDelivery = allOrderNeedDelivery, allDriverName = allDriverName, lstSender = allInform[0],
#             lstReceiver = allInform[1], lstProduct = allInform[2], paginationPage = execute_database[0], CurrentPage = int(CurrentPage), sumMoneyOrder = sumMoneyOrder,
#             dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, dictDriverTimes = dictDriverTimes,
#             len_all_order = len_all_order)

# # Log out
# @deliverySystem_blueprint.route('/logout')
# def logoutDS():
#     session.clear()
#     return redirect(url_for('homepageBp.homepage'))

# # This route use for static for all order
# @deliverySystem_blueprint.route('/get-static', methods= ['GET', 'POST'])
# def getStaticDS():
#     id_ds = session['idDS']
#     allDriverDS = dsExecuteDatabase.getAllDriverDS(id_ds)

#     if request.method == 'POST':
#         if 'staticForEachDriver' in request.form:
#             idDriver = request.form['chooseIDDriver']
#             dateSearch = request.form['chooseDateForEachDriver']
#             if dateSearch == '':
#                 return redirect(url_for('getStaticDSBP.getStaticDSDriver', CurrentPage = 1, dateSearch = 'None', idDriver = idDriver))
#             else:
#                 return redirect(url_for('getStaticDSBP.getStaticDSDriver', CurrentPage = 1, dateSearch = dateSearch, idDriver = idDriver))
#         elif 'staticByDate' in request.form:
#             chooseDateForStatic = request.form['chooseDateForStatic']
#             return redirect(url_for('getStaticDSBP.getStaticDSDate', CurrentPage = 1, dateValue = chooseDateForStatic))
#         elif 'staticByMonth' in request.form:
#             chooseMonthForStatic = request.form['chooseMonthStaticMonth']
#             return redirect(url_for('getStaticDSBP.getStaticDSMonth', CurrentPage = 1, monthValue = chooseMonthForStatic))
#         elif 'staticByYear' in request.form:
#             chooseYearForStatic = request.form['chooseYearStaticYear']
#             return redirect(url_for('getStaticDSBP.getStaticDSYear', CurrentPage = 1, yearValue = chooseYearForStatic))
#         elif 'staticOrderByStatus' in request.form:
#             idDS = session['idDS']
#             pieChart = drawChart.drawPieChartStaticDS(idDS = idDS)
#             return render_template('staticDS.html', pieChart = pieChart, allDriverDS = allDriverDS)
#         elif 'showOrderDelivery' in request.form:
#             return redirect(url_for('.deliveryInTransit', CurrentPage = 1))
#         elif 'showOrderPending' in request.form:
#             return redirect(url_for('.orderInPending', CurrentPage = 1))
#         elif 'showOrderNeedDelivery' in request.form:
#             return redirect(url_for('.orderNeedDelivery', CurrentPage = 1))
#         elif 'showOrderSuccess' in request.form:
#             return redirect(url_for('.orderDeliverySuccessful', CurrentPage = 1))
#         elif 'submitMonthAllMoney' in request.form:
#             monthValue = request.form['inputMonthAllMoney']
#             return redirect(url_for('getStaticDSBP.totalMoneyOneMonth', monthValue = monthValue, CurrentPage = 1))
#         elif 'submitDateAllMoney' in request.form:
#             dayValue = request.form['inputDateAllMoney']
#             return redirect(url_for('getStaticDSBP.totalMoneyOneDay', dayValue = dayValue, CurrentPage = 1))
#         elif 'submitYearAllMoney' in request.form:
#             yearValue = request.form['inputYearAllMoney']
#             return redirect(url_for('getStaticDSBP.totalMoneyOneYear', yearValue = yearValue, CurrentPage = 1))

#     return render_template('staticDS.html', allDriverDS = allDriverDS)

# # Search order by code order
# @deliverySystem_blueprint.route('/search-order', methods= ['GET', 'POST'])
# def searchOrder():
#     if request.method == 'POST':
#         if 'searchOrderDS' in request.form:
#             orderSearchInput = request.form['inputSearchOrder']
#             return redirect(url_for('searchOrderDSBP.searchOrderByCodeOrder', CurrentPage = 1, CodeOrder = orderSearchInput))
#         elif 'submitChooseDay' in request.form:
#             dateSearchInput = request.form['chooseDateSearch']
#             return redirect(url_for('searchOrderDSBP.searchOrderDSByDay', CurrentPage = 1, chooseDate = dateSearchInput))
#         elif 'submitChooseMonth' in request.form:
#             monthSearchInput = request.form['chooseMonthSearch']
#             return redirect(url_for('searchOrderDSBP.searchOrderDSByMonth', CurrentPage = 1, chooseMonth = monthSearchInput))
#         elif 'submitChooseYear' in request.form:
#             yearSearchInput = request.form['chooseYearSearch']
#             return redirect(url_for('searchOrderDSBP.searchOrderDSByYear', CurrentPage = 1, chooseYear = yearSearchInput))
#         elif 'submitChooseUserTel' in request.form:
#             telSender = request.form['chooseUserSenderByTel']
#             telReceiver = request.form['chooseUserReceiverByTel']

#             # Check condition telSender and telReceiver
#             if telSender == '':
#                 telSender = 'None'
#             if telReceiver == '':
#                 telReceiver = 'None'
                
#             return redirect(url_for('searchOrderDSBP.searchOrderDSByTel', CurrentPage = 1, telSender = telSender, telReceiver = telReceiver))
        
#     return render_template('searchOrderDS.html')

# @deliverySystem_blueprint.route('/order-return/<int:CurrentPage>', methods= ['GET', 'POST'])
# def orderReturnDS(CurrentPage = 1):
#     id_ds = session.get('idDS')

#     execute_database = dsExecuteDatabase.orderReturn(id_ds, CurrentPage, limitShowOrder)
#     paginationPage = execute_database['pagination']
#     all_order = execute_database['all_order']
#     len_all_order = execute_database['len_all_order']

#     allInform = giveInformation.giveInformationAboutOrder(all_order)

#     return_para = {'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage,
#         'all_order' : all_order, 'len_all_order' : len_all_order, 'CurrentPage' : CurrentPage}
    
#     return render_template('orderReturn.html', **return_para)




