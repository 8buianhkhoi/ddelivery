from Homepage.models import *

# Cập nhật từng đơn hàng cho từng kho. Kho nào nhiều hơn capacity thì lưu sang kho tiếp theo. Nếu hết kho thì báo lỗi
# TODO viết thêm try catch
def update_id_inventory(idDeliveryAddress = "", codeOrderPara = "", allInventory = ''):
    if idDeliveryAddress != '':
        idDetailOrderTemp = Detail_Order_tb.objects.filter(id_delivery_addr = idDeliveryAddress).values()[0]['id']
    elif codeOrderPara != '':
        idDetailOrderTemp = Detail_Order_tb.objects.filter(code_order_delivery = codeOrderPara).values()[0]['id']
    allQuantity = Product_tb.objects.filter(id_detail_order = idDetailOrderTemp).values()[0]['quantity']
    idProductTemp = Product_tb.objects.filter(id_detail_order = idDetailOrderTemp).values()[0]['id']
    for inventory in allInventory:
        idInventoryTemp = inventory['id']
        # Trong table inventory có cột thứ 4 chứa thông tin sức chứa của kho
        capacityInventoryTemp = inventory['capacity_inventory']
        
        currentCapacity = Inventory_map_product_tb.objects.filter(id_inventory = idInventoryTemp).count()
        
        if currentCapacity >= capacityInventoryTemp:
            continue
        else:
            capacityCanContain = capacityInventoryTemp - currentCapacity
            if capacityCanContain >= allQuantity:
                Inventory_map_product_tb(
                    id_inventory = idInventoryTemp,
                    id_product = idProductTemp
                    quantity = allQuantity
                )

                allQuantity = 0
                break
            else:
                Inventory_map_product_tb(
                    id_inventory = idInventoryTemp,
                    id_product = idProductTemp,
                    quantity = capacityCanContain
                )

                allQuantity = allQuantity - capacityCanContain
                        
    if allQuantity > 0:
        return 'Error'
    else:
        return 'Success'