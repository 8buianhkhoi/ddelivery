from django.urls import path

from . import views
from . import profile_view

app_name = 'DS_app'

urlpatterns = [
    path('homepage/', views.base_DS_page, name = 'DS_homepage'),
    path('order-in-delivery/<int:current_page>/', views.order_delivery, name = 'DS_order_in_delivery'),
    path('order-successful/<int:current_page>/', views.order_success, name = 'DS_order_success'),
    path('order-in-pending/<int:current_page>/<str:notificationMsg>/', views.order_in_pending, name = 'DS_order_pending'),
    path('order-need-delivery/<int:current_page>/', views.order_need_delivery, name = 'DS_order_need_delivery'),
    path('log-out/', views.log_out, name = 'DS_log_out'),
    path('static/', views.get_static, name = 'DS_get_static'),
    path('static-order-by-driver/<dateSearch>/<idDriver>/<CurrentPage>/<driver_name>/', views.get_static_ds_driver, name = 'DS_static_order_by_driver'),
    path('static-by-date/<int:CurrentPage>/<dateValue>/', views.get_static_ds_date, name = 'DS_static_by_date'),
    path('static-by-month/<int:CurrentPage>/<monthValue>/', views.get_static_month, name = 'DS_static_by_month'),
    path('static-by-year/<int:CurrentPage>/<yearValue>/', views.get_static_year, name = 'DS_static_by_year'),
    path('static-total-money-one-month/<monthValue>/<int:CurrentPage>/', views.total_money_one_month, name = 'DS_total_money_one_month'),
    path('static-total-money-one-day/<dayValue>/<int:CurrentPage>/', views.total_money_one_day, name = 'DS_total_money_one_day'),
    path('static-total-money-one-year/<yearValue>/<int:CurrentPage>/', views.total_money_one_year, name = 'DS_total_money_one_year'),
    path('search-order/', views.search_order, name = 'DS_search_order'),
    path('search-by-code-order/<int:CurrentPage>/<CodeOrder>/<msgNotification>/', views.searchOrderByCodeOrder, name = 'DS_search_order_by_code'),
    path('search-by-day/<int:CurrentPage>/<chooseDate>/', views.searchOrderDSByDay, name = 'DS_search_order_by_day'),
    path('search-by-month/<int:CurrentPage>/<chooseMonth>/', views.searchOrderDSByMonth, name = 'DS_search_order_by_month'),
    path('search-by-year/<int:CurrentPage>/<chooseYear>/', views.searchOrderDSByYear, name = 'DS_search_order_by_year'),
    # path('search-by-tel/<telSender>/<telReceiver>/<int:CurrentPage>/', views.searchOrderDSByTel, name = 'DS_search_order_by_tel'),
    # path('search-order-all/<int:CurrentPage>/', views.searchOrderDSAll, name = 'DS_search_order_all'),
    # path('order-return/<int:CurrentPage>/', views.orderReturnDS, name = 'DS_order_return'),
    # path('profile/homepage/', views.baseDSProfile, name ='DS_base_profile'),
]