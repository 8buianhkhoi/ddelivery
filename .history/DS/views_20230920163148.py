from django.shortcuts import render, redirect
from django.http import HttpResponse
import datetime 
import locale

from . import exec_data
from . import get_inform_order
# Create your views here.

# @deliverySystem_blueprint.before_request
# def checkCurrentToken():
#     if 'tokenDeliveryWedDemo' not in session:
#         return redirect(url_for('loginAccountBp.loginAccount'))
#     load_dotenv()
#     secretKey = os.getenv('secret_key')
#     decodeToken = jwt.decode(session['tokenDeliveryWedDemo'], secretKey, algorithms = ["HS256"])

#     currentTime = datetime.now()
#     startTimeToken = datetime.strptime(decodeToken['startTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')
#     endTimeToken = datetime.strptime(decodeToken['endTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')

#     if currentTime > startTimeToken and currentTime < endTimeToken:
#         roleUserTemp = decodeToken['roleUser']
#         if roleUserTemp == 'DS':
#             session['roleUser'] = 'User'
#             session['idDS'] = decodeToken['idRoleUser']
#         else:
#             return "<p>You are not login at role user, so you can't access this page</p>"
#     else:
#         return redirect(url_for('loginAccountBp.loginAccount', notificationEndToken = True))


def base_DS_page(request):
    #     idDS = session['idDS']
    #     imageChart = drawChart.drawBarChartHomepage(idDS = idDS)
    #     totalMoneyChart = drawChart.drawTotalMoneyChart(idDS = idDS)
    #     statusOrderEachMonth = drawChart.drawChartOrderCreateEachMonth(idDS = idDS)
    #     len_order_fail = dsExecuteDatabase.orderReturn(idDS, 1, limitShowOrder)['len_all_order']
    #     chart_each_driver_order = drawChart.drawChartOrderEachDriver(idDS)

    #     return render_template('baseDeliverySystem.html', imageChartBase = imageChart, totalMoneyChart = totalMoneyChart, 
    #         statusOrderEachMonth = statusOrderEachMonth, len_order_fail = len_order_fail, status ='Homepage', chart_each_driver_order = chart_each_driver_order)
    return render(request, 'DS/base_ds.html')

# Show all order in Delivery, old name : deliveryInTransit, new name : order_delivery
def order_delivery(request, current_page = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.get_order_in_delivery(id_ds, current_page)
    all_order_delivery = execute_database[1]
    lst_driver = execute_database[2]

    all_inform = get_inform_order.get_information(all_order_delivery)
    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(allInform[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(allInform[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(all_order_delivery)      
    # dictDriverTimes = countTimeExits.countTimeExitsDriver(lst_driver)

    # dictDriverTimes = dictDriverTimes, sumMoneyOrder = sumMoneyOrder, dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr
    context_return = {'allOrderDelivery' : all_order_delivery, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstType' : all_inform[2], 'lstDriver' : lst_driver, 'paginationPage' : execute_database[0], 'CurrentPage' : current_page, 'len_all_order' : execute_database[3]}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, execute_database[0] + 1)]

    return render(request, 'DS/order_in_delivery.html', context_return)
        
# Show all order in delivery success
def order_success(request, current_page = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.get_order_success(id_ds, current_page)
    lstDriver = execute_database[2]
    all_order_success = execute_database[1]

    all_inform = get_inform_order.get_information(all_order_success)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(all_inform[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(all_inform[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(all_order_success)
    # dictDriverTimes = countTimeExits.countTimeExitsDriver(lstDriver)
    
    # dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, sumMoneyOrder = sumMoneyOrder, dictDriverTimes = dictDriverTimes
    context_return = {'allOrderSuccess' : all_order_success, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstType' : all_inform[2], 'lstDriver' : lstDriver, 'CurrentPage' : current_page, 'paginationPage' : execute_database[0], 'len_all_order' : execute_database[3]}

    context_return['range_pagination_page'] = [iteration for iteration in range(1, execute_database[0] + 1)]

    return render(request, 'DS/order_success.html', context_return)

# Show all order need delivery. DS will assign each other for driver
def order_in_pending(request, current_page = 1, notificationMsg = []):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.get_order_pending(id_ds, current_page)
    all_driver = execute_database[2]
    all_order_pending = execute_database[1]
    len_all_order = execute_database[3]

    all_inform = get_inform_order.get_information(all_order_pending)

    notificationMsg = request.GET.getlist('notificationMsg')
    if request.method == 'POST':
        # if 'autoUpdateDriver' in request.form:
        #     return redirect(url_for('autoUpdateShipperDS.autoUpdateShipper'))

        lstMessageUpdate = []
        for index in range(0, len(all_order_pending)):
            eachOrderTemp = request.POST.get(f'updateShipper{index}', None)
            if eachOrderTemp == 'notValue':
                continue
            else:
                driverName = eachOrderTemp.split(",")[2]
                codeOrder = eachOrderTemp.split(",")[1]
                idDriver = int(eachOrderTemp.split(",")[0])

                message_update = exec_data.post_order_pending(id_ds, codeOrder, idDriver, driverName)
                lstMessageUpdate.append(message_update)
        return redirect('DS_app:DS_order_pending', current_page = 1, notificationMsg = lstMessageUpdate)
    # TODO
    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(allInform[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(allInform[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(all_product_need_delivery)
    context_return = {'all_order_pending' : all_order_pending, 'allDriver' : all_driver, 'lstSender' : all_inform[0], 'CurrentPage' : current_page, 'paginationPage' : execute_database[0], 'lstReceiver' : all_inform[1], 'lstType' : all_inform[2], 'len_all_order' : len_all_order}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, execute_database[0] + 1)]

    if notificationMsg:
        context_return['notificationMsg'] = notificationMsg

    return render(request, 'DS/order_in_pending.html', context_return)            

def order_need_delivery(request ,current_page = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)
    
    execute_database = exec_data.get_order_need_delivery(id_ds, current_page)

    allOrderNeedDelivery = execute_database[1]
    allDriverName = execute_database[2]
    lstDriverFull = execute_database[3]
    len_all_order = execute_database[4]

    all_inform = get_inform_order.get_information(allOrderNeedDelivery)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(all_inform[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(all_inform[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrderNeedDelivery)
    # dictDriverTimes = countTimeExits.countTimeExitsDriver(lstDriverFull)

    #sumMoneyOrder = sumMoneyOrder,dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, dictDriverTimes = dictDriverTimes
    context_return = {'allOrderNeedDelivery' : allOrderNeedDelivery, 'allDriverName' : allDriverName, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstProduct' : all_inform[2], 'paginationPage' : execute_database[0], 'CurrentPage' : current_page, 'len_all_order' : len_all_order}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, execute_database[0] + 1)]

    return render(request, 'DS/order_need_delivery.html', context_return)

# Log out
def log_out(request):
    request.session.clear()
    return redirect('homepage_app:Homepage_homepage')

# This route use for static for all order
def get_static(request):
    id_ds = request.session.get('id_role_user_ddelivery', None)
    allDriverDS = exec_data.get_all_driver(id_ds)

    if request.method == 'POST':
        form_data = request.POST
        
        # TODO core more here
        if 'staticForEachDriver' in form_data:
            idDriver = request.POST.get('chooseIDDriver', None).split(',')[0]
            driverName = request.POST.get('chooseIDDriver', None).split(',')[1]
            dateSearch = request.POST.get('chooseDateForEachDriver', None)
            if dateSearch == '':
                return redirect('DS_app:DS_static_order_by_driver', CurrentPage = 1, dateSearch = 'None', idDriver = idDriver,driver_name = driverName)
            else:
                return redirect('DS_app:DS_static_order_by_driver', CurrentPage = 1, dateSearch = dateSearch, idDriver = idDriver, driver_name = driverName)
        elif 'staticByDate' in form_data:
            chooseDateForStatic = request.POST.get('chooseDateForStatic', None)
            return redirect('DS_app:DS_static_by_date', CurrentPage = 1, dateValue = chooseDateForStatic)
        elif 'staticByMonth' in form_data:
            chooseMonthForStatic = request.POST.get('chooseMonthStaticMonth', None)
            return redirect('DS_app:DS_static_by_month', CurrentPage = 1, monthValue = chooseMonthForStatic)
        elif 'staticByYear' in form_data:
            chooseYearForStatic = request.POST.get('chooseYearStaticYear', None)
            return redirect('DS_app:DS_static_by_year', CurrentPage = 1, yearValue = chooseYearForStatic)
        elif 'staticOrderByStatus' in form_data:
            idDS = request.session.get('id_role_user_ddelivery', None)
            # TODO code chart here
            # pieChart = drawChart.drawPieChartStaticDS(idDS = idDS)
            # return render_template('staticDS.html', pieChart = pieChart, allDriverDS = allDriverDS)

            return render(request, 'DS/static_ds.html', {'allDriverDS' : allDriverDS})
        elif 'showOrderDelivery' in form_data:
            return redirect('DS_app:DS_order_in_delivery', CurrentPage = 1)
        elif 'showOrderPending' in form_data:
            return redirect('DS_app:DS_order_pending', CurrentPage = 1)
        elif 'showOrderNeedDelivery' in form_data:
            return redirect('DS_app:DS_order_need_delivery', CurrentPage = 1)
        elif 'showOrderSuccess' in form_data:
            return redirect('DS_app:DS_order_success', CurrentPage = 1)
        elif 'submitMonthAllMoney' in form_data:
            monthValue = request.POST.get('inputMonthAllMoney', None)
            return redirect('DS_app:DS_total_money_one_month', monthValue = monthValue, CurrentPage = 1)
        elif 'submitDateAllMoney' in form_data:
            dayValue = request.POST.get('inputDateAllMoney', None)
            return redirect('DS_app:DS_total_money_one_day', dayValue = dayValue, CurrentPage = 1)
        elif 'submitYearAllMoney' in form_data:
            yearValue = request.POST.get('inputYearAllMoney', None)
            return redirect('DS_app:DS_total_money_one_year', yearValue = yearValue, CurrentPage = 1)

    return render(request, 'DS/static_ds.html', {'allDriverDS' : allDriverDS})

# Search order by code order
def search_order(request):
    if request.method == 'POST':
        form_data = request.POST
        
        if 'searchOrderDS' in form_data:
            orderSearchInput = request.POST.get('inputSearchOrder', None)
            
            return redirect('DS_app:DS_search_order_by_code', CurrentPage = 1, CodeOrder = orderSearchInput, msgNotification = None)
        elif 'submitChooseDay' in form_data:
            dateSearchInput = request.POST.get('chooseDateSearch', None)
            return redirect('DS_app:DS_search_order_by_day', CurrentPage = 1, chooseDate = dateSearchInput)
        elif 'submitChooseMonth' in form_data:
            monthSearchInput = request.POST.get('chooseMonthSearch', None)
            return redirect('DS_app:DS_search_order_by_month', CurrentPage = 1, chooseMonth = monthSearchInput)
        elif 'submitChooseYear' in form_data:
            yearSearchInput = request.POST.get('chooseYearSearch', None)
            return redirect(url_for('searchOrderDSBP.searchOrderDSByYear', ))
            return redirect('DS_app:DS_search_order_by_year', CurrentPage = 1, chooseYear = yearSearchInput)
        elif 'submitChooseUserTel' in form_data:
            telSender = request.POST.get('chooseUserSenderByTel', None)
            telReceiver = request.POST.get('chooseUserReceiverByTel', None)

            # Check condition telSender and telReceiver
            if telSender == '':
                telSender = 'None'
            if telReceiver == '':
                telReceiver = 'None'
                
            return redirect('DS_app:DS_search_order_by_tel', CurrentPage = 1, telSender = telSender, telReceiver = telReceiver)
        
    return render(request, 'DS/search_order.html')

# TODO
def orderReturnDS(CurrentPage = 1):
    id_ds = session.get('idDS')

    execute_database = dsExecuteDatabase.orderReturn(id_ds, CurrentPage, limitShowOrder)
    paginationPage = execute_database['pagination']
    all_order = execute_database['all_order']
    len_all_order = execute_database['len_all_order']

    allInform = giveInformation.giveInformationAboutOrder(all_order)

    return_para = {'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage,
        'all_order' : all_order, 'len_all_order' : len_all_order, 'CurrentPage' : CurrentPage}
    
    return render_template('orderReturn.html', **return_para)

# TODO
def baseDSProfile():
    return render_template('baseDSProfile.html')


def get_static_ds_driver(request, dateSearch, idDriver, CurrentPage, driver_name ):
    if request.method == 'POST':
        form_data = request.POST.get('chooseKindTime')

        if 'all' in form_data:
            id_driver = request.POST.get('chooseDriverStatic').split(',')[0]
            driver_name = request.POST.get('chooseDriverStatic').split(',')[1]
            return redirect('DS_app:DS_static_order_by_driver', CurrentPage = 1, dateSearch = 'all', idDriver = id_driver, driver_name = driver_name)
        elif 'date' in form_data:
            id_driver = request.POST.get('chooseDriverStatic').split(',')[0]
            date_input = request.POST.get('chooseTimeDate')
            driver_name = request.POST.get('chooseDriverStatic').split(',')[1]
            return redirect('DS_app:DS_static_order_by_driver', CurrentPage = 1, dateSearch = date_input, idDriver = id_driver, driver_name = driver_name)

    idDS = request.session.get('id_role_user_ddelivery', None)

    get_all_driver = exec_data.get_all_driver_ds(idDS)
    # driver_name = request.GET.get('driver_name')

    if idDriver is None and dateSearch is None:
        return render(request, 'DS/static_ds_driver.html', {'get_all_driver' : get_all_driver})
    else:
        if dateSearch == 'all':
            execute_database = exec_data.get_order_by_driver(idDS, idDriver, 'all', CurrentPage)
        elif dateSearch != '':
            dateSearchTemp = datetime.datetime.strptime(dateSearch, f'%Y-%m-%d').date()
            execute_database = exec_data.get_order_by_driver(idDS, idDriver, dateSearchTemp, CurrentPage)

        allOrderByDriver = execute_database[1]
        paginationPage = execute_database[0]
        len_all_order = execute_database[2]
        all_inform = get_inform_order.get_information(allOrderByDriver)

        # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(informOrder[0])
        # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(informOrder[1])
        # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrderByDriver)

        #'dictCountryShippingAddr' : dictCountryShippingAddr, 'dictCountryDeliveryAddr' : dictCountryDeliveryAddr,'sumMoneyOrder' : sumMoneyOrder,
        return_para = {'allOrderByDriver' : allOrderByDriver, 'idDriver' : idDriver, 'paginationPage' : paginationPage,
            'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstType' : all_inform[2], 'dateSearch' : dateSearch,
            'CurrentPage' : CurrentPage,  'get_all_driver' : get_all_driver, 'len_all_order' : len_all_order}
        
        return_para['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

        if driver_name is not None:
            return_para['driver_name'] = driver_name
        
        return render(request, 'DS/static_ds_driver.html', return_para)

def get_static_ds_date(request, CurrentPage = 1, dateValue = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)
    if dateValue == 'None':
        return HttpResponse('<p>Not choose Date, back choose Date</p>')

    chooseDate = datetime.datetime.strptime(dateValue, f'%Y-%m-%d').date()

    execute_database = exec_data.getStaticDSDateTime(idDS, CurrentPage, chooseDate)
    paginationPage = execute_database['pagination']
    allOrder = execute_database['all_order']
    len_all_order = execute_database['len_all_order']

    informOrder = get_inform_order.get_information(allOrder)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(informOrder[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(informOrder[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrder)

    # dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, sumMoneyOrder = sumMoneyOrder
    context_return = {'allOrder' : allOrder, 'lstSender' : informOrder[0], 'lstReceiver' : informOrder[1], 'lstType' : informOrder[2], 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage , 'dateValue' : dateValue, 'len_all_order' : len_all_order}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

    return render(request, 'DS/static_ds_filter.html', context_return)

def get_static_month(request, CurrentPage = 1, monthValue = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)
    if monthValue == "None":
        return HttpResponse('<p>Not choose Month, back choose month</p>')

    execute_database = exec_data.getStaticDSMonthTime(idDS, CurrentPage, monthValue)
    paginationPage = execute_database['pagination']
    allOrder = execute_database['all_order']
    len_all_order = execute_database['len_all_order']
    
    informOrder = get_inform_order.get_information(allOrder)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(informOrder[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(informOrder[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrder)

    # dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, sumMoneyOrder = sumMoneyOrder
    context_return = {'allOrder' : allOrder, 'lstSender' : informOrder[0], 'lstReceiver' : informOrder[1], 'lstType' : informOrder[2], 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage, 'monthValue' : monthValue, 'len_all_order' : len_all_order}
    
    return render(request, 'DS/static_ds_filter.html', context_return)

def get_static_year(request, CurrentPage = 1, yearValue = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)
    
    if yearValue == "None":
        return HttpResponse('<p>Not choose Year, back choose year</p>')

    execute_database = exec_data.getStaticDSYearTime(idDS, CurrentPage, yearValue)
    paginationPage = execute_database['pagination']
    allOrder = execute_database['all_order']
    len_all_order = execute_database['len_all_order']
    
    informOrder = get_inform_order.get_information(allOrder)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(informOrder[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(informOrder[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrder)

    # TODO code chart here
    # allOrderEachMonth =[0]*12
    # for order in allOrder:
    #     monthEachOther = order[12].month
    #     allOrderEachMonth[monthEachOther-1] += 1

    # imageChart = drawChart.drawBarChartStaticDSYear(allOrderEachMonth, yearValue)

    # imageChart = imageChart, dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, sumMoneyOrder = sumMoneyOrder
    context_return = {'allOrder' : allOrder, 'lstSender' : informOrder[0], 'lstReceiver' : informOrder[1], 'lstType' : informOrder[2], 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage, 'yearValue' : yearValue, 'len_all_order' : len_all_order}
    
    return render(request, 'DS/static_ds_filter.html', context_return)

def total_money_one_month(request, monthValue = 'None', CurrentPage = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.getTotalMoneyMonth(monthValue, id_ds, CurrentPage)
    total_money = execute_database[0]
    all_order = execute_database[1]
    all_driver = execute_database[2]
    paginationPage = execute_database[3]
    
    locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')
    total_money = locale.format_string("%d", total_money, grouping=True)
    
    allInform = get_inform_order.get_information(all_order)

    context_return = {'total_money' : total_money, 'monthValue' : monthValue, 'all_order' : all_order, 'lstDriver' : all_driver, 'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage, 'CurrentPage' : CurrentPage}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
    
    return render(request, 'DS/total_money_static.html', context_return)

def total_money_one_day(request, dayValue = 'None', CurrentPage = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.getTotalMoneyDay(dayValue, id_ds, CurrentPage)
    total_money = execute_database[0]
    all_order = execute_database[1]
    all_driver = execute_database[2]
    paginationPage = execute_database[3]
    
    locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')
    total_money = locale.format_string("%d", total_money, grouping=True)

    allInform = get_inform_order.get_information(all_order)

    context_return = {'total_money' : total_money, 'total_money' : dayValue, 'all_order' : all_order, 'lstDriver' : all_driver, 'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage, 'CurrentPage' : CurrentPage}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
    
    return render(request, 'DS/total_money_static.html', context_return)

def total_money_one_year(request, yearValue = 'None', CurrentPage = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.getTotalMoneyYear(yearValue, id_ds, CurrentPage)
    total_money = execute_database[0]
    all_order = execute_database[1]
    all_driver = execute_database[2]
    paginationPage = execute_database[3]
    
    locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')
    total_money = locale.format_string("%d", total_money, grouping=True)

    allInform = get_inform_order.get_information(all_order)

    context_return = {'total_money' : total_money, 'yearValue' : yearValue, 'all_order' : all_order, 'lstDriver' : all_driver, 'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage, 'CurrentPage' : CurrentPage}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

    return render(request, 'DS/total_money_static.html', context_return)

def searchOrderByCodeOrder(request, CurrentPage = 1, CodeOrder ='None', msgNotification = None):
    idDS = request.session.get('id_role_user_ddelivery', None)
    searchOrderDS = exec_data.getSearchOrderByCodeOrder(idDS, CodeOrder)

    if request.method == 'POST':
        form_data = request.POST.get('chooseOptionSearchOrderByCode', None)

        # TODO
        # if 'deliveryFail' in form_data:
        #     execute_database = exec_data.postFailOrderByDriver(idDS, CodeOrder, searchOrderDS[0][4], datetime.now())

        #     if execute_database == 'OK': 
        #         return redirect(url_for('.searchOrderByCodeOrder', CurrentPage = 1, 
        #             CodeOrder = CodeOrder, msgNotification = f'Update Order {CodeOrder} : Product Return'))
        #     else:
        #         return redirect(url_for('.searchOrderByCodeOrder', CurrentPage = 1, CodeOrder = CodeOrder))
    
    informOrder = get_inform_order.get_information(searchOrderDS)

    return_para = {'searchOrderDS' : searchOrderDS, 'lstSender' : informOrder[0], 'lstReceiver' : informOrder[1], 'lstType' : informOrder[2]}

    if msgNotification is not None:
        return_para['msgNotification'] = msgNotification

    return render(request, 'DS/searchOrderbyCode.html', return_para)

def searchOrderDSByDay(request, CurrentPage = 1, chooseDate = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)
    
    execute_database = exec_data.getSearchOrderDSByDay(idDS, chooseDate, CurrentPage)
    lstOrder = execute_database[1]
    paginationPage = execute_database[0]
    len_all_order = execute_database[2]

    lstInform = get_inform_order.get_information(lstOrder)

    context_return = {'lstOrder' : lstOrder, 'lstSender' : lstInform[0], 'lstReceiver' : lstInform[1], 'lstProduct' : lstInform[2], 'dayValue' : chooseDate, 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage, 'len_all_order' : len_all_order}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

    return render(request, 'DS/searchOrderDSFilter.html', context_return)

def searchOrderDSByMonth(request, CurrentPage = 1, chooseMonth = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.getSearchOrderDSByMonth(idDS, chooseMonth, CurrentPage)
    lstOrder = execute_database[1]
    paginationPage = execute_database[0]
    len_all_order = execute_database[2]

    lstInform = get_inform_order.get_information(lstOrder)

    context_return = {'lstOrder' : lstOrder, 'lstSender' : lstInform[0], 'lstReceiver' : lstInform[1], 'lstProduct' : lstInform[2], 'monthValue' : chooseMonth, 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage, 'len_all_order' : len_all_order}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

    return render(request, 'DS/searchOrderDSFilter.html', context_return)

def searchOrderDSByYear(request, CurrentPage = 1, chooseYear = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.getSearchOrderDSByYear(idDS, chooseYear, CurrentPage)
    lstOrder = execute_database[1]
    paginationPage = execute_database[0]
    len_all_order = execute_database[2]

    lstInform = get_inform_order.get_information(lstOrder)

    context_return = {'lstOrder' : lstOrder, 'lstSender' : lstInform[0], 'lstReceiver' : lstInform[1], 'lstProduct' : lstInform[2], 'yearValue' : chooseYear, 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage, 'len_all_order' : len_all_order}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

    return render(request, 'DS/searchOrderDSFilter.html', context_return)

def searchOrderDSByTel(request, CurrentPage = 1, telSender = 'None', telReceiver = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.get_order_by_tel(CurrentPage, telSender, telReceiver)
    lstOrder = execute_database['lstOrder']
    allInform = execute_database['allInform']
    paginationPage = execute_database['paginationPage']
    len_all_order = execute_database['len_all_order']

    context_return = {'lstOrder' : lstOrder, 'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage, 'telSender' : telSender, 'telReceiver' : telReceiver, 'len_all_order' : len_all_order} 
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
    return render(request, 'DS/searchOrderDSFilter.html', context_return)
    
def searchOrderDSAll(request, CurrentPage = 1):
    if request.method == 'POST':
        form_data = request.POST.get('chooseTypeSearch', None)
        # if 'all' in form_data:
        #     return redirect(url_for('.searchOrderDSAll', CurrentPage = 1))
        # elif 'code' in form_data:
        #     code_order = request.form['chooseCodeOrder']
        #     return redirect(url_for('.searchOrderByCodeOrder', CurrentPage = 1, CodeOrder = code_order))
        # elif 'day' in form_data:
        #     dayValue = request.form['chooseDateTime']
        #     return redirect(url_for('.searchOrderDSByDay', CurrentPage = 1, chooseDate = dayValue))
        # elif 'month' in form_data:
        #     monthValue = request.form['chooseMonthTime']
        #     return redirect(url_for('.searchOrderDSByMonth', CurrentPage = 1, chooseMonth = monthValue))
        # elif 'year' in form_data:
        #     yearValue = request.form['chooseYearTime']
        #     return redirect(url_for('.searchOrderDSByYear', CurrentPage = 1, chooseYear = yearValue))
        # elif 'tel' in form_data:
        #     tel_sender = request.form['chooseUserSenderByTel']
        #     tel_receiver = request.form['chooseUserReceiverByTel']
            
        #     if tel_sender == '':
        #         tel_sender = 'None'
        #     if tel_receiver == '':
        #         tel_receiver = 'None'
                
        #     return redirect(url_for('.searchOrderDSByTel', CurrentPage = 1, telSender = tel_sender, telReceiver = tel_receiver))

    idDS = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.getAllOrderSearchOrder(idDS, CurrentPage)
    paginationPage = execute_database['pagination']
    all_order = execute_database['all_order']
    len_all_order = execute_database['len_all_order']

    allInform = get_inform_order.get_information(all_order)

    return_para = {'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage,
        'all_order' : all_order, 'len_all_order' : len_all_order, 'CurrentPage' : CurrentPage}
    return_para['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
    
    return render(request, 'DS/searchOrderDSAll.html', return_para)