from Homepage.models import *

# Cập nhật từng đơn hàng cho từng kho. Kho nào nhiều hơn capacity thì lưu sang kho tiếp theo. Nếu hết kho thì báo lỗi
def update_id_inventory(idDeliveryAddress = "", codeOrderPara = "", allInventory = ''):
    if idDeliveryAddress != '':
        idDetailOrderTemp = conn.execute(detailOrder.select().where(detailOrder.c.idDeliveryAddr == idDeliveryAddress[0])).fetchone()[0]
        idDetailOrderTemp = Detail_Order_tb.objects.filter(id_delivery_addr = idDeliveryAddress[0]).values()
    elif codeOrderPara != '':
        idDetailOrderTemp = conn.execute(detailOrder.select().where(detailOrder.c.codeOrder == codeOrderPara)).fetchone()[0]
    allQuantity = conn.execute(product.select().where(product.c.idDetailOrder == idDetailOrderTemp)).fetchone()[4]
    idProductTemp = conn.execute(product.select().where(product.c.idDetailOrder == idDetailOrderTemp)).fetchone()[0]

    for inventory in allInventory:
        idInventoryTemp = inventory[0]
        # Trong table inventory có cột thứ 4 chứa thông tin sức chứa của kho
        capacityInventoryTemp = inventory[3]
        
        currentCapacity = len(conn.execute(inventoryMapProduct.select().where(inventoryMapProduct.c.idInventory == idInventoryTemp)).fetchall())
        
        if currentCapacity >= capacityInventoryTemp:
            continue
        else:
            capacityCanContain = capacityInventoryTemp - currentCapacity
            if capacityCanContain >= allQuantity:
                queryUpdateInventory = inventoryMapProduct.insert().values(
                    idInventory = idInventoryTemp,
                    idProduct = idProductTemp,
                    quantity = allQuantity
                )
                conn.execute(queryUpdateInventory)
                allQuantity = 0
                break
            else:
                queryUpdateInventory = inventoryMapProduct.insert().values(
                    idInventory = idInventoryTemp,
                    idProduct = idProductTemp,
                    quantity = capacityCanContain
                )
                conn.execute(queryUpdateInventory)
                allQuantity = allQuantity - capacityCanContain

            if platform.system() == 'Linux':
                conn.commit()
                conn.close()
                        
    if allQuantity > 0:
        return 'Error'
    else:
        return 'Success'