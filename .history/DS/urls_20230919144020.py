from django.urls import path

from . import views 

app_name = 'DS_app'

urlpatterns = [
    path('homepage/', views.base_DS_page, name = 'DS_homepage'),
    path('order-in-delivery/<int:current_page>/', views.order_delivery, name = 'DS_order_in_delivery'),
    path('order-successful/<int:current_page>/', views.order_success, name = 'DS_order_success'),
    path('order-in-pending/<int:current_page>/<str:notificationMsg>/', views.order_in_pending, name = 'DS_order_pending'),
    path('order-need-delivery/<int:current_page>/', views.order_need_delivery, name = 'DS_order_need_delivery')
]