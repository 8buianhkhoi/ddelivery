from django.shortcuts import render, redirect
from django.http import HttpResponse
import datetime 

from . import exec_data
from . import get_inform_order
# Create your views here.

# @deliverySystem_blueprint.before_request
# def checkCurrentToken():
#     if 'tokenDeliveryWedDemo' not in session:
#         return redirect(url_for('loginAccountBp.loginAccount'))
#     load_dotenv()
#     secretKey = os.getenv('secret_key')
#     decodeToken = jwt.decode(session['tokenDeliveryWedDemo'], secretKey, algorithms = ["HS256"])

#     currentTime = datetime.now()
#     startTimeToken = datetime.strptime(decodeToken['startTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')
#     endTimeToken = datetime.strptime(decodeToken['endTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')

#     if currentTime > startTimeToken and currentTime < endTimeToken:
#         roleUserTemp = decodeToken['roleUser']
#         if roleUserTemp == 'DS':
#             session['roleUser'] = 'User'
#             session['idDS'] = decodeToken['idRoleUser']
#         else:
#             return "<p>You are not login at role user, so you can't access this page</p>"
#     else:
#         return redirect(url_for('loginAccountBp.loginAccount', notificationEndToken = True))


def base_DS_page(request):
    #     idDS = session['idDS']
    #     imageChart = drawChart.drawBarChartHomepage(idDS = idDS)
    #     totalMoneyChart = drawChart.drawTotalMoneyChart(idDS = idDS)
    #     statusOrderEachMonth = drawChart.drawChartOrderCreateEachMonth(idDS = idDS)
    #     len_order_fail = dsExecuteDatabase.orderReturn(idDS, 1, limitShowOrder)['len_all_order']
    #     chart_each_driver_order = drawChart.drawChartOrderEachDriver(idDS)

    #     return render_template('baseDeliverySystem.html', imageChartBase = imageChart, totalMoneyChart = totalMoneyChart, 
    #         statusOrderEachMonth = statusOrderEachMonth, len_order_fail = len_order_fail, status ='Homepage', chart_each_driver_order = chart_each_driver_order)
    return render(request, 'DS/base_ds.html')

# Show all order in Delivery, old name : deliveryInTransit, new name : order_delivery
def order_delivery(request, current_page = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.get_order_in_delivery(id_ds, current_page)
    all_order_delivery = execute_database[1]
    lst_driver = execute_database[2]

    all_inform = get_inform_order.get_information(all_order_delivery)
    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(allInform[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(allInform[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(all_order_delivery)      
    # dictDriverTimes = countTimeExits.countTimeExitsDriver(lst_driver)

    # dictDriverTimes = dictDriverTimes, sumMoneyOrder = sumMoneyOrder, dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr
    context_return = {'allOrderDelivery' : all_order_delivery, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstType' : all_inform[2], 'lstDriver' : lst_driver, 'paginationPage' : execute_database[0], 'CurrentPage' : current_page, 'len_all_order' : execute_database[3]}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, execute_database[0] + 1)]

    return render(request, 'DS/order_in_delivery.html', context_return)
        
# Show all order in delivery success
def order_success(request, current_page = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.get_order_success(id_ds, current_page)
    lstDriver = execute_database[2]
    all_order_success = execute_database[1]

    all_inform = get_inform_order.get_information(all_order_success)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(all_inform[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(all_inform[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(all_order_success)
    # dictDriverTimes = countTimeExits.countTimeExitsDriver(lstDriver)
    
    # dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, sumMoneyOrder = sumMoneyOrder, dictDriverTimes = dictDriverTimes
    context_return = {'allOrderSuccess' : all_order_success, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstType' : all_inform[2], 'lstDriver' : lstDriver, 'CurrentPage' : current_page, 'paginationPage' : execute_database[0], 'len_all_order' : execute_database[3]}

    context_return['range_pagination_page'] = [iteration for iteration in range(1, execute_database[0] + 1)]

    return render(request, 'DS/order_success.html', context_return)

# Show all order need delivery. DS will assign each other for driver
def order_in_pending(request, current_page = 1, notificationMsg = []):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.get_order_pending(id_ds, current_page)
    all_driver = execute_database[2]
    all_order_pending = execute_database[1]
    len_all_order = execute_database[3]

    all_inform = get_inform_order.get_information(all_order_pending)

    notificationMsg = request.GET.getlist('notificationMsg')
    if request.method == 'POST':
        # if 'autoUpdateDriver' in request.form:
        #     return redirect(url_for('autoUpdateShipperDS.autoUpdateShipper'))

        lstMessageUpdate = []
        for index in range(0, len(all_order_pending)):
            eachOrderTemp = request.POST.get(f'updateShipper{index}', None)
            if eachOrderTemp == 'notValue':
                continue
            else:
                driverName = eachOrderTemp.split(",")[2]
                codeOrder = eachOrderTemp.split(",")[1]
                idDriver = int(eachOrderTemp.split(",")[0])

                message_update = exec_data.post_order_pending(id_ds, codeOrder, idDriver, driverName)
                lstMessageUpdate.append(message_update)
        return redirect('DS_app:DS_order_pending', current_page = 1, notificationMsg = lstMessageUpdate)
    # TODO
    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(allInform[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(allInform[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(all_product_need_delivery)
    context_return = {'all_order_pending' : all_order_pending, 'allDriver' : all_driver, 'lstSender' : all_inform[0], 'CurrentPage' : current_page, 'paginationPage' : execute_database[0], 'lstReceiver' : all_inform[1], 'lstType' : all_inform[2], 'len_all_order' : len_all_order}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, execute_database[0] + 1)]

    if notificationMsg:
        context_return['notificationMsg'] = notificationMsg

    return render(request, 'DS/order_in_pending.html', context_return)            

def order_need_delivery(request ,current_page = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)
    
    execute_database = exec_data.get_order_need_delivery(id_ds, current_page)

    allOrderNeedDelivery = execute_database[1]
    allDriverName = execute_database[2]
    lstDriverFull = execute_database[3]
    len_all_order = execute_database[4]

    all_inform = get_inform_order.get_information(allOrderNeedDelivery)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(all_inform[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(all_inform[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrderNeedDelivery)
    # dictDriverTimes = countTimeExits.countTimeExitsDriver(lstDriverFull)

    #sumMoneyOrder = sumMoneyOrder,dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, dictDriverTimes = dictDriverTimes
    context_return = {'allOrderNeedDelivery' : allOrderNeedDelivery, 'allDriverName' : allDriverName, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstProduct' : all_inform[2], 'paginationPage' : execute_database[0], 'CurrentPage' : current_page, 'len_all_order' : len_all_order}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, execute_database[0] + 1)]

    return render(request, 'DS/order_need_delivery.html', context_return)

# Log out
def log_out(request):
    request.session.clear()
    return redirect('homepage_app:Homepage_homepage')

# This route use for static for all order
def get_static(request):
    id_ds = request.session.get('id_role_user_ddelivery', None)
    allDriverDS = exec_data.get_all_driver(id_ds)

    if request.method == 'POST':
        form_data = request.POST
        
        # TODO core more here
        if 'staticForEachDriver' in form_data:
            idDriver = request.POST.get('chooseIDDriver', None).split(',')[0]
            driverName = request.POST.get('chooseIDDriver', None).split(',')[1]
            dateSearch = request.POST.get('chooseDateForEachDriver', None)
            if dateSearch == '':
                return redirect('DS_app:DS_static_order_by_driver', CurrentPage = 1, dateSearch = 'None', idDriver = idDriver,driver_name = driverName)
            else:
                return redirect('DS_app:DS_static_order_by_driver', CurrentPage = 1, dateSearch = dateSearch, idDriver = idDriver, driver_name = driverName)
        elif 'staticByDate' in form_data:
            chooseDateForStatic = request.POST.get('chooseDateForStatic', None)
            return redirect('DS_app:DS_static_by_date', CurrentPage = 1, dateValue = chooseDateForStatic)
        elif 'staticByMonth' in form_data:
            chooseMonthForStatic = request.POST.get('chooseMonthStaticMonth', None)
            return redirect('DS_app:DS_static_by_month', CurrentPage = 1, monthValue = chooseMonthForStatic)
        elif 'staticByYear' in form_data:
            chooseYearForStatic = request.POST.get('chooseYearStaticYear', None)
            return redirect('DS_app:DS_static_by_year', CurrentPage = 1, yearValue = chooseYearForStatic)
        elif 'staticOrderByStatus' in form_data:
            idDS = request.session.get('id_role_user_ddelivery', None)
            # TODO code chart here
            # pieChart = drawChart.drawPieChartStaticDS(idDS = idDS)
            # return render_template('staticDS.html', pieChart = pieChart, allDriverDS = allDriverDS)

            return render(request, 'DS/static_ds.html', {'allDriverDS' : allDriverDS})
        elif 'showOrderDelivery' in form_data:
            return redirect('DS_app:DS_order_in_delivery', CurrentPage = 1)
        elif 'showOrderPending' in form_data:
            return redirect('DS_app:DS_order_pending', CurrentPage = 1)
        elif 'showOrderNeedDelivery' in form_data:
            return redirect('DS_app:DS_order_need_delivery', CurrentPage = 1)
        elif 'showOrderSuccess' in form_data:
            return redirect('DS_app:DS_order_success', CurrentPage = 1)
        elif 'submitMonthAllMoney' in form_data:
            monthValue = request.POST.get('inputMonthAllMoney', None)
            return redirect('DS_app:DS_total_money_one_month', monthValue = monthValue, CurrentPage = 1)
        elif 'submitDateAllMoney' in form_data:
            dayValue = request.POST.get('inputDateAllMoney', None)
            return redirect('DS_app:DS_total_money_one_day', dayValue = dayValue, CurrentPage = 1)
        elif 'submitYearAllMoney' in form_data:
            yearValue = request.POST.get('inputYearAllMoney', None)
            return redirect('DS_app:DS_total_money_one_year', yearValue = yearValue, CurrentPage = 1)

    return render(request, 'DS/static_ds.html', {'allDriverDS' : allDriverDS})

# # Search order by code order
# @deliverySystem_blueprint.route('/search-order', methods= ['GET', 'POST'])
# def searchOrder():
#     if request.method == 'POST':
#         if 'searchOrderDS' in request.form:
#             orderSearchInput = request.form['inputSearchOrder']
#             return redirect(url_for('searchOrderDSBP.searchOrderByCodeOrder', CurrentPage = 1, CodeOrder = orderSearchInput))
#         elif 'submitChooseDay' in request.form:
#             dateSearchInput = request.form['chooseDateSearch']
#             return redirect(url_for('searchOrderDSBP.searchOrderDSByDay', CurrentPage = 1, chooseDate = dateSearchInput))
#         elif 'submitChooseMonth' in request.form:
#             monthSearchInput = request.form['chooseMonthSearch']
#             return redirect(url_for('searchOrderDSBP.searchOrderDSByMonth', CurrentPage = 1, chooseMonth = monthSearchInput))
#         elif 'submitChooseYear' in request.form:
#             yearSearchInput = request.form['chooseYearSearch']
#             return redirect(url_for('searchOrderDSBP.searchOrderDSByYear', CurrentPage = 1, chooseYear = yearSearchInput))
#         elif 'submitChooseUserTel' in request.form:
#             telSender = request.form['chooseUserSenderByTel']
#             telReceiver = request.form['chooseUserReceiverByTel']

#             # Check condition telSender and telReceiver
#             if telSender == '':
#                 telSender = 'None'
#             if telReceiver == '':
#                 telReceiver = 'None'
                
#             return redirect(url_for('searchOrderDSBP.searchOrderDSByTel', CurrentPage = 1, telSender = telSender, telReceiver = telReceiver))
        
#     return render_template('searchOrderDS.html')

# @deliverySystem_blueprint.route('/order-return/<int:CurrentPage>', methods= ['GET', 'POST'])
# def orderReturnDS(CurrentPage = 1):
#     id_ds = session.get('idDS')

#     execute_database = dsExecuteDatabase.orderReturn(id_ds, CurrentPage, limitShowOrder)
#     paginationPage = execute_database['pagination']
#     all_order = execute_database['all_order']
#     len_all_order = execute_database['len_all_order']

#     allInform = giveInformation.giveInformationAboutOrder(all_order)

#     return_para = {'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage,
#         'all_order' : all_order, 'len_all_order' : len_all_order, 'CurrentPage' : CurrentPage}
    
#     return render_template('orderReturn.html', **return_para)


# @DSProfile_blueprint.route('/')
# def baseDSProfile():
#     return render_template('baseDSProfile.html')

# @DSProfile_blueprint.route('/ds-edit-driver-profile', methods = ['GET','POST'])
# def editDriverProfile():
#     id_ds = session.get('idDS')

#     if request.method == 'POST':
#         form_data = request.form 
#         notificationMsg = ''
#         id_driver = form_data['inputIdDriver']

#         if 'submitBasicInform' in form_data:
#             driver_user = form_data['inputNameDriver']
#             tel_driver = form_data['inputTelDriver']
#             gmail_driver = form_data['inputGmailDriver']
#             gender_driver = form_data['inputGenderDriver']

#             statusUpdateBasic = dsExecuteDatabase.updateBasicInformDriver(id_driver, driver_user, tel_driver, gmail_driver, gender_driver)
#             if statusUpdateBasic == 'OK':
#                 notificationMsg = 'Update basic information success'
#             else:
#                 notificationMsg = 'Update basic information fail'
#         elif 'submitAdvanceInform' in form_data:
#             province_driver = form_data['chooseProvinceDriverProfile']
#             district_driver = form_data['chooseDistrictDriverProfile']
#             ward_driver = form_data['chooseWardDriverProfile']
            
#             if province_driver == 'NotProvince' or district_driver == 'notDistrict' or ward_driver == 'NotWard':
#                 return redirect(url_for('.editDriverProfile', notificationMsg = "Update advanced information fail"))

#             statusUpdateAdvanced = dsExecuteDatabase.updateAdvancedInformDriver(id_driver, province_driver, district_driver, ward_driver)
#             if statusUpdateAdvanced == 'OK':
#                 notificationMsg = "Update advanced information success"
#             else:
#                 notificationMsg = "Update advanced information fail"
#         elif 'submitChangePass' in form_data:
#             first_pass = form_data['inputFirstPass']
#             second_pass = form_data['inputSecondPass']
#             load_dotenv()
#             secret_key = os.getenv('secret_key')

#             statusUpdatePass = dsExecuteDatabase.updatePasswordDriver(id_driver, first_pass, secret_key)
#             if statusUpdatePass == 'OK':
#                 notificationMsg = "Change password success"
#             else:
#                 notificationMsg = "Change password fail"
#         elif 'submitDriverLicense' in form_data:
#             full_name = form_data['inputFullNameLicenseDriver']
#             beginning_date = form_data['inputBeginningDateLicenseDriver']
#             class_motor = form_data['inputClassLicenseDriver']

#             statusUpdateDriverLicense = dsExecuteDatabase.updateDriverLicense(id_driver, full_name, beginning_date, class_motor)
#             if statusUpdateDriverLicense =='OK':
#                 notificationMsg = "Change driver license success"
#             else:
#                 notificationMsg = "Change driver license fail"
#         elif 'submitMotorRegCert' in form_data:
#             number_reg_cert = form_data['inputNumberRegCert']
#             palate_reg_cert = form_data['inputPalateRegCert']
#             branch_reg_cert = form_data['inputBranchRegCert']
#             color_reg_cert = form_data['inputColorRegCert']
#             capacity_motor_reg_cert = form_data['inputCapacityMotorRegCert']
#             first_reg_cert = form_data['inputFirstRegCert']

#             statusUpdateMotorRegCert = dsExecuteDatabase.updateDriverRegCert(id_driver, number_reg_cert, palate_reg_cert, branch_reg_cert, color_reg_cert, capacity_motor_reg_cert, first_reg_cert)
#             if statusUpdateMotorRegCert == 'OK':
#                 notificationMsg = 'Update motorbike registration certificate success'
#             else:
#                 notificationMsg = 'Update motorbike registration certificate fail'

#         return redirect(url_for('.editDriverProfile', notificationMsg = notificationMsg))


#     all_name_driver = dsExecuteDatabase.getAllDriver(id_ds)

#     notificationMsg = request.args.get('notificationMsg')
#     if notificationMsg is None:
#         return render_template('DSeditDriverProfile.html', allNameDriver = all_name_driver)
#     else:
#         return render_template('DSeditDriverProfile.html', allNameDriver = all_name_driver, notificationMsg = notificationMsg)

# @DSProfile_blueprint.route('/ds-fee', methods = ['GET','POST'])
# def dsProfileFee():
#     id_ds = session.get('idDS')
#     current_month_now = datetime.datetime.now().strftime("%Y-%m")
#     current_year_now = datetime.datetime.now().strftime("%Y")

#     if request.method == 'POST':
#         if 'submitWarehousingFee' in request.form:
#             execute_post = dsExecuteDatabase.postWarehousingFee(id_ds, datetime.datetime.now())
            
#             if execute_post == 'Success':
#                 return redirect(url_for('.dsProfileFee', notificationMsg = 'Update Warehousing Fee Success'))
#             else:
#                 return redirect(url_for('.dsProfileFee', notificationMsg = 'Update Warehousing Fee Fail'))
#         elif 'submitHumanResourceCost' in request.form:
#             execute_post = dsExecuteDatabase.postHumanResourceCost(id_ds, current_month_now, datetime.datetime.now())

#             if execute_post == 'OK':
#                 return redirect(url_for('.dsProfileFee', notificationMsg = 'Update Human Resource Cost Success'))
#             else:
#                 return redirect(url_for('.dsProfileFee', notificationMsg = 'Update Human Resource Cost Fail'))
    
#     warehousing_fee = dsExecuteDatabase.calcWarehousingFee(id_ds, current_year_now)
#     get_human_resource_cost = dsExecuteDatabase.getHumanResourceCost(id_ds, current_month_now)
    
#     locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')

#     if warehousing_fee == 'Finish':
#         storage_fee, th_fee, insurance_fee = 0, 0, 0
#         len_all_order = 0
#     else:
#         storage_fee = locale.format_string("%d", int(warehousing_fee[0]), grouping=True)
#         th_fee = locale.format_string("%d", int(warehousing_fee[1]), grouping=True)
#         insurance_fee = locale.format_string("%d", int(warehousing_fee[2]), grouping=True)

#         len_all_order = warehousing_fee[3]
    
#     if get_human_resource_cost == 'Finish':
#         get_human_resource_cost = 0
#     else:
#         get_human_resource_cost['employee'] = [locale.format_string("%d", fee, grouping=True) for fee in get_human_resource_cost['employee']]
#         get_human_resource_cost['head_of_department'] = [locale.format_string("%d", fee, grouping=True) for fee in get_human_resource_cost['head_of_department']]
#         get_human_resource_cost['ceo'] = [locale.format_string("%d", fee, grouping=True) for fee in get_human_resource_cost['ceo']]

#     return_context = {'storage_fee' : storage_fee, 'th_fee' : th_fee, 'insurance_fee' : insurance_fee, 'len_all_order' : len_all_order, 'human_resource_cost' : get_human_resource_cost}
    
#     notificationMsg = request.args.get('notificationMsg')

#     if notificationMsg is None:
#         return render_template('DSProfileFee.html', **return_context)
#     else:
#         return_context['notificationMsg'] = notificationMsg
#         return render_template('DSProfileFee.html', **return_context)

# @DSProfile_blueprint.route('/ds-edit-order', methods = ['GET','POST'])
# def dsEditOrder():
#     id_ds = session.get('idDS')

#     if request.method == 'POST':
#         if 'submitTelNum' in request.form:
#             input_tel_sender = request.form['inputTelSender']
#             input_tel_recv = request.form['inputTelRecv']

#             execute_database = dsExecuteDatabase.getIDAddrMatchTel(input_tel_sender, input_tel_recv)
            
#             return redirect(url_for('.dsEditOrder', lst_id_order = execute_database))
#         elif 'submitOrderNumber' in request.form:
#             input_order_number = request.form['inputOrderNumber']
#             execute_database = dsExecuteDatabase.checkOrderNumberExits(input_order_number)

#             if execute_database == 'Not':
#                 return redirect(url_for('.dsEditOrder', msg_notification = f'Not found any order match order number {input_order_number}'))
#             else:
#                 return redirect(url_for('.dsEditOrder', msg_notification = f'OK', input_order_number = input_order_number))

#         elif 'submitEditOrder' in request.form:
#             code_order_number = request.form['valueCodeOrderNumber']
#             input_name_sender = request.form['editNameSender']
#             input_name_receiver = request.form['editNameReceiver']
#             input_tel_sender = request.form['editTelSender']
#             input_tel_receiver = request.form['editTelReceiver']

#             execute_database = dsExecuteDatabase.updateEditOrder(code_order_number, input_name_sender, input_name_receiver,input_tel_sender, input_tel_receiver)
            
#             if execute_database == 'OK':
#                 return 'Update Successful'
#             else:
#                 return 'Have some problem when update, please update again or contact admin for more support'

#     msg_notification = request.args.get('msg_notification', 'None')
#     lst_id_order = request.args.getlist('lst_id_order')

#     if msg_notification == 'OK':
#         order_number_need_edit = request.args.get('input_order_number')
#         return render_template('DSEditOrder.html', edit_order = True, order_number_need_edit = order_number_need_edit)

#     if lst_id_order == []:
#         return render_template('DSEditOrder.html', msg_notification = msg_notification) 
#     else:
#         all_order = dsExecuteDatabase.getAllDetailOrder(lst_id_order)
#         return render_template('DSEditOrder.html', all_order = all_order, msg_notification = msg_notification) 

# @DSProfile_blueprint.route('/ds-show-all-order-by-tel/<int:CurrentPage>', methods = ['GET','POST'])
# def dsShowAllOrderByTel(CurrentPage = 1):
#     id_ds = session.get('idDS')

#     get_all_order = request.args.getlist('all_order')
#     all_order = [eval(row) for row in get_all_order]

#     calcPagination = paginationPageDS.calPagination(CurrentPage, limit_show_order, len(all_order))
#     all_order = all_order[calcPagination[1] : calcPagination[2]]

#     all_inform = giveInformation.giveInformationAboutOrder(all_order)

#     return render_template('dsShowAllOrderByTel.html', all_order = all_order, paginationPage = calcPagination[0],
#         lstSender = all_inform[0], lstReceiver = all_inform[1], lstProduct = all_inform[2], CurrentPage = CurrentPage)

def get_static_ds_driver(request, dateSearch, idDriver, CurrentPage, driver_name ):
    if request.method == 'POST':
        form_data = request.POST.get('chooseKindTime')

        if 'all' in form_data:
            id_driver = request.POST.get('chooseDriverStatic').split(',')[0]
            driver_name = request.POST.get('chooseDriverStatic').split(',')[1]
            return redirect('DS_app:DS_static_order_by_driver', CurrentPage = 1, dateSearch = 'all', idDriver = id_driver, driver_name = driver_name)
        elif 'date' in form_data:
            id_driver = request.POST.get('chooseDriverStatic').split(',')[0]
            date_input = request.POST.get('chooseTimeDate')
            driver_name = request.POST.get('chooseDriverStatic').split(',')[1]
            return redirect('DS_app:DS_static_order_by_driver', CurrentPage = 1, dateSearch = date_input, idDriver = id_driver, driver_name = driver_name)

    idDS = request.session.get('id_role_user_ddelivery', None)

    get_all_driver = exec_data.get_all_driver_ds(idDS)
    # driver_name = request.GET.get('driver_name')

    if idDriver is None and dateSearch is None:
        return render(request, 'DS/static_ds_driver.html', {'get_all_driver' : get_all_driver})
    else:
        if dateSearch == 'all':
            execute_database = exec_data.get_order_by_driver(idDS, idDriver, 'all', CurrentPage)
        elif dateSearch != '':
            dateSearchTemp = datetime.datetime.strptime(dateSearch, f'%Y-%m-%d').date()
            execute_database = exec_data.get_order_by_driver(idDS, idDriver, dateSearchTemp, CurrentPage)

        allOrderByDriver = execute_database[1]
        paginationPage = execute_database[0]
        len_all_order = execute_database[2]
        all_inform = get_inform_order.get_information(allOrderByDriver)

        # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(informOrder[0])
        # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(informOrder[1])
        # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrderByDriver)

        #'dictCountryShippingAddr' : dictCountryShippingAddr, 'dictCountryDeliveryAddr' : dictCountryDeliveryAddr,'sumMoneyOrder' : sumMoneyOrder,
        return_para = {'allOrderByDriver' : allOrderByDriver, 'idDriver' : idDriver, 'paginationPage' : paginationPage,
            'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstType' : all_inform[2], 'dateSearch' : dateSearch,
            'CurrentPage' : CurrentPage,  'get_all_driver' : get_all_driver, 'len_all_order' : len_all_order}
        
        return_para['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

        if driver_name is not None:
            return_para['driver_name'] = driver_name
        
        return render(request, 'DS/static_ds_driver.html', return_para)

def get_static_ds_date(request, CurrentPage = 1, dateValue = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)
    if dateValue == 'None':
        return HttpResponse('<p>Not choose Date, back choose Date</p>')

    chooseDate = datetime.datetime.strptime(dateValue, f'%Y-%m-%d').date()

    execute_database = exec_data.getStaticDSDateTime(idDS, CurrentPage, chooseDate)
    paginationPage = execute_database['pagination']
    allOrder = execute_database['all_order']
    len_all_order = execute_database['len_all_order']

    informOrder = get_inform_order.get_information(allOrder)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(informOrder[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(informOrder[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrder)

    # dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, sumMoneyOrder = sumMoneyOrder
    context_return = {'allOrder' : allOrder, 'lstSender' : informOrder[0], 'lstReceiver' : informOrder[1], 'lstType' : informOrder[2], 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage , 'dateValue' : dateValue, 'len_all_order' : len_all_order}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

    return render(request, 'DS/static_ds_filter.html', context_return)

def get_static_month(request, CurrentPage = 1, monthValue = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)
    if monthValue == "None":
        return HttpResponse('<p>Not choose Month, back choose month</p>')

    execute_database = exec_data.getStaticDSMonthTime(idDS, CurrentPage, monthValue)
    paginationPage = execute_database['pagination']
    allOrder = execute_database['all_order']
    len_all_order = execute_database['len_all_order']
    
    informOrder = get_inform_order.get_information(allOrder)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(informOrder[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(informOrder[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrder)

    # dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, sumMoneyOrder = sumMoneyOrder
    context_return = {'allOrder' : allOrder, 'lstSender' : informOrder[0], 'lstReceiver' : informOrder[1], 'lstType' : informOrder[2], 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage, 'monthValue' : monthValue, 'len_all_order' : len_all_order}
    
    return render(request, 'DS/static_ds_filter.html', context_return)

def get_static_year(request, CurrentPage = 1, yearValue = 'None'):
    idDS = request.session.get('id_role_user_ddelivery', None)
    
    if yearValue == "None":
        return HttpResponse('<p>Not choose Year, back choose year</p>')

    execute_database = exec_data.getStaticDSYearTime(idDS, CurrentPage, yearValue)
    paginationPage = execute_database['pagination']
    allOrder = execute_database['all_order']
    len_all_order = execute_database['len_all_order']
    
    informOrder = get_inform_order.get_information(allOrder)

    # dictCountryShippingAddr = countTimeExits.countTimeExitsShippingAddr(informOrder[0])
    # dictCountryDeliveryAddr = countTimeExits.countTimeExitsDeliveryAddr(informOrder[1])
    # sumMoneyOrder = countTimeExits.sumMoneyAllOrder(allOrder)

    # TODO code chart here
    # allOrderEachMonth =[0]*12
    # for order in allOrder:
    #     monthEachOther = order[12].month
    #     allOrderEachMonth[monthEachOther-1] += 1

    # imageChart = drawChart.drawBarChartStaticDSYear(allOrderEachMonth, yearValue)

    # imageChart = imageChart, dictCountryShippingAddr = dictCountryShippingAddr, dictCountryDeliveryAddr = dictCountryDeliveryAddr, sumMoneyOrder = sumMoneyOrder
    context_return = {'allOrder' : allOrder, 'lstSender' : informOrder[0], 'lstReceiver' : informOrder[1], 'lstType' : informOrder[2], 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage, 'yearValue' : yearValue, 'len_all_order' : len_all_order}
    
    return render(request, 'DS/static_ds_filter.html', context_return)

def total_money_one_month(request, monthValue = 'None', CurrentPage = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.getTotalMoneyMonth(monthValue, id_ds, CurrentPage, limit_show_order)
    total_money = execute_database[0]
    all_order = execute_database[1]
    all_driver = execute_database[2]
    paginationPage = execute_database[3]
    
    locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')
    total_money = locale.format_string("%d", total_money, grouping=True)
    
    allInform = get_inform_order.get_information(all_order)

    context_return = {'total_money' : total_money, 'monthValue' : monthValue, 'all_order' : all_order, 'lstDriver' : all_driver, 'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage, 'CurrentPage' : CurrentPage}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
    
    return render(request, 'DS/total_money_static.html', context_return)

def total_money_one_day(request, dayValue = 'None', CurrentPage = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.getTotalMoneyDay(dayValue, id_ds, CurrentPage, limit_show_order)
    total_money = execute_database[0]
    all_order = execute_database[1]
    all_driver = execute_database[2]
    paginationPage = execute_database[3]
    
    locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')
    total_money = locale.format_string("%d", total_money, grouping=True)

    allInform = get_inform_order.get_information(all_order)

    context_return = {'total_money' : total_money, 'total_money' : dayValue, 'all_order' : all_order, 'lstDriver' : all_driver, 'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage, 'CurrentPage' : CurrentPage}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]
    
    return render(request, 'DS/total_money_static.html', context_return)

def total_money_one_year(request, yearValue = 'None', CurrentPage = 1):
    id_ds = request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.getTotalMoneyYear(yearValue, id_ds, CurrentPage)
    total_money = execute_database[0]
    all_order = execute_database[1]
    all_driver = execute_database[2]
    paginationPage = execute_database[3]
    
    locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')
    total_money = locale.format_string("%d", total_money, grouping=True)

    allInform = get_inform_order.get_information(all_order)

    context_return = {'total_money' : total_money, 'yearValue' : yearValue, 'all_order' : all_order, 'lstDriver' : all_driver, 'lstSender' : allInform[0], 'lstReceiver' : allInform[1], 'lstProduct' : allInform[2], 'paginationPage' : paginationPage, 'CurrentPage' : CurrentPage}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

    return render(request, 'DS/total_money_static.html', context_return)