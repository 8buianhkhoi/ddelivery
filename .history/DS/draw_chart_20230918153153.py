# from sqlalchemy import and_
# import matplotlib.pyplot as plt
# import matplotlib
# matplotlib.use('Agg')
# import io
# import base64 
# import datetime 

# from models.models import *
# from . import dsExecuteDatabase

# def drawBarChartHomepage(idDS):
#     conn = engine.connect()

#     execute_database = dsExecuteDatabase.drawChartDrawBarChartHomepage(idDS)

#     lstX = ['Success', 'In Pending', 'Need Accept Driver', 'In Delivery']
#     lstY = [execute_database[0], execute_database[1], execute_database[2], execute_database[3]]

#     buffer = io.BytesIO()
#     plt.figure()

#     plt.bar(lstX, lstY)
#     for index in range(0, len(lstX)):
#         plt.text(lstX[index], lstY[index], str(execute_database[index]) + ' order', ha='center', va='bottom', color='black')

#     plt.xlabel('Order Status')
#     plt.ylabel('Number Order')
#     plt.title('All Order static by status')
#     plt.legend()

#     plt.savefig(buffer, format = 'png')
#     buffer.seek(0)

#     imageChart = base64.b64encode(buffer.getvalue()).decode()
#     plt.close()
    
#     return imageChart

# def drawPieChartStaticDS(idDS):
#     execute_database = dsExecuteDatabase.getDrawPieChartStaticDS(idDS)

#     lstPercent = [execute_database[0], execute_database[1], execute_database[2], execute_database[3]]
#     lstLabel = ['Order Pending', 'Order Need Delivery', 'Order Delivery', 'Order Success']

#     buffer = io.BytesIO()
#     plt.figure()

#     plt.pie(lstPercent, shadow = True, pctdistance = 0.5, labeldistance = 1.1, explode = [0.1, 0.1, 0.1, 0.1], 
#         autopct = f'%0.0f%%', labels = lstLabel)
#     plt.legend(bbox_to_anchor=(1.2, 1))

#     plt.savefig(buffer, format = 'png', bbox_inches='tight')
#     buffer.seek(0)

#     pieChart = base64.b64encode(buffer.getvalue()).decode()
#     plt.close()

#     return pieChart

# def drawBarChartStaticDSYear(allOrderEachMonth, yearValue):
#     allMonth = [1,2,3,4,5,6,7,8,9,10,11,12]

#     buffer = io.BytesIO()
#     plt.figure()

#     plt.xlabel('Month')
#     plt.ylabel('All order')
#     plt.title(f'All Order Success Each Month in {yearValue}')

#     plt.bar(allMonth, allOrderEachMonth)
#     plt.legend()

#     plt.savefig(buffer, format = 'png')
#     buffer.seek(0)

#     imageChart = base64.b64encode(buffer.getvalue()).decode()
#     plt.close()

#     return imageChart

# def drawTotalMoneyChart(idDS):
#     CurrentPage = 1 
#     limitShowOrder = 30

#     buffer = io.BytesIO()
#     plt.figure()

#     current_time = datetime.datetime.now()

#     total_money_today = dsExecuteDatabase.getTotalMoneyDay(current_time.strftime(f'%Y-%m-%d'), idDS, CurrentPage = CurrentPage, limitShowOrder = limitShowOrder)[0]
#     total_money_month = dsExecuteDatabase.getTotalMoneyMonth(current_time.strftime(f'%Y-%m'), idDS, CurrentPage = CurrentPage, limitShowOrder = limitShowOrder)[0]
#     total_money_year = dsExecuteDatabase.getTotalMoneyYear(current_time.strftime(f'%Y'), idDS, CurrentPage = CurrentPage, limitShowOrder = limitShowOrder)[0]

#     lst_total_money = [total_money_today, total_money_month, total_money_year]
#     plt.barh(["day", "month", "year"], lst_total_money)
    
#     for index in range(0,3):
#         plt.text(index, index, str(lst_total_money[index]), va='center', ha ='left')

#     plt.xlabel('Total Money')
#     plt.ylabel('Time')
#     plt.title('Total Money')
#     plt.legend()

#     plt.savefig(buffer, format = 'png')
#     buffer.seek(0)

#     imageChart = base64.b64encode(buffer.getvalue()).decode()
#     plt.close()

#     return imageChart

# def drawChartOrderCreateEachMonth(idDS):
#     year_value = datetime.datetime.now().strftime("%Y")

#     execute_database = dsExecuteDatabase.getOrderAllMonth(idDS, year_value)
#     lst_order_status = ['Create', 'Pending', 'Need Delivery', 'Delivery', 'Success']
#     lst_str_month = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']

#     buffer = io.BytesIO()
#     plt.figure()

#     for index in range(0,5):
#         plt.plot(lst_str_month, execute_database[index], label= lst_order_status[index])
    
#     plt.xlabel('Month')
#     plt.title('Order Each Month')
#     plt.legend()
#     plt.tight_layout()
    
#     plt.savefig(buffer, format = 'png')
#     buffer.seek(0)

#     imageChart = base64.b64encode(buffer.getvalue()).decode()
#     plt.close()

#     return imageChart

# def drawChartOrderEachDriver(idDS):
#     execute_database = dsExecuteDatabase.getEachDriverOrder(idDS)
#     lstX = []
#     lstY = []

#     for index in execute_database:
#         lstX.append(index)
#         lstY.append(execute_database[index])
    
#     buffer = io.BytesIO()
#     plt.figure()

#     plt.bar(lstX, lstY)
#     plt.xlabel('Name')
#     plt.ylabel('Order')
#     plt.title('Total order of each driver')
#     plt.tight_layout()

#     plt.savefig(buffer, format = 'png')
#     buffer.seek(0)

#     imageChart = base64.b64encode(buffer.getvalue()).decode()

#     plt.close()

#     return imageChart
