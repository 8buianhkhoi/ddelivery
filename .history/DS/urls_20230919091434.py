from django.urls import path

from . import views 

app_name = 'DS_app'

urlpatterns = [
    path('homepage/', views.base_DS_page, name = 'DS_homepage')
]