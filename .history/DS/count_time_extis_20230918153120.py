def countTimeExitsShippingAddr(lstShippingAddr):
    dictCountryShippingAddr = {}
    for index in lstShippingAddr:
        if index[4] not in dictCountryShippingAddr:
            dictCountryShippingAddr[index[4]] = 1
        else:
            dictCountryShippingAddr[index[4]] = dictCountryShippingAddr[index[4]] + 1
    return dictCountryShippingAddr

def countTimeExitsDeliveryAddr(lstDeliveryAddr):
    dictCountryDeliveryAddr = {}
    for index in lstDeliveryAddr:
        if index[4] not in dictCountryDeliveryAddr:
            dictCountryDeliveryAddr[index[4]] = 1
        else:
            dictCountryDeliveryAddr[index[4]] = dictCountryDeliveryAddr[index[4]] + 1
    return dictCountryDeliveryAddr

def sumMoneyAllOrder(lstOrder):
    sumMoneyOrder = 0
    for order in lstOrder:
        sumMoneyOrder = sumMoneyOrder + int(order[8])
    return sumMoneyOrder

def countTimeExitsDriver(lstDriver):
    dictDriverTimes = {}
    for index in lstDriver:
        if index[4] not in dictDriverTimes:
            dictDriverTimes[index[4]] = 1
        else:
            dictDriverTimes[index[4]] = dictDriverTimes[index[4]] + 1
    return dictDriverTimes