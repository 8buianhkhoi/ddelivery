from django.urls import path

from . import views 

app_name = 'DS_app'

urlpatterns = [
    path('homepage/', views.base_DS_page, name = 'DS_homepage'),
    path('order-in-delivery/<int:current_page>/', views.order_delivery, name = 'DS_order_in_delivery')
]