from django.shortcuts import render, redirect
from dotenv import load_dotenv
import os 
import datetime

from . import exec_data
from . import get_inform_order

# @orderSuccessFilter_blueprint.route('/by-date/<int:CurrentPage>/<dateValue>')
# def orderSuccessFilterDate(CurrentPage = 1, dateValue = 'None'):
#     id_driver = session['idDriver']
#     date_choose = datetime.datetime.strptime(dateValue, f'%Y-%m-%d')

#     execute_database = driverExecuteDatabase.driverOrderSuccessFilterDate(id_driver, date_choose, CurrentPage, limit_show)
#     lst_inform = giveInform.giveInformationAboutOrder(execute_database[1])

#     return render_template('orderSuccessFilter.html', allOrderSuccess = execute_database[1], lstSender = lst_inform[0], dateValue = dateValue,
#         lstReceiver = lst_inform[1], lstProduct = lst_inform[2], CurrentPage = CurrentPage, paginationPage = execute_database[0], len_all_order = execute_database[2])

# @orderSuccessFilter_blueprint.route('/by-month/<int:CurrentPage>/<monthValue>')
# def orderSuccessFilterMonth(CurrentPage = 1, monthValue = 'None'):
#     id_driver = session['idDriver']

#     execute_database = driverExecuteDatabase.driverOrderSuccessFilterMonth(id_driver, monthValue, CurrentPage, limit_show)
#     lst_inform = giveInform.giveInformationAboutOrder(execute_database[1])

#     return render_template('orderSuccessFilter.html', allOrderSuccess = execute_database[1], lstSender = lst_inform[0], monthValue = monthValue,
#         lstReceiver = lst_inform[1], lstProduct = lst_inform[2], CurrentPage = CurrentPage, paginationPage = execute_database[0], len_all_order = execute_database[2])

# @orderSuccessFilter_blueprint.route('/by-year/<int:CurrentPage>/<int:yearValue>')
# def orderSuccessFilterYear(CurrentPage = 1, yearValue = 'None'):
#     id_driver = session['idDriver']

#     execute_database = driverExecuteDatabase.driverOrderSuccessFilterYear(id_driver, yearValue, CurrentPage, limit_show)
#     lst_inform = giveInform.giveInformationAboutOrder(execute_database[1])

#     return render_template('orderSuccessFilter.html', allOrderSuccess = execute_database[1], lstSender = lst_inform[0], yearValue = yearValue,
#         lstReceiver = lst_inform[1],lstProduct = lst_inform[2], CurrentPage = CurrentPage, paginationPage = execute_database[0], len_all_order = execute_database[2])