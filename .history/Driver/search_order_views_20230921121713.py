from django.shortcuts import render, redirect
from dotenv import load_dotenv
import os 

from . import exec_data
from . import get_inform_order

# TODO

# @driverSearchOrder_blueprint.route('/search-by-telephone-number/<int:CurrentPage>/<telNum>')
# def searchOrderByTel(CurrentPage = 1, telNum = 'None'):
#     id_driver = session['idDriver']

#     if telNum == 'None':
#         return '<p>Have some problem</p>'
    
#     execute_database = driverExecuteDatabase.driverSearchOrderTel(id_driver, telNum, CurrentPage, limitShowOrder)
#     lstOrder = execute_database[1]
#     paginationPage = execute_database[0]
#     lstInform = giveInform.giveInformationAboutOrder(lstOrder)

#     return render_template('searchOrderByFilter.html', lstOrder = lstOrder, lstSender = lstInform[0], lstReceiver = lstInform[1], 
#         lstProduct = lstInform[2], CurrentPage = CurrentPage, paginationPage = paginationPage, telNum = telNum)   

# @driverSearchOrder_blueprint.route('/search-order/search-by-day/<int:CurrentPage>/<chooseDate>')
# def searchOrderByDay(CurrentPage = 1, chooseDate = 'None'):
#     id_driver = session['idDriver']

#     if chooseDate == 'None':
#         return '<p>Have some problem</p>'

#     execute_database = driverExecuteDatabase.driverSearchOrderDay(id_driver, chooseDate, CurrentPage, limitShowOrder)
#     lstOrder = execute_database[1]
#     paginationPage = execute_database[0]

#     lstInform = giveInform.giveInformationAboutOrder(lstOrder)

#     return render_template('searchOrderByFilter.html', lstOrder = lstOrder, lstSender = lstInform[0], lstReceiver = lstInform[1], 
#         lstProduct = lstInform[2], dayValue = chooseDate, CurrentPage = CurrentPage, paginationPage = paginationPage)

# @driverSearchOrder_blueprint.route('/search-order/search-by-month/<int:CurrentPage>/<chooseMonth>')
# def searchOrderByMonth(CurrentPage = 1, chooseMonth = 'None'):
#     id_driver = session['idDriver']

#     if chooseMonth == 'None':
#         return '<p>Have some problem</p>'

#     execute_database = driverExecuteDatabase.driverSearchOrderMonth(id_driver, chooseMonth, CurrentPage, limitShowOrder)
#     lstOrder = execute_database[1]
#     paginationPage = execute_database[0]

#     lstInform = giveInform.giveInformationAboutOrder(lstOrder)

#     return render_template('searchOrderByFilter.html', lstOrder = lstOrder, lstSender = lstInform[0], lstReceiver = lstInform[1], 
#         lstProduct = lstInform[2], monthValue= chooseMonth, CurrentPage = CurrentPage, paginationPage = paginationPage)

# @driverSearchOrder_blueprint.route('/search-order/search-by-year/<int:CurrentPage>/<int:chooseYear>')
# def searchOrderByYear(CurrentPage = 1, chooseYear = 'None'):
#     id_driver = session['idDriver']

#     if chooseYear == 'None':
#         return '<p>Have some problem</p>'
        
#     execute_database = driverExecuteDatabase.driverSearchOrderYear(id_driver, chooseYear, CurrentPage, limitShowOrder)
#     lstOrder = execute_database[1]
#     paginationPage = execute_database[0]
    
#     lstInform = giveInform.giveInformationAboutOrder(lstOrder)

#     return render_template('searchOrderByFilter.html', lstOrder = lstOrder, lstSender = lstInform[0], lstReceiver = lstInform[1], 
#         lstProduct = lstInform[2], yearValue = chooseYear, CurrentPage = CurrentPage, paginationPage = paginationPage)
    
# @driverSearchOrder_blueprint.route('/search-order/search-all/<int:CurrentPage>', methods = ['GET', 'POST'])
# def driverSearchAllOrder(CurrentPage = 1):
#     if request.method == 'POST':
#         form_data = request.form['chooseTypeSearch']
#         if 'all' in form_data:
#             return redirect(url_for('.driverSearchAllOrder', CurrentPage = 1))
#         elif 'tel' in form_data:
#             tel_number = request.form['inputTel']
#             return redirect(url_for('driverSearchOrderBp.searchOrderByTel', CurrentPage = 1, telNum = tel_number))
#         elif 'day' in form_data:
#             day_value = request.form['inputDate']
#             return redirect(url_for('driverSearchOrderBp.searchOrderByDay', CurrentPage = 1, chooseDate = day_value))
#         elif 'month' in form_data:
#             month_value = request.form['inputMonth']
#             return redirect(url_for('driverSearchOrderBp.searchOrderByMonth', CurrentPage = 1, chooseMonth = month_value))
#         elif 'year' in form_data:
#             year_value = request.form['inputYear']
#             return redirect(url_for('driverSearchOrderBp.searchOrderByYear', CurrentPage = 1, chooseYear = year_value))

#     id_driver = session['idDriver']

#     execute_database = driverExecuteDatabase.getAllOrderByDriver(id_driver, CurrentPage, limitShowOrder)
#     paginationPage = execute_database['pagination']
#     all_order = execute_database['all_order']
#     len_all_order = execute_database['len_all_order']

#     lstInform = giveInform.giveInformationAboutOrder(all_order)

#     return_para = {'lstSender':lstInform[0], 'lstReceiver' : lstInform[1], 'lstProduct' : lstInform[2], 'paginationPage' : paginationPage,
#         'CurrentPage' : CurrentPage, 'all_order' : all_order, 'len_all_order' : len_all_order}

#     return render_template('driverSearchAllOrder.html', **return_para)