import datetime

from . import pagination_page

limit_show_order = 30

def driverOrderSuccessFilterDate(idDriver, dateChoose, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Success', real_time_arrive__date = dateChoose.date())
        len_order_date = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_order_date)

        order_success_date = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        return [calc_pagination[0], order_success_date, len_order_date]
    except:
        return False

def driverOrderSuccessFilterMonth(idDriver, monthValue, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Success', real_time_arrive__year = int(monthValue[:4]), real_time_arrive__month = int(monthValue[5:]))

        len_order_month = queryOrder.count()
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_order_month)

        order_success_month = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        return [calc_pagination[0], order_success_month, len_order_month]

def driverOrderSuccessFilterYear(idDriver, yearValue, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Success', real_time_arrive__year = int(yearValue))

        len_order_year = queryOrder.count()
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_order_year)

        order_success_year = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        return [calc_pagination[0], order_success_year, len_order_year]
    except:
        return False

def driverSearchOrderTel(idDriver, telNum, CurrentPage):
    try:
        lstIdShippingAddr = []
        lstIdDeliveryAddr = []
        lstOrder = []
        setOrder = set()

        allOrderByDriver = Detail_Order_tb.objects.filter(id_driver = idDriver).values()

        for order in allOrderByDriver:
            idShippingAddr = order['id_shipping_addr_id']
            idDeliveryAddr = order['id_delivery_addr_id']

            # TODO check code here again
            getShippingAddr = Shipping_Addr_tb.objects.filter(id = idShippingAddr).values()[0]['tel_num']
            getDeliveryAddr = Delivery_Addr_tb.objects.filter(id = idDeliveryAddr).values()[0]['tel_num']

            if getShippingAddr == telNum:
                lstIdShippingAddr.append(idShippingAddr)
            if getDeliveryAddr == telNum:
                lstIdDeliveryAddr.append(idDeliveryAddr)
        for index in lstIdShippingAddr:
            idDetailOrder = Detail_Order_tb.objects.filter(id_shipping_addr = index).values()[0]['id']
            setOrder.add(idDetailOrder)
        for index in lstIdDeliveryAddr:
            idDetailOrder = Detail_Order_tb.objects.filter(id_delivery_addr = index).values()[0]['id']
            setOrder.add(idDetailOrder)
        for index in setOrder:
            getOrder = Detail_Order_tb.objects.filter(id = index).values()
            lstOrder.append(getOrder)
        
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len(lstOrder))
        lstOrder = lstOrder[calc_pagination[1] : calc_pagination[2] + 1]
    
        return [calc_pagination[0], lstOrder]
    except:
        return False

def driverSearchOrderDay(idDriver, chooseDate, CurrentPage):
    try:
        dateChoose = datetime.datetime.strptime(chooseDate, f'%Y-%m-%d')
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, date_create__date = dateChoose.date())

        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], lstOrder]
    except:
        return False

def driverSearchOrderMonth(idDriver, chooseMonth, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, date_create__year = int(chooseMonth[:4]), date_create__month = int(chooseMonth[5:]))

        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order]
        return [calc_pagination[0], lstOrder]
    except:
        return False

def driverSearchOrderYear(idDriver, chooseYear, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, date_create__year = int(chooseYear))
        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], lstOrder]
    except:
        return False

def driverOrderSuccess(idDriver, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Success')
        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], lstOrder, len_lst_order]
    except:
        return False

def driverOrderDelivery(idDriver, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver =  idDriver, status_order = 'Delivery')
        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], lstOrder, len_lst_order]

def driverPostOrderDelivery(getCodeOrder):
    try:
        dateTimeNow = datetime.datetime.now()
        updateRealTimeArrive = Detail_Order_tb.objects.filter(code_order_delivery = getCodeOrder).update(real_time_arrive = dateTimeNow)
        queryUpdateSuccess = Detail_Order_tb.objects.filter(code_order_delivery = getCodeOrder).update(status_order = 'Success')

def driverOrderNeedDelivery(idDriver, CurrentPage):
    try:
        nameDriver = Driver_tb.objects.filter(id = idDriver).values()[0]['driver_name']

        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Need-Delivery')

        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values 

        return [calc_pagination[0], lstOrder, nameDriver, len_lst_order]
    except:
        return False

def driverPostOrderNeedDelivery(getCodeOrder):
    with engine.connect() as conn:
        queryUpdateTransmit = detailOrder.update().where(detailOrder.c.codeOrder == getCodeOrder).values(status='Delivery')
        conn.execute(queryUpdateTransmit)
        
        executeDatabaseOS(conn)

def driverUpdateStatusDelivery(idDriver):
    with engine.connect() as conn:
        queryUpdateStatus = detailOrder.update().where(and_(detailOrder.c.idDriver == idDriver, detailOrder.c.status == 'Need-Delivery')).values(status='Delivery')
        conn.execute(queryUpdateStatus)

        executeDatabaseOS(conn)

def driverBasePage(idDriver):
    with engine.connect() as conn:
        queryDriver = driver.select().where(driver.c.idDriver == idDriver)
        queryAllOrderNeedDelivery = detailOrder.select().where(and_(detailOrder.c.idDriver == idDriver, detailOrder.c.status == 'Need-Delivery'))
        driverName = conn.execute(queryDriver).fetchone()[4]
        len_all_order = conn.execute(queryAllOrderNeedDelivery).rowcount

    return [driverName, len_all_order]

def executeGetInformDriver(idDriver):
    with engine.connect() as conn:
        driverInform = conn.execute(select(driver).where(driver.c.idDriver == idDriver)).fetchall()

    return driverInform

def updateBasicInformDriver(idDriver, nameDriver, telDriver, gmailDriver, genderDriver, citizenIdentifyNumber):
    with engine.connect() as conn:
        if nameDriver != '':
            queryUpdateNameDriver = driver.update().where(driver.c.idDriver == idDriver).values(name = nameDriver)
            conn.execute(queryUpdateNameDriver)
        if telDriver != '':
            queryUpdateTelDriver = driver.update().where(driver.c.idDriver == idDriver).values(telNum = telDriver)
            conn.execute(queryUpdateTelDriver)
        if gmailDriver != '':
            queryUpdateGmailDriver = driver.update().where(driver.c.idDriver == idDriver).values(gmail = gmailDriver)
            conn.execute(queryUpdateGmailDriver)
        if genderDriver != '':
            queryUpdateGenderDriver = driver.update().where(driver.c.idDriver == idDriver).values(gender = genderDriver)
            conn.execute(queryUpdateGenderDriver)
        if citizenIdentifyNumber != '':
            queryUpdateCitizenIdentifyNumber = driver.update().where(driver.c.idDriver == idDriver).values(citizenIdentifyNumber = citizenIdentifyNumber)
            conn.execute(queryUpdateCitizenIdentifyNumber)
        executeDatabaseOS(conn)

    return 'OK'

def updateAdvancedInformDriver(idUser, provinceUser, districtUser, wardUser):
    with engine.connect() as conn:
        update_address = users.update().where(users.c.idUser == idUser).values(province = provinceUser, district = districtUser, ward = wardUser)
        conn.execute(update_address)

        executeDatabaseOS(conn)

    return 'OK'

def hashPassword(password, salt=None, iterations= 100000, key_length =32):
    key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, iterations, dklen=key_length)
    
    return salt + key 

def updatePasswordDriver(idUser, passwordUser, secretKey):
    with engine.connect() as conn:
        hash_bytes = hashPassword(passwordUser, secretKey.encode())
        hash_password = base64.b64encode(hash_bytes).decode('utf-8')

        update_pass = users.update().where(users.c.idUser == idUser).values(passWord = hash_password)
        conn.execute(update_pass)

        executeDatabaseOS(conn)

    return 'OK'

def updateDriverLicense(idDriver, fullName, beginDate, classMotor):
    with engine.connect() as conn:
        update_driver_license = driverLicense.update().where(driverLicense.c.idDriver == idDriver).values(fullName = fullName, beginDate = beginDate, classLicense = classMotor)
        conn.execute(update_driver_license)

        executeDatabaseOS(conn)
    
    return 'OK'

def updateDriverRegCert(idDriver, numberCert, palateCert, branchCert, colorCert, capacityMotorCert, firstRegCert):
    with engine.connect() as conn:
        update_driver_reg_cert = motorbikeRegCert.update().where(motorbikeRegCert.c.idDriver == idDriver).values(numberReg = numberCert, plate = palateCert, branch = branchCert, color = colorCert, capacity = capacityMotorCert, firstReg = firstRegCert)
        conn.execute(update_driver_reg_cert)

        executeDatabaseOS(conn)

    return 'OK'

def allOrderNeedDeliveryAutoUpdateRoute(idDriver):
    with engine.connect() as conn:
        query_order = detailOrder.select().where(and_(detailOrder.c.idDriver == idDriver, detailOrder.c.status == 'Need-Delivery'))
        all_order_need_delivery = conn.execute(query_order).fetchall()
    
    return all_order_need_delivery

def getDeliveryAddrAutoUpdateRoute(idDATemp):
    with engine.connect() as conn:
        query_order = deliveryAddr.select().where(deliveryAddr.c.idDeliveryAddr == idDATemp)
        deliveryAddr_temp = conn.execute(query_order).fetchone()
    
    return deliveryAddr_temp

def getRevenueDay(id_driver, dayValue, CurrentPage, limitShowOrder):
    with engine.connect() as conn:
        query_order_revenue = select(func.sum(detailOrder.c.totalCost.cast(Integer))).where(and_(detailOrder.c.idDriver == id_driver, detailOrder.c.status == 'Success', cast(detailOrder.c.realTimeArrive, Date) == dayValue.date()))
        query_all_order = select(detailOrder).where(and_(detailOrder.c.idDriver == id_driver, detailOrder.c.status == 'Success', cast(detailOrder.c.realTimeArrive, Date) == dayValue.date()))
        
        execute_revenue = conn.execute(query_order_revenue).scalar()
        len_all_order = conn.execute(query_all_order).rowcount 
        
        calc_pagination = calcPagination.paginationPage(CurrentPage, limitShowOrder, len_all_order)

        all_order = conn.execute(query_all_order.offset(calc_pagination[1]).limit(limitShowOrder).order_by(detailOrder.c.realTimeArrive.desc())).fetchall()

        if execute_revenue is None:
            total_money = 0
        else:
            total_money = int(execute_revenue)
    return [calc_pagination[0], total_money, all_order]

def getRevenueMonth(id_driver, monthValue, CurrentPage, limitShowOrder):
    with engine.connect() as conn:
        query_order_revenue = select(func.sum(detailOrder.c.totalCost.cast(Integer))).where(and_(detailOrder.c.idDriver == id_driver, detailOrder.c.status == 'Success', extract('year', detailOrder.c.realTimeArrive) == int(monthValue[:4]), extract('month', detailOrder.c.realTimeArrive) == int(monthValue[5:])))
        query_all_order = select(detailOrder).where(and_(detailOrder.c.idDriver == id_driver, detailOrder.c.status == 'Success', extract('year', detailOrder.c.realTimeArrive) == int(monthValue[:4]), extract('month', detailOrder.c.realTimeArrive) == int(monthValue[5:])))
        
        execute_revenue = conn.execute(query_order_revenue).scalar()
        len_all_order = conn.execute(query_all_order).rowcount 
        
        calc_pagination = calcPagination.paginationPage(CurrentPage, limitShowOrder, len_all_order)

        all_order = conn.execute(query_all_order.offset(calc_pagination[1]).limit(limitShowOrder).order_by(detailOrder.c.realTimeArrive.desc())).fetchall()

        if execute_revenue is None:
            total_money = 0
        else:
            total_money = int(execute_revenue)
    return [calc_pagination[0], total_money, all_order]

def getRevenueYear(id_driver, yearValue, CurrentPage, limitShowOrder):
    with engine.connect() as conn:
        query_order_revenue = select(func.sum(detailOrder.c.totalCost.cast(Integer))).where(and_(detailOrder.c.idDriver == id_driver, detailOrder.c.status == 'Success', extract('year', detailOrder.c.realTimeArrive) == int(yearValue)))
        query_all_order = select(detailOrder).where(and_(detailOrder.c.idDriver == id_driver, detailOrder.c.status == 'Success', extract('year', detailOrder.c.realTimeArrive) == int(yearValue)))
        
        execute_revenue = conn.execute(query_order_revenue).scalar()
        len_all_order = conn.execute(query_all_order).rowcount 
        
        calc_pagination = calcPagination.paginationPage(CurrentPage, limitShowOrder, len_all_order)

        all_order = conn.execute(query_all_order.offset(calc_pagination[1]).limit(limitShowOrder).order_by(detailOrder.c.realTimeArrive.desc())).fetchall()

        if execute_revenue is None:
            total_money = 0
        else:
            total_money = int(execute_revenue)
    return [calc_pagination[0], total_money, all_order]

def getAllOrderByDriver(id_driver, CurrentPage, limitShowOrder):
    with engine.connect() as conn:
        query_order = detailOrder.select().where(detailOrder.c.idDriver == id_driver)
        len_all_order = conn.execute(query_order).rowcount

        calc_pagination = calcPagination.paginationPage(CurrentPage, limitShowOrder, len_all_order)

        all_order = conn.execute(query_order.offset(calc_pagination[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()
    
    return {'len_all_order' : len_all_order, 'pagination' : calc_pagination[0], 'all_order' : all_order}

def getRevenueByDriver(id_driver, CurrentPage, limitShowOrder):
    with engine.connect() as conn:
        query_order = detailOrder.select().where(detailOrder.c.idDriver == id_driver)
        query_revenue = select(func.sum(detailOrder.c.totalCost.cast(Integer))).where(detailOrder.c.idDriver == id_driver)

        len_all_order = conn.execute(query_order).rowcount
        all_revenue = conn.execute(query_revenue).scalar() or 0

        calc_pagination = calcPagination.paginationPage(CurrentPage, limitShowOrder, len_all_order)

        all_order = conn.execute(query_order.offset(calc_pagination[1]).limit(limitShowOrder).order_by(detailOrder.c.totalCost.desc())).fetchall()
    
    return {'len_all_order' : len_all_order, 'pagination' : calc_pagination[0], 'all_order' : all_order, 'all_revenue' : int(all_revenue)}

def returnOrderDriver(id_driver, CurrentPage, limitShowOrder):
    with engine.connect() as conn:
        query_order = detailOrder.select().where(and_(detailOrder.c.idDriver == id_driver, detailOrder.c.status == 'Return-Order'))
        len_all_order = conn.execute(query_order).rowcount
        calc_pagination = calcPagination.paginationPage(CurrentPage, limitShowOrder, len_all_order)

        all_order = conn.execute(query_order.offset(calc_pagination[1]).limit(limitShowOrder).order_by(detailOrder.c.dateCreate.desc())).fetchall()

    return {'len_all_order' : len_all_order, 'pagination' : calc_pagination[0], 'all_order' : all_order}