from django.shortcuts import render
import datetime
from firebase_admin import storage
from dotenv import load_dotenv
import jwt
import platform
import os

from . import exec_data
from . import get_inform_order

# Create your views here.

# TODO
# This function use for upload image to firebase storage
# When driver choose image and press submit button, this function will execute
# def uploadImageFirebase(files, remote_file_name):
#     bucket = storage.bucket()
#     blob = bucket.blob(remote_file_name)
#     blob.upload_from_file(files.stream)

# TODO
# Check valid token and exp time, if not return homepage or login page
# @driverPage_blueprint.before_request
# def checkCurrentToken():
#     if 'tokenDeliveryWedDemo' not in session:
#         return redirect(url_for('loginAccountBp.loginAccount'))

#     # This function help load a key - value in .env file
#     load_dotenv()
#     secretKey = os.getenv('secret_key')
#     decodeToken = jwt.decode(session['tokenDeliveryWedDemo'], secretKey, algorithms = ["HS256"])

#     currentTime = datetime.datetime.now()
#     endTimeToken = datetime.datetime.strptime(decodeToken['endTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')

#     # Check exp time, if end exp time, return to login page
#     if currentTime < endTimeToken:
#         roleUserTemp = decodeToken['roleUser']
#         if roleUserTemp == 'Driver':
#             session['roleUser'] = 'Driver'
#             session['idDriver'] = decodeToken['idRoleUser']
#         else:
#             return "<p>You are not login at role user, so you can't access this page</p>"
#     else:
#         return redirect(url_for('loginAccountBp.loginAccount', notificationEndToken = True))


def baseDriverPage(request):
    id_driver =  request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.driverBasePage(id_driver)
    driverName = execute_database[0]
    len_all_order = execute_database[1] 

    context_return = {'driverName' : driverName, 'allOrderNeedDeliveryHomepage' : len_all_order}
    return render(request, 'Driver/baseDriverPage.html', context_return)

def orderSuccess(request, CurrentPage = 1):
    if request.method == 'POST':
        form_data = request.POST
        
        # TODO code here
        # if 'inputFilterDate' in form_data:
        #     dateValue = form_data['inputDateFilter']
        #     return redirect(url_for('orderSuccessFilterBp.orderSuccessFilterDate', CurrentPage = 1, dateValue = dateValue))     
        # elif 'inputFilterMonth' in form_data:
        #     monthValue = form_data['inputMonthFilter']
        #     return redirect(url_for('orderSuccessFilterBp.orderSuccessFilterMonth', CurrentPage = 1, monthValue = monthValue))
        # elif 'inputFilterYear' in form_data:
        #     yearValue = form_data['inputYearFilter']
        #     return redirect(url_for('orderSuccessFilterBp.orderSuccessFilterYear', CurrentPage = 1, yearValue = yearValue))
            
    id_driver = request.session.get('id_role_user_ddelivery', None)
    execute_database = exec_data.driverOrderSuccess(id_driver, CurrentPage)
    allOrderSuccess = execute_database[1]
    paginationPage = execute_database[0]
    len_lst_order_success = execute_database[2]

    all_inform = get_inform_order.get_information(allOrderSuccess)

    context_return = {'allOrderSuccess' : allOrderSuccess, 'lstSender' : all_inform[0], 'lstReceiver' : all_inform[1], 'lstType' : all_inform[2], 'CurrentPage' : CurrentPage, 'paginationPage': paginationPage, 'len_lst_order_success' : len_lst_order_success}
    context_return['range_pagination_page'] = [iteration for iteration in range(1, paginationPage + 1)]

    return render(request, 'Driver/orderSuccess.html', context_return)

def orderDelivery(request, CurrentPage = 1):
    if request.method == 'POST':
        getCodeOrder = request.form['valueDeliverySuccess']
        driverExecuteDatabase.driverPostOrderDelivery(getCodeOrder)
            
        imageDeliverySuccess = request.files['ImageDeliverySuccess']
        uploadImageFirebase(imageDeliverySuccess, f'DeliverySuccess/{getCodeOrder}.jpg')

        return redirect(url_for('.orderDelivery', CurrentPage = 1, notificationMsg = f'Update Success order "{getCodeOrder}"'))
    
    id_driver = request.session.get('id_role_user_ddelivery', None)
    notificationMsg = request.args.get('notificationMsg', 'None')

    execute_database = driverExecuteDatabase.driverOrderDelivery(id_driver, CurrentPage, limitShowOrder)
    lstOrder = execute_database[1]
    paginationPage = execute_database[0]
    len_lst_order = execute_database[2]

    lstInformDelivery = giveInform.giveInformationAboutOrder(lstOrder)
    context_return = {'allOrderDelivery' : lstOrder, 'lstSender' : lstInformDelivery[0],
        'lstReceiver' : lstInformDelivery[1], 'CurrentPage' : CurrentPage,
        'lstProduct' : lstInformDelivery[2], 'paginationPage' : paginationPage, 'len_lst_order' : len_lst_order}

    if notificationMsg == 'None':
        return render_template('orderDelivery.html', **context_return)
    else:
        context_return['notificationMsg'] = notificationMsg
        return render_template('orderDelivery.html', **context_return)

def orderNeedTransmit(CurrentPage = 1):
    id_driver = session['idDriver']
    if request.method == 'POST':
        if 'updateTransmit' in request.form:
            getCodeOrder = request.form['valueTransmit']
            
            imageGetProduct = request.files['ImageGetProduct']
            imageInTransport = request.files['ImageInTransport']
            uploadImageFirebase(imageGetProduct, f'ImageGetProduct/{getCodeOrder}.jpg')
            uploadImageFirebase(imageInTransport, f'ImageInTransport/{getCodeOrder}.jpg')
            
            driverExecuteDatabase.driverPostOrderNeedDelivery(getCodeOrder)
            return redirect(url_for('.orderNeedTransmit', CurrentPage = 1, notificationMsg = f'Update Delivery order {getCodeOrder} Success'))

    execute_database = driverExecuteDatabase.driverOrderNeedDelivery(id_driver, CurrentPage, limitShowOrder)
    lstOrder = execute_database[1]
    paginationPage = execute_database[0]
    nameDriver = execute_database[2]
    len_lst_order = execute_database[3]

    lstInformNeedDelivery = giveInform.giveInformationAboutOrder(lstOrder)
    notificationMsg = request.args.get('notificationMsg', 'None')

    context_return = {'allOrderNeedDelivery' : lstOrder, 'lstSender' : lstInformNeedDelivery[0], 'lstReceiver' : lstInformNeedDelivery[1],
        'lstProduct' : lstInformNeedDelivery[2], 'nameDriver' : nameDriver, 'CurrentPage' : CurrentPage, 'paginationPage' : paginationPage,
        'len_lst_order' : len_lst_order}

    if notificationMsg == 'None':
        return render_template('orderNeedTransmit.html',  **context_return)
    else:
        context_return['notificationMsg'] = notificationMsg
        return render_template('orderNeedTransmit.html', **context_return)