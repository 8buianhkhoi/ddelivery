from django.shortcuts import render, redirect
from dotenv import load_dotenv
import os 

from . import exec_data
from . import get_inform_order

def baseDriverPersonalPage(request, notificationMsg = None):
    id_driver = request.session.get('id_role_user_ddelivery', None)

    if request.method == 'POST':
        form_data = request.POST 
        print(form_data)
        notificationMsg = ''
        
        if 'submitBasicInform' in form_data:
            driver_user = form_data['inputNameDriver']
            tel_driver = form_data['inputTelDriver']
            gmail_driver = form_data['inputGmailDriver']
            gender_driver = form_data['inputGenderDriver']
            citizen_identify_number = form_data['inputCitizenIdentifyNumber']

            statusUpdateBasic = exec_data.updateBasicInformDriver(id_driver, driver_user, tel_driver, gmail_driver, gender_driver, citizen_identify_number)
            if statusUpdateBasic == 'OK':
                notificationMsg = 'Update basic information success'
            else:
                notificationMsg = 'Update basic information fail'
        elif 'submitAdvanceInform' in form_data:
            province_driver = form_data['chooseProvinceDriverProfile']
            district_driver = form_data['chooseDistrictDriverProfile']
            ward_driver = form_data['chooseWardDriverProfile']
            
            if province_driver == 'NotProvince' or district_driver == 'notDistrict' or ward_driver == 'NotWard':
                return redirect('Driver_app:Driver_profile', notificationMsg = "Update advanced information fail")

            statusUpdateAdvanced = exec_data.updateAdvancedInformDriver(id_driver, province_driver, district_driver, ward_driver)
            if statusUpdateAdvanced == 'OK':
                notificationMsg = "Update advanced information success"
            else:
                notificationMsg = "Update advanced information fail"
        elif 'submitChangePass' in form_data:
            first_pass = form_data['inputFirstPass']
            second_pass = form_data['inputSecondPass']
            load_dotenv()
            secret_key = os.getenv('secret_key')

            statusUpdatePass = exec_data.updatePasswordDriver(id_driver, first_pass, secret_key)
            if statusUpdatePass == 'OK':
                notificationMsg = "Change password success"
            else:
                notificationMsg = "Change password fail"
        elif 'submitDriverLicense' in form_data:
            full_name = form_data['inputFullNameLicenseDriver']
            beginning_date = form_data['inputBeginningDateLicenseDriver']
            class_motor = form_data['inputClassLicenseDriver']

            statusUpdateDriverLicense = exec_data.updateDriverLicense(id_driver, full_name, beginning_date, class_motor)
            if statusUpdateDriverLicense =='OK':
                notificationMsg = "Change driver license success"
            else:
                notificationMsg = "Change driver license fail"
        elif 'submitMotorRegCert' in form_data:
            number_reg_cert = form_data['inputNumberRegCert']
            palate_reg_cert = form_data['inputPalateRegCert']
            branch_reg_cert = form_data['inputBranchRegCert']
            color_reg_cert = form_data['inputColorRegCert']
            capacity_motor_reg_cert = form_data['inputCapacityMotorRegCert']
            first_reg_cert = form_data['inputFirstRegCert']

            statusUpdateMotorRegCert = exec_data.updateDriverRegCert(id_driver, number_reg_cert, palate_reg_cert, branch_reg_cert, color_reg_cert, capacity_motor_reg_cert, first_reg_cert)
            if statusUpdateMotorRegCert == 'OK':
                notificationMsg = 'Update motorbike registration certificate success'
            else:
                notificationMsg = 'Update motorbike registration certificate fail'

        return redirect('Driver_app:Driver_profile', notificationMsg = notificationMsg)

    driverInform = exec_data.executeGetInformDriver(id_driver)
    
    if notificationMsg is None:
        return render(request, 'Driver/driverPersonalProfile.html', {'driverInform' : driverInform})
    else:
        return render(request, 'Driver/driverPersonalProfile.html', {'driverInform' : driverInform, 'notificationMsg' : notificationMsg})
