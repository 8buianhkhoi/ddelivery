from django.urls import path

from . import views 

app_name = 'Driver_app'

urlpatterns = [
    path('homepage/', views.baseDriverPage, name = 'Driver_homepage'),
    path('order-was-successful/<int:CurrentPage>/', views.orderSuccess, name = 'Driver_order_success')
]