from django.shortcuts import render
import datetime
from firebase_admin import storage
from dotenv import load_dotenv
import jwt
import platform
import os

from . import exec_data

# Create your views here.

# TODO
# This function use for upload image to firebase storage
# When driver choose image and press submit button, this function will execute
# def uploadImageFirebase(files, remote_file_name):
#     bucket = storage.bucket()
#     blob = bucket.blob(remote_file_name)
#     blob.upload_from_file(files.stream)

# TODO
# Check valid token and exp time, if not return homepage or login page
# @driverPage_blueprint.before_request
# def checkCurrentToken():
#     if 'tokenDeliveryWedDemo' not in session:
#         return redirect(url_for('loginAccountBp.loginAccount'))

#     # This function help load a key - value in .env file
#     load_dotenv()
#     secretKey = os.getenv('secret_key')
#     decodeToken = jwt.decode(session['tokenDeliveryWedDemo'], secretKey, algorithms = ["HS256"])

#     currentTime = datetime.datetime.now()
#     endTimeToken = datetime.datetime.strptime(decodeToken['endTimeTokenStr'], f'%d-%m-%Y %H:%M:%S')

#     # Check exp time, if end exp time, return to login page
#     if currentTime < endTimeToken:
#         roleUserTemp = decodeToken['roleUser']
#         if roleUserTemp == 'Driver':
#             session['roleUser'] = 'Driver'
#             session['idDriver'] = decodeToken['idRoleUser']
#         else:
#             return "<p>You are not login at role user, so you can't access this page</p>"
#     else:
#         return redirect(url_for('loginAccountBp.loginAccount', notificationEndToken = True))


def baseDriverPage(request):
    id_driver =  request.session.get('id_role_user_ddelivery', None)

    execute_database = exec_data.driverBasePage(id_driver)
    driverName = execute_database[0]
    len_all_order = execute_database[1] 

    return render_template('baseDriverPage.html', driverName = driverName, allOrderNeedDeliveryHomepage = len_all_order)