import datetime

from . import pagination_page
from Homepage.models import *

limit_show_order = 30

def driverOrderSuccessFilterDate(idDriver, dateChoose, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Success', real_time_arrive__date = dateChoose.date())
        len_order_date = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_order_date)

        order_success_date = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        return [calc_pagination[0], order_success_date, len_order_date]
    except:
        return False

def driverOrderSuccessFilterMonth(idDriver, monthValue, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Success', real_time_arrive__year = int(monthValue[:4]), real_time_arrive__month = int(monthValue[5:]))

        len_order_month = queryOrder.count()
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_order_month)

        order_success_month = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        return [calc_pagination[0], order_success_month, len_order_month]
    except:
        return False

def driverOrderSuccessFilterYear(idDriver, yearValue, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Success', real_time_arrive__year = int(yearValue))

        len_order_year = queryOrder.count()
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_order_year)

        order_success_year = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        return [calc_pagination[0], order_success_year, len_order_year]
    except:
        return False

def driverSearchOrderTel(idDriver, telNum, CurrentPage):
    try:
        lstIdShippingAddr = []
        lstIdDeliveryAddr = []
        lstOrder = []
        setOrder = set()

        allOrderByDriver = Detail_Order_tb.objects.filter(id_driver = idDriver).values()

        for order in allOrderByDriver:
            idShippingAddr = order['id_shipping_addr_id']
            idDeliveryAddr = order['id_delivery_addr_id']

            # TODO check code here again
            getShippingAddr = Shipping_Addr_tb.objects.filter(id = idShippingAddr).values()[0]['tel_num']
            getDeliveryAddr = Delivery_Addr_tb.objects.filter(id = idDeliveryAddr).values()[0]['tel_num']

            if getShippingAddr == telNum:
                lstIdShippingAddr.append(idShippingAddr)
            if getDeliveryAddr == telNum:
                lstIdDeliveryAddr.append(idDeliveryAddr)
        for index in lstIdShippingAddr:
            idDetailOrder = Detail_Order_tb.objects.filter(id_shipping_addr = index).values()[0]['id']
            setOrder.add(idDetailOrder)
        for index in lstIdDeliveryAddr:
            idDetailOrder = Detail_Order_tb.objects.filter(id_delivery_addr = index).values()[0]['id']
            setOrder.add(idDetailOrder)
        for index in setOrder:
            getOrder = Detail_Order_tb.objects.filter(id = index).values()
            lstOrder.append(getOrder)
        
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len(lstOrder))
        lstOrder = lstOrder[calc_pagination[1] : calc_pagination[2] + 1]
    
        return [calc_pagination[0], lstOrder]
    except:
        return False

def driverSearchOrderDay(idDriver, chooseDate, CurrentPage):
    try:
        dateChoose = datetime.datetime.strptime(chooseDate, f'%Y-%m-%d')
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, date_create__date = dateChoose.date())

        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], lstOrder]
    except:
        return False

def driverSearchOrderMonth(idDriver, chooseMonth, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, date_create__year = int(chooseMonth[:4]), date_create__month = int(chooseMonth[5:]))

        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order]
        return [calc_pagination[0], lstOrder]
    except:
        return False

def driverSearchOrderYear(idDriver, chooseYear, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, date_create__year = int(chooseYear))
        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], lstOrder]
    except:
        return False

def driverOrderSuccess(idDriver, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Success')
        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], lstOrder, len_lst_order]
    except:
        return False

def driverOrderDelivery(idDriver, CurrentPage):
    try:
        queryOrder = Detail_Order_tb.objects.filter(id_driver =  idDriver, status_order = 'Delivery')
        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], lstOrder, len_lst_order]
    except:
        return False

def driverPostOrderDelivery(getCodeOrder):
    try:
        dateTimeNow = datetime.datetime.now()
        updateRealTimeArrive = Detail_Order_tb.objects.filter(code_order_delivery = getCodeOrder).update(real_time_arrive = dateTimeNow)
        queryUpdateSuccess = Detail_Order_tb.objects.filter(code_order_delivery = getCodeOrder).update(status_order = 'Success')
    except:
        return False

def driverOrderNeedDelivery(idDriver, CurrentPage):
    try:
        nameDriver = Driver_tb.objects.filter(id = idDriver).values()[0]['driver_name']

        queryOrder = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Need-Delivery')

        len_lst_order = queryOrder.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_lst_order)
        lstOrder = queryOrder.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return [calc_pagination[0], lstOrder, nameDriver, len_lst_order]
    except:
        return False

def driverPostOrderNeedDelivery(getCodeOrder):
    with engine.connect() as conn:
        queryUpdateTransmit = detailOrder.update().where(detailOrder.c.codeOrder == getCodeOrder).values(status='Delivery')
        conn.execute(queryUpdateTransmit)
        
        executeDatabaseOS(conn)

def driverUpdateStatusDelivery(idDriver):
    try:
        queryUpdateStatus = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Need-Delivery').update(status_order = 'Delivery')
    except:
        return False

def driverBasePage(idDriver):
    try:
        print('---- : ', idDriver)
        driverName = Driver_tb.objects.filter(id = idDriver).values()[0]['full_name_driver']
        queryAllOrderNeedDelivery = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Need-Delivery')

        len_all_order = queryAllOrderNeedDelivery.count()

        return [driverName, len_all_order]

    except Exception as e:
        print(e)
        return False

def executeGetInformDriver(idDriver):
    try:
        driverInform = Driver_tb.objects.filter(id = idDriver).values()
        return driverInform
    except:
        return False

def updateBasicInformDriver(idDriver, nameDriver, telDriver, gmailDriver, genderDriver, citizenIdentifyNumber):
    try:
        if nameDriver != '':
            queryUpdateNameDriver = Driver_tb.objects.filter(id = idDriver).update(full_name_driver = nameDriver)
        if telDriver != '':
            queryUpdateTelDriver = Driver_tb.objects.filter(id = idDriver).update(tel_driver = telDriver)
        if gmailDriver != '':
            queryUpdateGmailDriver = Driver_tb.objects.filter(id = idDriver).update(gmail_driver = gmailDriver)
        if genderDriver != '':
            queryUpdateGenderDriver = Driver_tb.objects.filter(id = idDriver).update(gender_driver = genderDriver)
        if citizenIdentifyNumber != '':
            queryUpdateCitizenIdentifyNumber = Driver_tb.objects.filter(id = idDriver).update(cin_driver = citizenIdentifyNumber)
        
        return 'OK'
    except:
        return False

def updateAdvancedInformDriver(idUser, provinceUser, districtUser, wardUser):
    try:
        Users_tb.objects.filter(id = idUser).update(province_user = provinceUser, district_user = districtUser, ward_user = wardUser)

        return 'OK'
    except:
        return False

def hashPassword(password, salt=None, iterations= 100000, key_length =32):
    key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, iterations, dklen=key_length)
    
    return salt + key 

def updatePasswordDriver(idUser, passwordUser, secretKey):
    try:
        hash_bytes = hashPassword(passwordUser, secretKey.encode())
        hash_password = base64.b64encode(hash_bytes).decode('utf-8')

        Users_tb.objects.filter(id = idUser).update(password_user = hash_password)
        return 'OK'
    except Exception as e:
        return False

def updateDriverLicense(idDriver, fullName, beginDate, classMotor):
    try:
        Driver_license_tb.objects.filter(id_driver = idDriver).update(full_name = fullName, begin_date = beginDate, class_license = classMotor)
        return 'OK'
    except:
        return False

def updateDriverRegCert(idDriver, numberCert, palateCert, branchCert, colorCert, capacityMotorCert, firstRegCert):
    try:
        Motorbike_reg_cert_tb.objects.filter(id_driver = idDriver).update(number_reg = numberCert, plate = palateCert, branch = branchCert, color = colorCert, capacity = capacityMotorCert, first_reg = firstRegCert)
        return 'OK'
    except:
        return False

def allOrderNeedDeliveryAutoUpdateRoute(idDriver):
    try:
        all_order_need_delivery = Detail_Order_tb.objects.filter(id_driver = idDriver, status_order = 'Need-Delivery').values()
        return all_order_need_delivery
    except:
        return False

def getDeliveryAddrAutoUpdateRoute(idDATemp):
    try:
        deliveryAddr_temp = Delivery_Addr_tb.objects.filter(id = idDATemp).values()
        return deliveryAddr_temp
    except:
        return False

def getRevenueDay(id_driver, dayValue, CurrentPage):
    try:
        query_order_revenue = Detail_Order_tb.objects.filter(id_driver = id_driver, status_order = 'Success', real_time_arrive__date = dayValue.date()).annotate(total_cost_as_int = Cast('total_cost', IntegerField())).aggregate(total_cost = Sum('total_cost_as_int'))['total_cost']
        query_all_order = Detail_Order_tb.objects.filter(id_driver = id_driver, status_order = 'Success', real_time_arrive__date = dayValue.date())

        len_all_order = query_all_order.count()
        
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)

        all_order = query_all_order.order_by('-real_time_arrive')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        if query_order_revenue is None:
            query_order_revenue = 0
    
        return [calc_pagination[0], query_order_revenue, all_order]
    except:
        return False

def getRevenueMonth(id_driver, monthValue, CurrentPage):
    try:
        query_order_revenue = Detail_Order_tb.objects.filter(id_driver = id_driver, status_order = 'Success', real_time_arrive__year = int(monthValue[:4]), real_time_arrive__month = int(monthValue[5:])).annotate(total_cost_as_int = Cast('total_cost', IntegerField())).aggregate(total_cost = Sum('total_cost_as_int'))['total_cost']

        query_all_order = Detail_Order_tb.objects.filter(id_driver = id_driver, status_order = 'Success', real_time_arrive__year = int(monthValue[:4]), real_time_arrive__month = int(monthValue[5:]))
        
        len_all_order = query_all_order.count()
        
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)

        all_order = query_all_order.order_by('-real_time_arrive')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
        if query_order_revenue is None:
            query_order_revenue = 0

        return [calc_pagination[0], query_order_revenue, all_order]
    except:
        return False

def getRevenueYear(id_driver, yearValue, CurrentPage):
    try:
        query_order_revenue = Detail_Order_tb.objects.filter(id_driver = id_driver, status_order = 'Success', real_time_arrive__year = int(yearValue)).annotate(total_cost_as_int = Cast('total_cost', IntegerField())).aggregate(total_cost = Sum('total_cost_as_int'))['total_cost']

        query_all_order = Detail_Order_tb.objects.filter(id_driver = id_driver, status_order = 'Success', real_time_arrive__year = int(yearValue))
        
        len_all_order = query_all_order.count()
        
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)

        all_order = query_all_order.order_by('-real_time_arrive')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        if query_order_revenue is None:
            query_order_revenue = 0
        return [calc_pagination[0], query_order_revenue, all_order]
    except:
        return False

def getAllOrderByDriver(id_driver, CurrentPage):
    try:
        query_order = Detail_Order_tb.objects.filter(id_driver = id_driver)
        len_all_order = query_order.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)

        all_order = query_order.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()
    
        return {'len_all_order' : len_all_order, 'pagination' : calc_pagination[0], 'all_order' : all_order}
    except:
        return False

def getRevenueByDriver(id_driver, CurrentPage):
    try:
        query_order = Detail_Order_tb.objects.filter(id_driver = id_driver)
        query_revenue = Detail_Order_tb.objects.filter(id_driver = id_driver).annotate(total_cost_as_int = Cast('total_cost', IntegerField())).aggregate(total_cost = Sum('total_cost_as_int'))['total_cost']

        len_all_order = query_order.count()

        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)

        all_order = query_order.order_by('-total_cost')[calc_pagination[1] :calc_pagination[1] + limit_show_order].values()
    
        return {'len_all_order' : len_all_order, 'pagination' : calc_pagination[0], 'all_order' : all_order, 'all_revenue' : query_revenue}
    except:
        return False

def returnOrderDriver(id_driver, CurrentPage):
    try:
        query_order = Detail_Order_tb.objects.filter(id_driver = id_driver, status_order = 'Return-Order')
        len_all_order = query_order.count()
        calc_pagination = pagination_page.cal_pagination(CurrentPage, limit_show_order, len_all_order)

        all_order = query_order.order_by('-date_create')[calc_pagination[1] : calc_pagination[1] + limit_show_order].values()

        return {'len_all_order' : len_all_order, 'pagination' : calc_pagination[0], 'all_order' : all_order}
    except:
        return False