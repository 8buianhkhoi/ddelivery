from django.urls import path

from . import views 
from . import profile_views

app_name = 'Driver_app'

urlpatterns = [
    path('homepage/', views.baseDriverPage, name = 'Driver_homepage'),
    path('order-was-successful/<int:CurrentPage>/', views.orderSuccess, name = 'Driver_order_success'),
    path('order-delivery/<int:CurrentPage>/<notificationMsg>', views.orderDelivery, name = 'Driver_order_delivery'),
    path('order-need-delivery/<int:CurrentPage>/<notificationMsg>', views.orderNeedTransmit, name = 'Driver_order_need_delivery'),
    path('log-out/', views.logoutDriver, name = 'Driver_log_out'),
    path('update-all-delivery-order', views.updateStatusDelivery, name = 'Driver_update_all_delivery_order'),
    path('order-return/<int:CurrentPage>/', views.orderReturnDriver, name = 'Driver_order_return')
]