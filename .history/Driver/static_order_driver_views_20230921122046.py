from django.shortcuts import render, redirect
from dotenv import load_dotenv
import os 
import datetime
import locale

from . import exec_data
from . import get_inform_order

# @staticOrderDriver_blueprint.route('/calc-revenue-day/<dayValue>/<int:CurrentPage>')
# def staticRevenueDay(dayValue = 'None', CurrentPage = 1):
#     id_driver = session.get('idDriver')
#     dayValue = datetime.datetime.strptime(dayValue, f'%Y-%m-%d')

#     execute_database = driverExecuteDatabase.getRevenueDay(id_driver, dayValue, CurrentPage, limitShowOrder)
#     paginationPage = execute_database[0]
#     total_revenue = execute_database[1]
#     all_order = execute_database[2]
#     locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')

#     total_revenue = locale.format_string("%d", total_revenue, grouping=True)
#     all_inform_order = giveInform.giveInformationAboutOrder(all_order)

#     return render_template('staticRevenueDriver.html', all_order = all_order, paginationPage = paginationPage,
#         CurrentPage = CurrentPage, total_revenue = total_revenue, lstSender = all_inform_order[0], lstReceiver = all_inform_order[1],
#         lstProduct = all_inform_order[2], chooseDate = dayValue)

# @staticOrderDriver_blueprint.route('/calc-revenue-month/<monthValue>/<int:CurrentPage>')
# def staticRevenueMonth(monthValue = 'None', CurrentPage = 1):
#     id_driver = session.get('idDriver')
    
#     execute_database = driverExecuteDatabase.getRevenueMonth(id_driver, monthValue, CurrentPage, limitShowOrder)
#     paginationPage = execute_database[0]
#     total_revenue = execute_database[1]
#     all_order = execute_database[2]
#     locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')

#     total_revenue = locale.format_string("%d", total_revenue, grouping=True)
#     all_inform_order = giveInform.giveInformationAboutOrder(all_order)

#     return render_template('staticRevenueDriver.html', all_order = all_order, paginationPage = paginationPage,
#         CurrentPage = CurrentPage, total_revenue = total_revenue, lstSender = all_inform_order[0], lstReceiver = all_inform_order[1],
#         lstProduct = all_inform_order[2], chooseMonth = monthValue)

# @staticOrderDriver_blueprint.route('/calc-revenue-year/<yearValue>/<int:CurrentPage>', methods = ['GET', 'POST'])
# def staticRevenueYear(yearValue = 'None', CurrentPage = 1):
#     id_driver = session.get('idDriver')

#     execute_database = driverExecuteDatabase.getRevenueYear(id_driver, yearValue, CurrentPage, limitShowOrder)
#     paginationPage = execute_database[0]
#     total_revenue = execute_database[1]
#     all_order = execute_database[2]
#     locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')

#     total_revenue = locale.format_string("%d", total_revenue, grouping=True)
#     all_inform_order = giveInform.giveInformationAboutOrder(all_order)

#     return render_template('staticRevenueDriver.html', all_order = all_order, paginationPage = paginationPage,
#         CurrentPage = CurrentPage, total_revenue = total_revenue, lstSender = all_inform_order[0], lstReceiver = all_inform_order[1],
#         lstProduct = all_inform_order[2], chooseYear = yearValue)

# @staticOrderDriver_blueprint.route('/static-revenue-driver/<int:CurrentPage>', methods = ['GET', 'POST'])
# def staticRevenueDriverTime(CurrentPage = 1):
#     if request.method == 'POST':
#         form_data = request.form['chooseTypeStatic']
#         if 'all' in form_data:
#             return redirect(url_for('.staticRevenueDriverTime', CurrentPage = 1))
#         elif 'day' in form_data:
#             day_value = request.form['inputDate']
#             return redirect(url_for('.staticRevenueDay', dayValue = day_value, CurrentPage = 1))
#         elif 'month' in form_data:
#             month_value = request.form['inputMonth']
#             return redirect(url_for('.staticRevenueMonth', monthValue = month_value, CurrentPage = 1))
#         elif 'year' in form_data:
#             year_value = request.form['inputYear']
#             return redirect(url_for('.staticRevenueYear', yearValue = year_value, CurrentPage = 1))

#     id_driver = session.get('idDriver')

#     execute_database = driverExecuteDatabase.getRevenueByDriver(id_driver, CurrentPage, limitShowOrder)
#     paginationPage = execute_database['pagination']
#     len_all_order = execute_database['len_all_order']
#     all_order = execute_database['all_order']

#     all_inform_order = giveInform.giveInformationAboutOrder(all_order)
#     locale.setlocale(locale.LC_ALL, 'vi_VN.UTF-8')
#     all_revenue = execute_database['all_revenue']

#     all_revenue = locale.format_string("%d", all_revenue, grouping=True)

#     return_para = {'CurrentPage' : CurrentPage, 'lstSender' : all_inform_order[0], 'lstReceiver' : all_inform_order[1], 'lstProduct' : all_inform_order[2],
#         'len_all_order' : len_all_order, 'paginationPage' : paginationPage, 'all_order' : all_order, 'all_revenue' : all_revenue}
#     return render_template('staticRevenueDriverTime.html', **return_para)