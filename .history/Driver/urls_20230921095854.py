from django.urls import path

from . import views 

app_name = 'Driver_app'

urlpatterns = [
    path('homepage/', views.baseDriverPage, name = 'Driver_homepage')
]