from django.shortcuts import render, redirect
import pdfkit
import ast

# Create your views here.

# This route for create pdf file and download pdf file
def generatePDF(request):
    codeOrder = request.args.get('codeOrder')
    totalCost = request.args.get('totalCost')
    nameDriver = request.args.get('driverName')
    sender = ast.literal_eval(request.args.get('sender'))
    receiver = ast.literal_eval(request.args.get('receiver'))
    allProduct = [ast.literal_eval(request.args.get('allProduct')[1:-1])]
    dateCreate = request.args.get('dateCreate')
    departureTimeOrder = request.args.get('departureTimeOrder')
    estimateArriveTime = request.args.get('estimateArriveOrder')

    return render_template('generatePDF.html', codeOrder = codeOrder, totalCost = totalCost, nameDriver = nameDriver, sender = sender, 
                receiver = receiver, allProduct = allProduct, dateCreate = dateCreate, 
                departureTimeOrder = departureTimeOrder, estimateArriveTime = estimateArriveTime, pdfStatus = True)

def downloadPDF(request):
    codeOrder = request.args.get('codeOrder')
    totalCost = request.args.get('totalCost')
    nameDriver = request.args.get('driverName')
    sender = ast.literal_eval(request.args.get('sender'))
    receiver = ast.literal_eval(request.args.get('receiver'))
    allProduct = [ast.literal_eval(request.args.get('allProduct')[1:-1])]
    dateCreate = request.args.get('dateCreate')
    departureTimeOrder = request.args.get('departureTimeOrder')
    estimateArriveTime = request.args.get('estimateArriveOrder')

    renderer = render_template('generatePDF.html', codeOrder = codeOrder, totalCost = totalCost, nameDriver = nameDriver, sender = sender, 
                receiver = receiver, allProduct = allProduct, dateCreate = dateCreate, 
                departureTimeOrder = departureTimeOrder, estimateArriveTime = estimateArriveTime)

    if platform.system() == 'Linux':
        config = pdfkit.configuration(wkhtmltopdf = '/usr/local/bin/wkhtmltopdf')
        pdf = pdfkit.from_string(renderer, False, configuration = config)
    elif platform.system() == 'Windows':
        pdf = pdfkit.from_string(renderer, False)

    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'attachment; filename = output.pdf'

    return response 
