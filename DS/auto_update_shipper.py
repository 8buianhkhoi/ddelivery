# from flask import Blueprint, session
# from models.models import *
# from math import ceil
# import json
# import numpy as np
# from sklearn.cluster import KMeans
# import platform

# autoUpdateShipper__bp = Blueprint('autoUpdateShipperDS', __name__, static_folder = 'static', template_folder = 'templates')

# Route này dùng thuật toán Kmeans để chia n đơn hàng cho m tài xế. Ở đây là 1000 đơn hàng.
# @autoUpdateShipper__bp.route('/auto-update-shipper', methods= ['GET', 'POST'])
# def autoUpdateShipper():
#     conn = engine.connect()
#     queryAllOrderNeedDelivery = detailOrder.select().where(and_(detailOrder.c.idDeliverySystem == session['idDS'], detailOrder.c.status == 'Pending'))
#     allOrderNeedDelivery = conn.execute(queryAllOrderNeedDelivery).fetchall()
#     allDriver = conn.execute(driver.select().where(driver.c.idDeliverySystem == session['idDS'])).fetchall()
#     eachDriverOrder = ceil(len(allOrderNeedDelivery) / len(allDriver))
    
#     # (21.0363416, 105.855503) là tọa độ kinh tuyến vĩ tuyến của phường phúc tân, quận hoàn kiếm hà nội, giả sử làm kho cho tổng kết hàng hóa
#     store_coordinate = [21.0363416, 105.855503]
#     allOrderCoord = []
#     with open('deliverySystem_blueprint/static/json/CoordAddressOrder.json', encoding ='utf8') as coordAddressOrder__json:
#         allCoordAddressOrder = json.load(coordAddressOrder__json)
#     with open('deliverySystem_blueprint/static/json/mapAddressCoord.json', encoding ='utf8') as mapAddressCoord__json:
#         allMapAddressCoord = json.load(mapAddressCoord__json)

#     for index in allCoordAddressOrder:
#         indexTemp = allCoordAddressOrder[index]
#         for subindex in indexTemp:
#             allOrderCoord.append(indexTemp[subindex])
    
#     order_coordinates  = np.array(allOrderCoord)
#     num_clusters = 10
#     kmeans = KMeans(n_clusters=num_clusters)
#     all_coordinates = np.vstack((store_coordinate, order_coordinates))
#     store_coordinate = all_coordinates[0]
#     order_coordinates = all_coordinates[1:]
#     cluster_labels = kmeans.fit_predict(order_coordinates)

    # Vì thuật toán Kmean không make sure 1 cụm là 100 địa điểm, có cụm sẽ ít hơn hoặc nhiều hơn, mặc định thuật toán là vậy, cần phải qua 1 bước xử lý nữa để make sure
    # 1 cụm là 100 điểm
    # address_per_cluster = 100
    # for cluster in range(num_clusters):
    #     cluster_points = order_coordinates[cluster_labels == cluster]
    #     num_points = len(cluster_points)
    #     if num_points > address_per_cluster:
    #         cluster_points = cluster_points[:address_per_cluster]
    #         num_points = address_per_cluster
    #     elif num_points < address_per_cluster:
    #         additional_points = order_coordinates[np.random.choice(order_coordinates.shape[0], address_per_cluster - num_points, replace = False)]
    #         print(len(cluster_points))
    #         cluster_points = np.concatenate((cluster_points, additional_points), axis = 0)

    #     order_coordinates[cluster_labels == cluster] = cluster_points

    # clusters = [[] for _ in range(num_clusters)]
    # for i, label in enumerate(cluster_labels):
    #     clusters[label].append(order_coordinates[i])
    # for i, cluster in enumerate(clusters):
    #     for coord in cluster:
    #         for keyDict, valueDict in allMapAddressCoord.items():
    #             if valueDict == [coord[0], coord[1]]:
    #                 wardAddr = keyDict.split(',')[0]
    #                 districtAddr = keyDict.split(',')[1].lstrip()
    #                 provinceAddr = keyDict.split(',')[2].lstrip()
    #                 idMatchDeliveryAddress = conn.execute(deliveryAddr.select().where(and_(deliveryAddr.c.country == provinceAddr, deliveryAddr.c.district == districtAddr, deliveryAddr.c.ward == wardAddr))).fetchall()
    #                 idDriverTemp = allDriver[i][0] 
    #                 # print(idMatchDeliveryAddress)
    #                 for idDeliveryAddress in idMatchDeliveryAddress:
                        
    #                     queryUpdateDriver = detailOrder.update().where(detailOrder.c.idDeliveryAddr == idDeliveryAddress[0]).values(idDriver = idDriverTemp)
    #                     conn.execute(detailOrder.update().where(detailOrder.c.idDeliveryAddr == idDeliveryAddress[0]).values(status='Need-Delivery'))
    #                     conn.execute(queryUpdateDriver)

    #                     if platform.system() == 'Linux':
    #                         conn.commit()
    #                         conn.close()

    #                     idDS = session['idDS']
    #                     allInventory = conn.execute(inventory.select().where(inventory.c.idDeliverySystem == idDS)).fetchall()
    #                     notificationUpdate = updateIdInventory.updateIdInventory(idDeliveryAddress = idDeliveryAddress, allInventory = allInventory)
    #                     if notificationUpdate == 'Error':
    #                         return '<p>Some problem happen, maybe inventory not enough space to contain order</p>'

    # return redirect(url_for('deliverySystem.orderInPending'))