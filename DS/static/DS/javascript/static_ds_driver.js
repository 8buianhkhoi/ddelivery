function changeKindTime(){
    let kind_time = document.getElementsByClassName('choose__kind__time__select')[0].value

    if (kind_time === 'date') {
        document.getElementsByClassName('choose__kind__time__day')[0].style.display = 'inline-block'
        document.getElementsByClassName('input__kind__time__date')[0].setAttribute('required', 'true')
    }
    else{
        document.getElementsByClassName('choose__kind__time__day')[0].style.display = 'none'
    }
}