function onPageLoad(){
    let searchOrder = document.getElementsByClassName("each___input__search")

    for (let index = 0; index < searchOrder.length; index++ ){
        searchOrder[index].style.display = 'none'
    }
}

function searchByCodeOrder(){
    onPageLoad()
    document.getElementsByClassName("input_search__code__order")[0].style.display = 'block'
}

function searchByDate(){
    onPageLoad()
    document.getElementsByClassName("input__search__date")[0].style.display = 'block'
}

function searchByMonth(){
    onPageLoad()
    document.getElementsByClassName("input__search__month")[0].style.display = 'block'
}

function searchByYear(){
    onPageLoad()
    document.getElementsByClassName("input__search__year")[0].style.display = 'block'
}

function searchByUser(){
    onPageLoad()
    document.getElementsByClassName('input__search__user__tel')[0].style.display = 'block'
}

function onPageLoadPagination(){
    if (paginationPage > 5){
        let leftPagination = 2;
        let rightPagination = 2;
        
        if ((CurrentPage - leftPagination < 1) && (CurrentPage + rightPagination <= paginationPage)){
            if (CurrentPage - leftPagination === 0) {
                leftPagination = leftPagination - 1
                rightPagination = rightPagination + 1
            }
            else if (CurrentPage - leftPagination === -1) {
                leftPagination = leftPagination - 2
                rightPagination = rightPagination + 2
                document.getElementById('btn__previous__pagination').style.display = 'none'
                document.getElementById('btn__first__pagination__page').style.display = 'none'
            }
        }
        else if ((CurrentPage - leftPagination >= 1) && (CurrentPage + rightPagination > paginationPage)){
            if (CurrentPage + rightPagination === paginationPage + 1){
                leftPagination = leftPagination + 1
                rightPagination = rightPagination -1
            }
            else if (CurrentPage + rightPagination === paginationPage + 2){
                leftPagination = leftPagination + 2
                rightPagination = rightPagination -2
                document.getElementById('btn__next__pagination').style.display = 'none'
                document.getElementById('btn__end__pagination__page').style.display = 'none'
            }
        }
        let allBtnPagination = document.getElementsByClassName('btn__pagination');
        
        for(let index = 0; index < allBtnPagination.length; index++){
            allBtnPagination[index].style.display = 'none'
        }

        document.getElementById(`btn__page__pagination__page${CurrentPage}`).style.display = 'inline-block';
        
        for (let index = 0; index < leftPagination; index ++){
            document.getElementById(`btn__page__pagination__page${CurrentPage - (index + 1)}`).style.display = 'inline-block';
        }
        for (let index = 0; index < rightPagination; index ++){
            document.getElementById(`btn__page__pagination__page${CurrentPage + (index + 1)}`).style.display = 'inline-block';
        }
    }
}

function previousBtnPagination(){
    CurrentPage = CurrentPage - 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function nextBtnPagination(){
    CurrentPage = CurrentPage + 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function firstPagePagination(){
    CurrentPage = 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function lastPagePagination(){
    CurrentPage = paginationPage;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}