function showDetailOptionDSFee(name_option){
    let all_detail_option = document.getElementsByClassName('detail__option__inner')
    let all_btn_option = document.getElementsByClassName('btn__choose__option__ds__profile__fee')

    for (let index = 0; index < all_detail_option.length; index ++){
        all_detail_option[index].style.display = 'none'
    }
    for (let index = 0; index < all_btn_option.length; index ++){
        all_btn_option[index].className = 'btn__choose__option__ds__profile__fee'
    }

    if (name_option === 'humanResourceCost'){
        document.getElementsByClassName('human__cost__ds')[0].style.display = 'block'
        all_btn_option[1].className = 'btn__choose__option__ds__profile__fee active__option__ds__profile__fee'
    }
    else if (name_option === 'warehousingFee'){
        document.getElementsByClassName('warehousing__fee__ds')[0].style.display = 'block'
        all_btn_option[0].className = 'btn__choose__option__ds__profile__fee active__option__ds__profile__fee'
    }
}