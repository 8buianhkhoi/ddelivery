function showDetailOptionProfile(name_option){
    all_detail_option = document.getElementsByClassName('detail__option__inner')
    all_btn_option = document.getElementsByClassName('btn__choose__option__personal__profile')

    for (let index = 0; index < all_detail_option.length; index ++){
        all_detail_option[index].style.display = 'none'
    }
    for (let index = 0; index < all_btn_option.length; index ++){
        all_btn_option[index].className = 'btn__choose__option__personal__profile'
    }

    if (name_option === 'advance'){
        document.getElementsByClassName('advance__information__personal__profile')[0].style.display = 'block'
        all_btn_option[1].className = 'btn__choose__option__personal__profile active__option__personal__profile'
    }
    else if (name_option === 'basic'){
        document.getElementsByClassName('basic__information__personal__profile')[0].style.display = 'block'
        all_btn_option[0].className = 'btn__choose__option__personal__profile active__option__personal__profile'
    }
    else if (name_option === 'changePass'){
        document.getElementsByClassName('change__password__personal__profile')[0].style.display = 'block'
        all_btn_option[2].className = 'btn__choose__option__personal__profile active__option__personal__profile'
    }
    // else if (name_option === 'changeAvatar') {
    //     document.getElementsByClassName('change__avatar__personal__profile')[0].style.display = 'block'
    //     all_btn_option[3].className = 'btn__choose__option__personal__profile active__option__personal__profile'
    // }
    else if(name_option === 'driverLicense' ){
        document.getElementsByClassName('change__driver__license')[0].style.display = 'block'
        all_btn_option[3].className = 'btn__choose__option__personal__profile active__option__personal__profile'
    }
    else if (name_option === 'motorRegCert'){
        document.getElementsByClassName('change__motor__reg__cert')[0].style.display = 'block'
        all_btn_option[4].className = 'btn__choose__option__personal__profile active__option__personal__profile'
    }
}

function onLoadProvince(){
    $.ajax({
        url:'/user-page/get-address/get-province',
        method: 'POST',
        data: {},
        success: function(response){
            let allProvince = response['result']
            for (let index in allProvince){
                let eachProvince = allProvince[index]
                let chooseSelect = document.getElementsByClassName('choose__province__advance__option');
                
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachProvince;
                newOptionElement.value = eachProvince;
                chooseSelect[0].appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function loadDistrict(){
    let province = document.getElementsByClassName('choose__province__advance__option')[0].value
    $.ajax({
        url:'/user-page/get-address/get-district',
        method: 'POST',
        data: {province:province},
        success: function(response){
            let allDistrict = response['result']
            let chooseSelect = document.getElementsByClassName('choose__district__advance__option')[0]
            let lengthOptionInSelect = chooseSelect.options.length
            for (let option = lengthOptionInSelect - 1; option >= 1 ; option--){
                chooseSelect.options[option].remove();
            }
            for (let index in allDistrict){
                let eachDistrict = allDistrict[index]
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachDistrict;
                newOptionElement.value = eachDistrict;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function loadWard(){
    let district = document.getElementsByClassName('choose__district__advance__option')[0].value
    let province = document.getElementsByClassName('choose__province__advance__option')[0].value
    $.ajax({
        url:'/user-page/get-address/get-wards',
        method: 'POST',
        data: {province:province, district :district},
        success: function(response){
            let allWard = response['result']
            let chooseSelect = document.getElementsByClassName('choose__ward__advance__option')[0]
            let lengthOptionInSelect = chooseSelect.options.length 
            for (let option = lengthOptionInSelect - 1; option >=1 ; option--){
                chooseSelect.options[option].remove()
            }
            for (let index in allWard){
                let eachWard = allWard[index]
                let newOptionElement = document.createElement('option');
                newOptionElement.text = eachWard;
                newOptionElement.value = eachWard;
                chooseSelect.appendChild(newOptionElement)
            }
        },
        error: function(error){
            console.log(error)
        }
    })
}

function checkFirstSecondPassword(){
    let first_pass = document.getElementsByClassName('input__first__pass')[0].value
    let second_pass = document.getElementsByClassName('input__second__pass')[0].value

    if ( first_pass !== second_pass ) {
        document.getElementById('check__first__second__pass').innerText = 'Password and confirm password must match'
        document.getElementById('check__first__second__pass').style.color = 'red'
        document.getElementById('submit__change__pass').setAttribute('disabled', 'disabled')
    }
    else {
        document.getElementById('check__first__second__pass').innerText = 'Match Password'
        document.getElementById('check__first__second__pass').style.color = 'blue'
        document.getElementById('submit__change__pass').removeAttribute('disabled')
    }
}