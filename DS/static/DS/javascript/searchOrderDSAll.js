function changeTypeSearchOrder(){
    let kind_type = document.getElementsByClassName('select__type__search')[0].value
    let all_kind_type = document.getElementsByClassName('each__kind__type')

    for (let index = 0; index < all_kind_type.length; index ++){
        all_kind_type[index].style.display = 'none';
        all_kind_type[index].removeAttribute('required')
    }

    if (kind_type === 'day'){
        document.getElementsByClassName('choose__date__time')[0].style.display = 'inline-block'
        document.getElementsByClassName('choose__date__time')[0].setAttribute('required', 'true')
    }
    else if (kind_type === 'month'){
        document.getElementsByClassName('choose__month__time')[0].style.display = 'inline-block'
        document.getElementsByClassName('choose__month__time')[0].setAttribute('required', 'true')
    }
    else if (kind_type === 'year'){
        document.getElementsByClassName('choose__year__time')[0].style.display = 'inline-block'
        document.getElementsByClassName('choose__year__time')[0].setAttribute('required', 'true')
    }
    else if (kind_type === 'code'){
        document.getElementsByClassName('choose__code__order')[0].style.display = 'inline-block'
        document.getElementsByClassName('choose__code__order')[0].setAttribute('required', 'true')
    }
    else if (kind_type === 'tel'){
        document.getElementsByClassName('choose__tel__type')[0].style.display = 'inline-block'
        document.getElementsByClassName('choose__tel__type')[0].setAttribute('required', 'true')
    }

    // document.getElementsByClassName('submit__kind__time')[0].style.display = 'inline-block'
}