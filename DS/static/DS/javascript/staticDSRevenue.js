function changeStaticDSRevenue(){
    let kind_time = document.getElementsByClassName('select__kind__time')[0].value
    let all_kind_time = document.getElementsByClassName('each__kind__time')

    for (let index = 0; index < all_kind_time.length; index ++){
        all_kind_time[index].style.display = 'none';
        all_kind_time[index].removeAttribute('required')
    }

    if (kind_time === 'day'){
        document.getElementsByClassName('choose__date__time')[0].style.display = 'inline-block'
        document.getElementsByClassName('choose__date__time')[0].setAttribute('required', 'true')
    }
    else if (kind_time === 'month'){
        document.getElementsByClassName('choose__month__time')[0].style.display = 'inline-block'
        document.getElementsByClassName('choose__month__time')[0].setAttribute('required', 'true')
    }
    else if (kind_time === 'year'){
        document.getElementsByClassName('choose__year__time')[0].style.display = 'inline-block'
        document.getElementsByClassName('choose__year__time')[0].setAttribute('required', 'true')
    }

    // document.getElementsByClassName('submit__kind__time')[0].style.display = 'inline-block'
}