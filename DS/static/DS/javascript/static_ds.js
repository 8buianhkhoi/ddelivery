function MakeSomeDivShow(nameDiv){
    allDivStatic = document.getElementsByClassName('each__get__static')
    for ( let index = 0; index < allDivStatic.length ; index++){
        allDivStatic[index].style.display = 'none'
    }

    if (nameDiv === 'csv') {
        document.getElementsByClassName('make__static__csv')[0].style.display = 'block';
    }
    else if (nameDiv === 'status'){
        document.getElementsByClassName('static__by__status')[0].style.display = 'block';
    }
    else if (nameDiv === 'driver'){
        document.getElementsByClassName('static__by__driver')[0].style.display = 'block';
    }
    else if (nameDiv === 'day'){
        document.getElementsByClassName('static__by__day')[0].style.display = 'block';
    }
    else if (nameDiv === 'month'){
        document.getElementsByClassName('static__by__month')[0].style.display = 'block';
    }
    else if (nameDiv === 'year'){
        document.getElementsByClassName('static__by__year')[0].style.display = 'block';
    }
    else if (nameDiv === 'moneyOneDay') {
        document.getElementsByClassName('static__by__total__money__one__day')[0].style.display = 'block'
    }
    else if (nameDiv === 'moneyOneMonth') {
        document.getElementsByClassName('static__by__total__money__one__month')[0].style.display = 'block'
    }
    else if (nameDiv === 'moneyOneYear') {
        document.getElementsByClassName('static__by__total__money__one__year')[0].style.display = 'block'
    }
}

function onPageLoadPagination(){
    if (paginationPage > 5){
        let leftPagination = 2;
        let rightPagination = 2;
        
        if ((CurrentPage - leftPagination < 1) && (CurrentPage + rightPagination <= paginationPage)){
            if (CurrentPage - leftPagination === 0) {
                leftPagination = leftPagination - 1
                rightPagination = rightPagination + 1
            }
            else if (CurrentPage - leftPagination === -1) {
                leftPagination = leftPagination - 2
                rightPagination = rightPagination + 2
                document.getElementById('btn__previous__pagination').style.display = 'none'
                document.getElementById('btn__first__pagination__page').style.display = 'none'
            }
        }
        else if ((CurrentPage - leftPagination >= 1) && (CurrentPage + rightPagination > paginationPage)){
            if (CurrentPage + rightPagination === paginationPage + 1){
                leftPagination = leftPagination + 1
                rightPagination = rightPagination -1
            }
            else if (CurrentPage + rightPagination === paginationPage + 2){
                leftPagination = leftPagination + 2
                rightPagination = rightPagination -2
                document.getElementById('btn__next__pagination').style.display = 'none'
                document.getElementById('btn__end__pagination__page').style.display = 'none'
            }
        }
        let allBtnPagination = document.getElementsByClassName('btn__pagination');
        
        for(let index = 0; index < allBtnPagination.length; index++){
            allBtnPagination[index].style.display = 'none'
        }

        document.getElementById(`btn__page__pagination__page${CurrentPage}`).style.display = 'inline-block';
        
        for (let index = 0; index < leftPagination; index ++){
            document.getElementById(`btn__page__pagination__page${CurrentPage - (index + 1)}`).style.display = 'inline-block';
        }
        for (let index = 0; index < rightPagination; index ++){
            document.getElementById(`btn__page__pagination__page${CurrentPage + (index + 1)}`).style.display = 'inline-block';
        }
    }
}

function previousBtnPagination(){
    CurrentPage = CurrentPage - 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function nextBtnPagination(){
    CurrentPage = CurrentPage + 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function firstPagePagination(){
    CurrentPage = 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function lastPagePagination(){
    CurrentPage = paginationPage;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

