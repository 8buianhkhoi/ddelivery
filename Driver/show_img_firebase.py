# from google.api_core.exceptions import NotFound
# from flask import Blueprint
# from firebase_admin import storage
# import base64

# showImageFirebase_blueprint = Blueprint('showImageFirebaseBp', __name__, static_folder = 'static', template_folder = 'templates')

# def downloadImageFirebase(remote_file_name):
#     bucket = storage.bucket()
#     blob = bucket.blob(remote_file_name)
#     fileBytes = blob.download_as_bytes()
#     return fileBytes

# @showImageFirebase_blueprint.route('/image-in-delivery/<codeOrder>')
# def showImageInDelivery(codeOrder = 'None'):
#     if codeOrder != 'None':
#         fileBytes = downloadImageFirebase(f'Driver/ImageInTransport/{codeOrder}.jpg')
#         contextReturn = {}

#         try:
#             imageInDelivery = base64.b64encode(fileBytes).decode('utf-8')
#             contextReturn['imageShow'] = imageInDelivery
#         except NotFound:
#             contextReturn['errorShowImage'] = 'Not Found Image In Database. Please contact to admin for more information'
#         except Exception as e:
#             contextReturn['errorShowImage'] = 'Have some problem in downloading process. Please contact to admin for more information'

#         return render_template('showImageFbDriver.html', **contextReturn)
#     else:
#         return render_template('showImageFbDriver.html')

# @showImageFirebase_blueprint.route('/image-order-success/<codeOrder>')
# def showImageOrderSuccess(codeOrder = 'None'):
#     if codeOrder != 'None':
#         fileBytes = downloadImageFirebase(f'Driver/DeliverySuccess/{codeOrder}.jpg')
#         contextReturn = {}

#         try:
#             imageOrderSuccess = base64.b64encode(fileBytes).decode('utf-8')
#             contextReturn['imageShow'] = imageOrderSuccess
#         except NotFound:
#             contextReturn['errorShowImage'] = 'Not Found Image In Database. Please contact to admin for more information'
#         except Exception as e:
#             contextReturn['errorShowImage'] = 'Have some problem in downloading process. Please contact to admin for more information'

#         return render_template('showImageFbDriver.html', **contextReturn)
#     else:
#         return render_template('showImageFbDriver.html')

# @showImageFirebase_blueprint.route('/image-get-product/<codeOrder>')
# def showImageGetProduct(codeOrder = 'None'):
#     if codeOrder != 'None':
#         fileBytes = downloadImageFirebase(f'Driver/ImageGetProduct/{codeOrder}.jpg')
#         contextReturn = {}

#         try:
#             imageGetProduct = base64.b64encode(fileBytes).decode('utf-8')
#             contextReturn['imageShow'] = imageGetProduct
#         except NotFound:
#             contextReturn['errorShowImage'] = 'Not Found Image In Database. Please contact to admin for more information'
#         except Exception as e:
#             contextReturn['errorShowImage'] = 'Have some problem in downloading process. Please contact to admin for more information'

#         return render_template('showImageFbDriver.html', **contextReturn)
#     else:
#         return render_template('showImageFbDriver.html')

# @showImageFirebase_blueprint.route('/user-provide-image/<codeOrder>')
# def showImageUserProvide(codeOrder = 'None'):
#     if codeOrder != 'None':
#         fileBytes = downloadImageFirebase(f'User/UserProvideImage/{codeOrder}.jpg')
#         contextReturn = {}

#         try:
#             imageUserProvide = base64.b64encode(fileBytes).decode('utf-8')
#             contextReturn['imageShow'] = imageUserProvide
#         except NotFound:
#             contextReturn['errorShowImage'] = 'Not Found Image In Database. Please contact to admin for more information'
#         except Exception as e:
#             contextReturn['errorShowImage'] = 'Have some problem in downloading process. Please contact to admin for more information'

#         return render_template('showImageFbDriver.html', **contextReturn)
#     else:
#         return render_template('showImageFbDriver.html')