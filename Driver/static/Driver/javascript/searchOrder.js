function showSearchTel(){
    document.getElementsByClassName('search__order__by__tel')[0].style.display = 'inline-block'
    document.getElementsByClassName('btn__expand__search__date')[0].style.display = 'none'
    document.getElementsByClassName('search__order_by__date')[0].style.display = 'none'
}

function showExpandSearchDate(){
    document.getElementsByClassName('btn__expand__search__date')[0].style.display = 'block'
    document.getElementsByClassName('search__order__by__tel')[0].style.display = 'none'
    document.getElementsByClassName('show__order__filter__tel')[0].style.display = 'none'
}

function showSearchDate() {
    let searchOrderCalendar = document.getElementsByClassName('search__order__calendar')
    for (let index = 0; index < searchOrderCalendar.length; index++){
        searchOrderCalendar[index].style.display = 'none'
    }
    document.getElementsByClassName('search__order_by__date')[0].style.display = 'block'
    document.getElementsByClassName('search__order__by__day')[0].style .display = 'block'
}

function showSearchMonth(){
    let searchOrderCalendar = document.getElementsByClassName('search__order__calendar')
    for (let index = 0; index < searchOrderCalendar.length; index++){
        searchOrderCalendar[index].style.display = 'none'
    }
    document.getElementsByClassName('search__order_by__date')[0].style.display = 'block'
    document.getElementsByClassName('search__oder__by__month')[0].style.display = 'block'
}

function showSearchYear(){
    let searchOrderCalendar = document.getElementsByClassName('search__order__calendar')
    for (let index = 0; index < searchOrderCalendar.length; index++){
        searchOrderCalendar[index].style.display = 'none'
    }
    document.getElementsByClassName('search__order_by__date')[0].style.display = 'block'
    document.getElementsByClassName('search__order__by__year')[0].style.display = 'block'
}