function changeType(){
    let all_input = document.getElementsByClassName('each__input')
    let value_input = document.getElementsByClassName('choose__type__static')[0].value

    for( let index = 0; index < all_input.length; index++){
        all_input[index].style.display = 'none'
    }

    if (value_input === 'day'){
        document.getElementsByClassName('input__date')[0].style.display = 'inline-block'
    }
    else if (value_input === 'month'){
        document.getElementsByClassName('input__month')[0].style.display = 'inline-block'
    }
    else if (value_input === 'year'){
        document.getElementsByClassName('input__year')[0].style.display = 'inline-block'
    }
}