function chooseFilter(){
    let filterValue = document.getElementById('option__filter').value 
    let inputFilter = document.getElementsByClassName('input__filter')

    for (let index = 0; index < inputFilter.length; index++){
        inputFilter[index].style.display = 'none'
    }

    if (filterValue === 'date') {
        document.getElementById('input__date__filter').style.display = 'inline-block'
        document.getElementById('input__submit__filter__date').style.display = 'inline-block'
        document.getElementById('form__input__filter').style.display = 'inline-block'
    }
    else if (filterValue === 'month') {
        document.getElementById('input__month__filter').style.display = 'inline-block'
        document.getElementById('input__submit__filter__month').style.display = 'inline-block'
        document.getElementById('form__input__filter').style.display = 'inline-block'
    }
    else if (filterValue === 'year') {
        document.getElementById('input__year__filter').style.display = 'inline-block'
        document.getElementById('input__submit__filter__year').style.display = 'inline-block'
        document.getElementById('form__input__filter').style.display = 'inline-block'
    }
}

function onPageLoadPagination(){
    if (paginationPage > 5){
        let leftPagination = 2;
        let rightPagination = 2;
        
        if ((CurrentPage - leftPagination < 1) && (CurrentPage + rightPagination <= paginationPage)){
            if (CurrentPage - leftPagination === 0) {
                leftPagination = leftPagination - 1
                rightPagination = rightPagination + 1
            }
            else if (CurrentPage - leftPagination === -1) {
                leftPagination = leftPagination - 2
                rightPagination = rightPagination + 2
                document.getElementById('btn__previous__pagination').style.display = 'none'
                document.getElementById('btn__first__pagination__page').style.display = 'none'
            }
        }
        else if ((CurrentPage - leftPagination >= 1) && (CurrentPage + rightPagination > paginationPage)){
            if (CurrentPage + rightPagination === paginationPage + 1){
                leftPagination = leftPagination + 1
                rightPagination = rightPagination -1
            }
            else if (CurrentPage + rightPagination === paginationPage + 2){
                leftPagination = leftPagination + 2
                rightPagination = rightPagination -2
                document.getElementById('btn__next__pagination').style.display = 'none'
                document.getElementById('btn__end__pagination__page').style.display = 'none'
            }
        }
        let allBtnPagination = document.getElementsByClassName('btn__pagination');
        
        for(let index = 0; index < allBtnPagination.length; index++){
            allBtnPagination[index].style.display = 'none'
        }

        document.getElementById(`btn__page__pagination__page${CurrentPage}`).style.display = 'inline-block';
        
        for (let index = 0; index < leftPagination; index ++){
            document.getElementById(`btn__page__pagination__page${CurrentPage - (index + 1)}`).style.display = 'inline-block';
        }
        for (let index = 0; index < rightPagination; index ++){
            document.getElementById(`btn__page__pagination__page${CurrentPage + (index + 1)}`).style.display = 'inline-block';
        }
    }
}

function previousBtnPagination(){
    CurrentPage = CurrentPage - 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function nextBtnPagination(){
    CurrentPage = CurrentPage + 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function firstPagePagination(){
    CurrentPage = 1;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}

function lastPagePagination(){
    CurrentPage = paginationPage;
    document.getElementById(`btn__page__pagination__page${CurrentPage}`).click();
}