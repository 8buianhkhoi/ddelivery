# from flask import Blueprint, render_template, session, request, redirect, url_for
# import networkx as nx
# import folium
# import math

# from . import driverExecuteDatabase

# autoUpdateRoute_blueprint = Blueprint('autoUpdateRouteBp', __name__, static_folder = 'static', template_folder = 'templates')
# mapShortestRouteHtml = ''

# # Calculation distance two point in earth. each point was set by latitude and longitude
# # using haversine algorithm
# def haversineDistance(lat1, lon1, lat2, lon2):
#     lat1, lon1, lat2, lon2 = map(math.radians, [float(lat1), float(lon1), float(lat2), float(lon2)])
#     radius = 6371

#     d_lat = lat2 - lat1
#     d_lon = lon2 - lon1

#     a = math.sin(d_lat/2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(d_lon/2)**2
#     c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
#     distance = radius * c

#     return distance

# # This route find shortest way for all order for driver
# # We use networkx library to calculation shortest way. Folium library for draw map
# @autoUpdateRoute_blueprint.route('/auto-update-route', methods = ['GET', 'POST'])
# def autoUpdateRoute():
#     id_driver = session['idDriver']
    
#     all_order_need_delivery = driverExecuteDatabase.allOrderNeedDeliveryAutoUpdateRoute(id_driver)
    
#     # (21.0363416, 105.855503) is latitude and longitude of phúc tân, hoàn kiếm district, hà nội city 
#     # This point is use for inventory and main store
#     lng_store = 105.855503
#     lat_store = 21.0363416

#     # Create a graph and add first node in graph is store
#     G = nx.Graph()
#     G.add_node("store", pos=(lat_store, lng_store))
#     count = 1
#     data_node_temp = {}

#     for index in all_order_need_delivery:
#         idDA_temp = index[2]
#         deliveryAddr_temp = driverExecuteDatabase.getDeliveryAddrAutoUpdateRoute(idDA_temp)
#         data_node_temp[f'node_{count}'] = f'{deliveryAddr_temp[6]}, {deliveryAddr_temp[5]}, {deliveryAddr_temp[4]}'

#         G.add_node(f'node_{count}', pos = (deliveryAddr_temp[10], deliveryAddr_temp[11]))
#         count = count + 1

#     for node1 in G.nodes:
#         for node2 in G.nodes:
#             if node1 != node2:
#                 pos1 = G.nodes[node1]["pos"]
#                 pos2 = G.nodes[node2]["pos"]
                
#                 distance = haversineDistance(pos1[0], pos1[1], pos2[0], pos2[1])
#                 G.add_edge(node1, node2, weight=distance)

#     shortest_path = nx.approximation.traveling_salesman_problem(G, weight="weight", cycle=True)
    
#     mapShortestRoute = folium.Map(location=[lat_store, lng_store], zoom_start=13)

#     for index in range(1, len(shortest_path) - 1):
#         folium.Marker([float(G.nodes[index]['pos'][0]), float(G.nodes[index]['pos'][1])], popup=f'Step {index}').add_to(mapShortestRoute)

#     shortest_route_str = []

#     for index in shortest_path[1:-1]:
#         shortest_route_str.append(data_node_temp[index])
    
#     if request.method == 'POST':
#         if 'seeMap' in request.form:
#             global mapShortestRouteHtml
#             mapShortestRouteHtml = mapShortestRoute.get_root().render()
#             return redirect(url_for('.seeMapShortestRoute'))

#     return render_template('autoUpdateRoute.html', shortestRouteStr = shortest_route_str)

# # This route help driver see shortest way with map
# @autoUpdateRoute_blueprint.route('/see-map-shortest-route')
# def seeMapShortestRoute():
#     return mapShortestRouteHtml