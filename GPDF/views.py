from django.shortcuts import render, redirect
import pdfkit
import ast

# Create your views here.

# This route for create pdf file and download pdf file
def generatePDF(request):
    codeOrder = request.GET.get('codeOrder', None)
    totalCost = request.GET.get('totalCost', None)
    nameDriver = request.GET.get('driverName', None)
    sender = ast.literal_eval(request.GET.get('sender', None))
    receiver = ast.literal_eval(request.GET.get('receiver', None))
    allProduct = [ast.literal_eval(request.GET.get('allProduct', None)[1:-1])]
    dateCreate = request.GET.get('dateCreate', None)
    departureTimeOrder = request.GET.get('departureTimeOrder', None)
    estimateArriveTime = request.GET.get('estimateArriveOrder', None)

    return render_template('generatePDF.html', codeOrder = codeOrder, totalCost = totalCost, nameDriver = nameDriver, sender = sender, 
                receiver = receiver, allProduct = allProduct, dateCreate = dateCreate, 
                departureTimeOrder = departureTimeOrder, estimateArriveTime = estimateArriveTime, pdfStatus = True)

def downloadPDF(request):
    codeOrder = request.GET.get('codeOrder', None)
    totalCost = request.GET.get('totalCost', None)
    nameDriver = request.GET.get('driverName', None)
    sender = ast.literal_eval(request.GET.get('sender'), None)
    receiver = ast.literal_eval(request.GET.get('receiver', None))
    allProduct = [ast.literal_eval(request.GET.get('allProduct', None)[1:-1])]
    dateCreate = request.GET.get('dateCreate', None)
    departureTimeOrder = request.GET.get('departureTimeOrder', None)
    estimateArriveTime = request.GET.get('estimateArriveOrder', None)

    renderer = render_template('generatePDF.html', codeOrder = codeOrder, totalCost = totalCost, nameDriver = nameDriver, sender = sender, 
                receiver = receiver, allProduct = allProduct, dateCreate = dateCreate, 
                departureTimeOrder = departureTimeOrder, estimateArriveTime = estimateArriveTime)

    if platform.system() == 'Linux':
        config = pdfkit.configuration(wkhtmltopdf = '/usr/local/bin/wkhtmltopdf')
        pdf = pdfkit.from_string(renderer, False, configuration = config)
    elif platform.system() == 'Windows':
        pdf = pdfkit.from_string(renderer, False)

    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'attachment; filename = output.pdf'

    return response 
