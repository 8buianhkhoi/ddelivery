from django.urls import path

from . import views 

app_name = 'GPDF_app'

urlpatterns = [
    path('generate-pdf/', views.generatePDF, name = 'GPDF_gen_pdf'),
    path('see-pdf/', views.downloadPDF, name = 'GPDF_see_pdf')
]